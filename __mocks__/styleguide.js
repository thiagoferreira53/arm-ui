import { forwardRef } from 'react';

export const Core = {
  Typography: ({ children, rest }) => <div {...rest}>{children}</div>,
  Box: ({ children, rest }) => <div {...rest}>{children}</div>,
};
export const Styles = {};
export const Icons = {};
export const Colors = {};
export const EBSMaterialUIProvider = ({ children }) => <>{children}</>;
