import { combineReducers } from 'redux'
import newRequest from './modules/NewRequest'

export default combineReducers({
  newRequest,
})
