import reduxModule, { createStore, applyMiddleware, compose } from 'redux'
import ReduxThunk from 'redux-thunk'

import reducers from './reducers'

reduxModule.__DO_NOT_USE__ActionTypes.REPLACE = '@@redux/INIT'
const composeEnhancers =
  process.env.NODE_ENV !== 'production' &&
  typeof window === 'object' &&
  window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
        // Specify extension’s options like name, actionsBlacklist, actionsCreators, serialize...
      })
    : compose
const enhancer = composeEnhancers(applyMiddleware(ReduxThunk))

//const store = createStore(reducers, applyMiddleware(ReduxThunk))
const store = createStore(reducers, enhancer)

export default store
