import { getDomainContext, getTokenId } from '@ebs/layout';

/**
 * @description Number of version to display
 */
export const VERSION = '22.02.2b';

/**
 * @description Const to allow select one or multiple experiments
 */
export const MULTIPLE_IN_GRID = 'Multiple';

/**
 * @description URL to API in core breeding
 */
export const CB_API = getDomainContext('cb').sgContext;

/**
 * @description URL to API in breeding
 */
export const BA_API = getDomainContext('ba').sgContext;

/**
 * @description URL to GraphQL in core system
 */
export const CS_API_GRAPHQL = `${
  getDomainContext('cs').sgContext
}graphql`.toString();

/**
 * @description Enable or disable [0|1] pop up to set new token in storage location
 */
 export const TOKEN_HELP_POPUP = 0;

/**
 * @description Bear token with autentication
 */
export const TOKEN =
  TOKEN_HELP_POPUP === 1
    ? localStorage.getItem(TOKEN_KEY_STORAGE_LOCATION)
    : getTokenId();

/**
 * @description       Filter row configuration
 * @returns {object}  Configuration setting
 * @returns {string}  return0.keyStorageLocation - Key to search in locat storage
 * @returns {object}  return0.sameExperiemnt - Information about the filter same experiment
 * @returns {bool}    return0.sameExperiemnt.isActive - Active filter
 * @returns {bool}    return0.sameExperiemnt.isEnable - Enable or disable filter in component
 * @returns {object}  return0.responseVaraibleOnlyFloat - Information about the filter response variable only float
 * @returns {bool}    return0.responseVaraibleOnlyFloat.isActive - Active filter
 * @returns {bool}    return0.responseVaraibleOnlyFloat.isEnable - Enable or disable filter in component
 */
export const FILTER_ROW = {
  keyStorageLocation: 'ba-filter-row',
  configuration: {
    sameExperiemnt: {
      isActive: true,
      isEnable: false,
    },
    responseVaraibleOnlyFloat: {
      isActive: true,
      isEnable: true,
      valueShouldTrue : ['FLOAT'],
    },
  },
};

/**
 * @description Key to search in locat storage token
 */
export const TOKEN_KEY_STORAGE_LOCATION = 'id_token';
