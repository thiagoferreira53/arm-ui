import Axios from 'axios';
import { CB_API, TOKEN } from 'utils/config';

/**
 * @description - Instance of axios to conect to Core Bredding API
 * @returns - instance of axios
 */
const instance = Axios.create({
  baseURL: CB_API,
  headers: {
    Authorization: `Bearer ${TOKEN}`,
  },
});

/**
 * @description - Create a interceptors to update the token
 */
instance.interceptors.request.use(function (config) {
  config.headers.Authorization = `Bearer ${TOKEN}`;
  return config;
});
export default instance;
