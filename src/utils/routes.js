/**
 * @description - Route to add new request
 */
export const ROUTE_ADD_REQUEST = '/add';

/**
 * @description - Default page to display all request was created
 */
export const ROUTE_LIST_REQUEST = '/list';
