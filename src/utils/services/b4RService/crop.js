import { getExperimentById } from './experiment'

/**
 * @description       - Get crop from first experiment
 * @param {array}     experimentDbIds - List of experiment ID
 * @returns {string}  - Name of crop
 */
export async function getFirstCropFromExperiment(experimentDbIds) {
  if (experimentDbIds !== undefined && experimentDbIds.length !== 0) {
    const result = await getExperimentById(experimentDbIds)
    const experiments = result.data
    const crops = [...new Set(experiments.map((experiment) => experiment.cropCode))]
    return crops[0]
  } else {
    return ''
  }
}
