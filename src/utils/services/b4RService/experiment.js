import clientAxios from 'utils/client/clientAxiosB4R';
import { getUrlB4rExperimentSearch } from 'utils/helpers/b4rHelper';
import { ExceptionTokenLive } from 'utils/helpers/coreHelper';

/**
 * @description       - Search all experiment from API
 * @param {number}    limit - Max number of element for page
 * @param {number}    page - Number of page
 * @param {array}     searchParameters - List of parameter to search from API
 * @returns {object}  - Information from API
 * @returns {object}  return0.status  - Object status about the request
 * @returns {number}  return0.status.status [200|201|400]  - Number of status GET - 200 => success, POST 201 => succes, 400 => Not found and 500 => error
 * @returns {string}  return0.status.message - Message of error
 * @returns {array}   return0.data - Data get of API
 * @returns {object}  return0.metadata - General information
 * @returns {object}  return0.metadata.pagination - Information about the pagination
 * @returns {number}  return0.metadata.pagination.totalPages - Total of pages
 * @returns {number}  return0.metadata.pagination.totalCount - Total of element in global request
 */
export async function getExperiment(limit, page, searchParameters) {
  let dataSet = {
    status: {
      status: 100,
      message: '',
    },
    data: [],
    metadata: {
      pagination: {
        totalPages: 0,
        totalCount: 0,
      },
    },
  };
  try {
    const url = getUrlB4rExperimentSearch(limit, page);
    const parameter = {
      experimentStatus: 'planted|observation',
      experimentType: 'Breeding Trial',
      ...searchParameters,
    };
    let result = await clientAxios.post(url, parameter);
    dataSet.data = result.data.result.data;
    dataSet.metadata.pagination.totalPages =
      result.data.metadata.pagination.totalPages;
    dataSet.metadata.pagination.totalCount =
      result.data.metadata.pagination.totalCount;
    dataSet.status.status = 200;
  } catch (ex) {
    //#CHANGE-LOG
    //console.log(`Error axios - Experiments:`.toString(), ex);
    if (ex instanceof ExceptionTokenLive) {
      dataSet.status.status = 500;
      dataSet.status.message = ex.message;
    } else {
      dataSet.status.status = ex.response.status;
      if (401 === ex.response.status) {
        dataSet.status.message = 'Unauthorized to view experiments.';
      } else if (404 === ex.response.status) {
        dataSet.status.message = 'No experiments found.';
      } else if (500 <= ex.response.status) {
        dataSet.status.message = 'Unable to view experiments.';
      }
    }
  }
  return dataSet;
}

/**
 * @description       - Search only one experiment by ID
 * @param {array}     experimentDbIds - List of experiment ID
 * @returns {object}  - Information from API
 * @returns {object}  return0.status  - Object status about the request
 * @returns {number}  return0.status.status [200|201|400]  - Number of status GET - 200 => success, POST 201 => succes, 400 => Not found and 500 => error
 * @returns {string}  return0.status.message - Message of error
 * @returns {array}   return0.data - Data get of API
 * @returns {object}  return0.metadata - General information
 * @returns {object}  return0.metadata.pagination - Information about the pagination
 * @returns {number}  return0.metadata.pagination.totalPages - Total of pages
 * @returns {number}  return0.metadata.pagination.totalCount - Total of element in global request
 */
export async function getExperimentById(experimentDbIds) {
  let dataSet = {
    status: {
      status: 100,
      message: '',
    },
    data: [],
    metadata: {
      pagination: {
        totalPages: 0,
        totalCount: 0,
      },
    },
  };
  const url = getUrlB4rExperimentSearch(100, 1);
  const parameter = {
    experimentDbId: experimentDbIds.join(','),
  };
  try {
    let result = await clientAxios.post(url, parameter);
    dataSet.data = result.data.result.data;
    dataSet.metadata.pagination.totalPages =
      result.data.metadata.pagination.totalPages;
    dataSet.metadata.pagination.totalCount =
      result.data.metadata.pagination.totalCount;
    dataSet.status.status = 200;
  } catch (ex) {
    //#CHANGE-LOG
    //console.log(`Error axios - Experiments by Id:`.toString(), ex)
    dataSet.status.status = ex.response.status;
    if (401 === ex.response.status) {
      dataSet.status.message = 'Unauthorized to view experiments.';
    } else if (404 === ex.response.status) {
      dataSet.status.message = 'No experiments found.';
    } else if (500 <= ex.response.status) {
      dataSet.status.message = 'Unable to view experiments.';
    }
  }
  return dataSet;
}
