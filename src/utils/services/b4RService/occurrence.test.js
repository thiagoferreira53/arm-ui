import { getIntersectionTraitFromOccurrences } from './occurrence';

test('Test get intersection trait from ocurrences - one trait, enable filter', () => {
  const occurrences = [
    {
      traits: [
        { variableDataType: 'FLOAT', variableDbId: 1, variableAbbrev: 'Test1' },
      ],
    },
  ];
  const filterRow = {
    sameExperiemnt: {
      isActive: true,
      isEnable: false,
    },
    responseVaraibleOnlyFloat: {
      isActive: true,
      isEnable: true,
      valueShouldTrue: ['FLOAT'],
    },
  };
  const resutl = getIntersectionTraitFromOccurrences(
    occurrences,
    filterRow.responseVaraibleOnlyFloat,
  );
  expect(resutl.length).toBe(1);
  expect(resutl[0].id).toBe(1);
  expect(resutl[0].title).toBe('Test1');
});

test('Test get intersection trait from ocurrences - one trait disable filter', () => {
  const occurrences = [
    {
      traits: [
        { variableDataType: 'FLOAT', variableDbId: 1, variableAbbrev: 'Test1' },
      ],
    },
  ];
  const responseVaraibleOnlyFloat = {
      isActive: false,
      isEnable: true,
      valueShouldTrue: ['FLOAT'],
  };
  const resutl = getIntersectionTraitFromOccurrences(
    occurrences,
    responseVaraibleOnlyFloat,
  );
  expect(resutl.length).toBe(1);
  expect(resutl[0].id).toBe(1);
  expect(resutl[0].title).toBe('Test1');
});

test('Test get intersection trait from ocurrences - two trait, enable filter', () => {
  const occurrences = [
    {
      traits: [
        { variableDataType: 'FLOAT', variableDbId: 1, variableAbbrev: 'Test1' },
      ],
    },
    {
      traits: [
        { variableDataType: 'FLOAT', variableDbId: 1, variableAbbrev: 'Test1' },
      ],
    },
  ];
  const filterRow = {
    sameExperiemnt: {
      isActive: true,
      isEnable: false,
    },
    responseVaraibleOnlyFloat: {
      isActive: true,
      isEnable: true,
      valueShouldTrue: ['FLOAT'],
    },
  };
  filterRow.responseVaraibleOnlyFloat.isActive = true;
  const resutl = getIntersectionTraitFromOccurrences(
    occurrences,
    filterRow.responseVaraibleOnlyFloat,
  );
  expect(resutl.length).toBe(1);
  expect(resutl[0].id).toBe(1);
  expect(resutl[0].title).toBe('Test1');
});

test('Test get intersection trait from ocurrences - tow trait disable filter', () => {
  const occurrences = [
    {
      traits: [
        { variableDataType: 'FLOAT', variableDbId: 1, variableAbbrev: 'Test1' },
      ],
    },
    {
      traits: [
        { variableDataType: 'FLOAT', variableDbId: 1, variableAbbrev: 'Test1' },
      ],
    },
  ];
  const filterRow = {
    sameExperiemnt: {
      isActive: true,
      isEnable: false,
    },
    responseVaraibleOnlyFloat: {
      isActive: false,
      isEnable: true,
      valueShouldTrue: ['FLOAT'],
    },
  };
  const resutl = getIntersectionTraitFromOccurrences(
    occurrences,
    filterRow.responseVaraibleOnlyFloat,
  );
  expect(resutl.length).toBe(1);
  expect(resutl[0].id).toBe(1);
  expect(resutl[0].title).toBe('Test1');
});

test('Test get intersection trait from ocurrences - occurrences with multiple equals trait , enable filter', () => {
  const occurrences = [
    {
      traits: [
        { variableDataType: 'FLOAT', variableDbId: 1, variableAbbrev: 'Test1' },
        { variableDataType: 'INT', variableDbId: 2, variableAbbrev: 'Test2' },
      ],
    },
    {
      traits: [
        { variableDataType: 'FLOAT', variableDbId: 1, variableAbbrev: 'Test1' },
        { variableDataType: 'INT', variableDbId: 2, variableAbbrev: 'Test2' },
      ],
    },
  ];
  const filterRow = {
    sameExperiemnt: {
      isActive: true,
      isEnable: false,
    },
    responseVaraibleOnlyFloat: {
      isActive: true,
      isEnable: true,
      valueShouldTrue: ['FLOAT'],
    },
  };
  const resutl = getIntersectionTraitFromOccurrences(
    occurrences,
    filterRow.responseVaraibleOnlyFloat,
  );
  expect(resutl.length).toBe(1);
  expect(resutl[0].id).toBe(1);
  expect(resutl[0].title).toBe('Test1');
});

test('Test get intersection trait from ocurrences - occurrences with multiple equals trait , disbale filter', () => {
  const occurrences = [
    {
      traits: [
        { variableDataType: 'FLOAT', variableDbId: 1, variableAbbrev: 'Test1' },
        { variableDataType: 'INT', variableDbId: 2, variableAbbrev: 'Test2' },
      ],
    },
    {
      traits: [
        { variableDataType: 'FLOAT', variableDbId: 1, variableAbbrev: 'Test1' },
        { variableDataType: 'INT', variableDbId: 2, variableAbbrev: 'Test2' },
      ],
    },
  ];
  const filterRow = {
    sameExperiemnt: {
      isActive: true,
      isEnable: false,
    },
    responseVaraibleOnlyFloat: {
      isActive: false,
      isEnable: true,
      valueShouldTrue: ['FLOAT'],
    },
  };
  const resutl = getIntersectionTraitFromOccurrences(
    occurrences,
    filterRow.responseVaraibleOnlyFloat,
  );
  expect(resutl.length).toBe(2);
  expect(resutl).toEqual([
    { id: 1, title: 'Test1' },
    { id: 2, title: 'Test2' },
  ]);
});

test('Test get intersection trait from ocurrences - occurrences with multiple not equals trait, enable filter', () => {
  const occurrences = [
    {
      traits: [
        { variableDataType: 'INT', variableDbId: 2, variableAbbrev: 'Test2' },
      ],
    },
    {
      traits: [
        { variableDataType: 'FLOAT', variableDbId: 1, variableAbbrev: 'Test1' },
        { variableDataType: 'INT', variableDbId: 2, variableAbbrev: 'Test2' },
      ],
    },
  ];
  const responseVaraibleOnlyFloat = {
    isActive: true,
    isEnable: true,
    valueShouldTrue: ['FLOAT'],
  };
  const resutl = getIntersectionTraitFromOccurrences(
    occurrences,
    responseVaraibleOnlyFloat,
  );
  expect(resutl.length).toBe(0);
});

test('Test get intersection trait from ocurrences - occurrences with multiple not equals trait, disable filter', () => {
  const occurrences = [
    {
      traits: [
        { variableDataType: 'INT', variableDbId: 2, variableAbbrev: 'Test2' },
      ],
    },
    {
      traits: [
        { variableDataType: 'FLOAT', variableDbId: 1, variableAbbrev: 'Test1' },
        { variableDataType: 'INT', variableDbId: 2, variableAbbrev: 'Test2' },
      ],
    },
  ];
  const responseVaraibleOnlyFloat = {
    isActive: false,
    isEnable: true,
    valueShouldTrue: ['FLOAT'],
  };
  const resutl = getIntersectionTraitFromOccurrences(
    occurrences,
    responseVaraibleOnlyFloat,
  );
  expect(resutl.length).toBe(1);
  expect(resutl[0]).toEqual({ id: 2, title: 'Test2' });
});

test('Test get intersection trait from ocurrences - occurrences with multiple not equals trait, filter type int', () => {
  const occurrences = [
    {
      traits: [
        { variableDataType: 'INT', variableDbId: 2, variableAbbrev: 'Test2' },
        { variableDataType: 'FLOAT', variableDbId: 1, variableAbbrev: 'Test1' },
      ],
    },
    {
      traits: [
        { variableDataType: 'FLOAT', variableDbId: 1, variableAbbrev: 'Test1' },
        { variableDataType: 'INT', variableDbId: 2, variableAbbrev: 'Test2' },
      ],
    },
  ];
  const responseVaraibleOnlyFloat = {
    isActive: true,
    isEnable: true,
    valueShouldTrue: ['INT', 'FLOAT'],
  };
  const resutl = getIntersectionTraitFromOccurrences(
    occurrences,
    responseVaraibleOnlyFloat,
  );
  expect(resutl.length).toBe(2);
  expect(resutl).toEqual([{ id: 1, title: 'Test1' },{ id: 2, title: 'Test2' }]);
});
