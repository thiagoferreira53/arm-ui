import clientAxios from 'utils/client/clientAxiosB4R';
import {
  getUrlB4rOccurrencesSearch,
  getUrlB4rOccurrencesPlotDataSearch,
  SORT_OCCURRENCEDBID_DESC,
  getUrlB4rOccurrencesPlotSearch,
  getUrlVariableDetail,
} from 'utils/helpers/b4rHelper';

/**
 * @description       - Search all occurrences by experiment ID
 * @param {number}    experimentDbId - ID of experiment
 * @param {number}    limit - Max number of element for page
 * @param {number}    page - Number of page
 * @returns {object}  - Information from API
 * @returns {object}  return0.status  - Object status about the request
 * @returns {number}  return0.status.status [200|201|400]  - Number of status GET - 200 => success, POST 201 => succes, 400 => Not found and 500 => error
 * @returns {string}  return0.status.message - Message of error
 * @returns {array}   return0.data - Data get of API
 * @returns {object}  return0.metadata - General information
 * @returns {object}  return0.metadata.pagination - Information about the pagination
 * @returns {number}  return0.metadata.pagination.totalPages - Total of pages
 * @returns {number}  return0.metadata.pagination.totalCount - Total of element in global request
 */
export async function getOccurrenceByExperimentDbId(
  experimentDbId,
  limit = 10,
  page = 1,
) {
  let dataSet = {
    status: {
      status: 100,
      message: '',
    },
    data: [],
    metadata: {
      pagination: {
        totalPages: 0,
      },
    },
  };
  const parameter = {
    experimentDbId: `${experimentDbId}`.toString(),
  };
  try {
    let url = getUrlB4rOccurrencesSearch(limit, page, SORT_OCCURRENCEDBID_DESC);
    let result = await clientAxios.post(url, parameter);
    dataSet.data = result.data.result.data;
    dataSet.metadata.pagination.totalPages =
      result.data.metadata.pagination.totalPages;
    dataSet.status.status = 200;
  } catch (ex) {
    //#CHANGE-LOG
    //console.log(`Error axios - Occurrence by Experiment:`.toString(), ex)
    dataSet.status.status = ex.response.status;
    if (401 === ex.response.status) {
      dataSet.status.message = 'Unauthorized to view occurrence.';
    } else if (404 === ex.response.status) {
      dataSet.status.message = 'No occurrence found.';
    } else if (500 <= ex.response.status) {
      dataSet.status.message = 'Unable to view occurrence.';
    }
  }
  return dataSet;
}

/**
 * @description       - Search plot data from occurrence ID
 * @param {number}    occurrenceDbId - ID of occurrence
 * @returns {object}  - Information from API
 * @returns {object}  return0.status  - Object status about the request
 * @returns {number}  return0.status.status [200|201|400]  - Number of status GET - 200 => success, POST 201 => succes, 400 => Not found and 500 => error
 * @returns {string}  return0.status.message - Message of error
 * @returns {array}   return0.data - Data get of API
 * @returns {object}  return0.metadata - General information
 * @returns {object}  return0.metadata.pagination - Information about the pagination
 * @returns {number}  return0.metadata.pagination.totalPages - Total of pages
 * @returns {number}  return0.metadata.pagination.totalCount - Total of element in global request
 */
export async function getPlotDataByOccurrenceDbId(occurrenceDbId) {
  let dataSet = {
    status: {
      status: 100,
      message: '',
    },
    data: [],
    metadata: {
      pagination: {
        totalPages: 0,
      },
    },
  };
  const parameter = {
    distinctOn: 'variableDbId',
  };
  try {
    let url = getUrlB4rOccurrencesPlotDataSearch(occurrenceDbId);
    const result = await clientAxios.post(url, parameter);
    dataSet.data = result.data.result.data;
    dataSet.metadata.pagination.totalPages =
      result.data.metadata.pagination.totalPages;
    dataSet.status.status = 200;
    //Search more information from trait
    if(dataSet.data[0].plotData.length > 0 ){
      for(let trait  of dataSet.data[0].plotData){
        const resultTrait = await getVariableDetail(trait.variableDbId);
        if (resultTrait.status.status === 200 && resultTrait.data.length === 1)
          trait.variableDataType = resultTrait.data[0].dataType;
      }
    }
  } catch (ex) {
    //#CHANGE-LOG
    //console.log(`Error axios - Plot data by Occurrence:`.toString(), ex)
    dataSet.status.status = ex.response.status;
    if (401 === ex.response.status) {
      dataSet.status.message = 'Unauthorized to view occurrences.';
    } else if (404 === ex.response.status) {
      dataSet.status.message = 'No occurrences found.';
    } else if (500 <= ex.response.status) {
      dataSet.status.message = 'Unable to view occurrences.';
    }
  }
  return dataSet;
}

/**
 * @description       - Search more information about the variable in B4R
 * @param {number}    variableID - ID to search detail
 * @returns {object}  - Information from API
 * @returns {object}  return0.status  - Object status about the request
 * @returns {number}  return0.status.status [200|201|400]  - Number of status GET - 200 => success, POST 201 => succes, 400 => Not found and 500 => error
 * @returns {string}  return0.status.message - Message of error
 * @returns {array}   return0.data - Data get of API
 * @returns {object}  return0.metadata - General information
 * @returns {object}  return0.metadata.pagination - Information about the pagination
 * @returns {number}  return0.metadata.pagination.totalPages - Total of pages
 * @returns {number}  return0.metadata.pagination.totalCount - Total of element in global request
 */
async function getVariableDetail(variableID){
  let dataSet = {
    status: {
      status: 100,
      message: '',
    },
    data: [],
    metadata: {
      pagination: {
        totalPages: 0,
      },
    },
  };
  try {
    const url = getUrlVariableDetail(variableID);
    const result = await clientAxios.get(url, {});
    dataSet.data = result.data.result.data;
    dataSet.metadata.pagination.totalPages =
      result.data.metadata.pagination.totalPages;
    dataSet.status.status = 200;
  } catch (ex) {
    dataSet.status.status = 500;
    dataSet.status.message = 'Unable to view varaible.';
  }
  return dataSet;
}

/**
 * @description       - Search plot from occurrence ID
 * @param {number}    occurrenceDbId - ID of occurrence
 * @returns {object}  - Information from API
 * @returns {object}  return0.status  - Object status about the request
 * @returns {number}  return0.status.status [200|201|400]  - Number of status GET - 200 => success, POST 201 => succes, 400 => Not found and 500 => error
 * @returns {string}  return0.status.message - Message of error
 * @returns {array}   return0.data - Data get of API
 * @returns {object}  return0.metadata - General information
 * @returns {object}  return0.metadata.pagination - Information about the pagination
 * @returns {number}  return0.metadata.pagination.totalPages - Total of pages
 * @returns {number}  return0.metadata.pagination.totalCount - Total of element in global request
 */
async function getPlotByOccurrenceDbId(occurrenceDbId) {
  let dataSet = {
    status: {
      status: 100,
      message: '',
    },
    data: [],
    metadata: {
      pagination: {
        totalPages: 0,
      },
    },
  };
  const parameter = {};
  try {
    let url = getUrlB4rOccurrencesPlotSearch(occurrenceDbId);
    let result = await clientAxios.post(url, parameter);
    dataSet.data = result.data.result.data;
    dataSet.metadata.pagination.totalPages =
      result.data.metadata.pagination.totalPages;
    dataSet.status.status = 200;
  } catch (ex) {
    //#CHANGE-LOG
    //console.log(`Error axios - Plot data by Occurrence:`.toString(), ex)
    dataSet.status.status = ex.response.status;
    if (401 === ex.response.status) {
      dataSet.status.message = 'Unauthorized to view occurrences.';
    } else if (404 === ex.response.status) {
      dataSet.status.message = 'No occurrences found.';
    } else if (500 <= ex.response.status) {
      dataSet.status.message = 'Unable to view occurrences.';
    }
  }
  return dataSet;
}

/**
 * @description       - Search occurrences with trait information
 * @param {number}    experimentDbId - ID of experiment
 * @returns {object}  - Information from API
 * @returns {object}  return0.status  - Object status about the request
 * @returns {number}  return0.status.status [200|201|400]  - Number of status GET - 200 => success, POST 201 => succes, 400 => Not found and 500 => error
 * @returns {string}  return0.status.message - Message of error
 * @returns {array}   return0.data - Data get of API
 * @returns {object}  return0.metadata - General information
 * @returns {object}  return0.metadata.pagination - Information about the pagination
 * @returns {number}  return0.metadata.pagination.totalPages - Total of pages
 * @returns {number}  return0.metadata.pagination.totalCount - Total of element in global request
 */
export async function getOccurrenceWithTraitByExperimentDbId(experimentDbId) {
  let dataSet = {
    status: {
      status: 100,
      message: '',
    },
    data: [],
    metadata: {
      pagination: {
        totalPages: 0,
      },
    },
  };
  let result = await getOccurrenceByExperimentDbId(experimentDbId);
  if (result.status.status === 200) {
    for (let element of result.data) {
      if (dataSet.status.status === 100) {
        let trait = await getPlotDataByOccurrenceDbId(element.occurrenceDbId);
        let plot = await getPlotByOccurrenceDbId(element.occurrenceDbId);
        if (trait.status.status === 200) {
          if (trait.data[0].plotData.length > 0) {
            element.traits = trait.data[0].plotData.map((item) => {
              return {
                variableDbId: item.variableDbId,
                variableAbbrev: item.variableAbbrev,
                variableDataType: item.variableDataType,
              };
            });
          } else {
            element.traits = [];
          }
        } else {
          dataSet.status.status = trait.status.status;
          dataSet.status.message = trait.status.message;
        }
        if (plot.status.status === 200) {
          if (plot.data[0].plots.length > 0) {
            let fieldXY = plot.data[0].plots.filter((plo) => plo.fieldX !== null);
            element.plots = plot.data[0].plots;
            element.isPlotXY = fieldXY.length > 0 ? true : false;
          } else {
            element.isPlotXY = false;
            element.plots = [];
          }
        } else {
          dataSet.status.status = plot.status.status;
          dataSet.status.message = plot.status.message;
        }
      }
    }
  } else {
    dataSet.status.status = result.status.status;
    dataSet.status.message = result.status.message;
  }
  if (dataSet.status.status === 100) {
    dataSet.status.status = 200;
    dataSet.data = result.data;
  }
  return dataSet;
}

/**
 * @description       - Intersection newtriat with an acumulative traits
 * @param {array}     newTrait - New traits to intersect
 * @param {array}     cumulativeTraits - Array of traits that was intersect
 * @returns {array}   - Array with trati that intersec with other trait
 */
export function intersectionTrait(newTrait, cumulativeTraits) {
  let result = { isIntersection: false, traits: cumulativeTraits[0] };
  if (cumulativeTraits.length === 0) {
    result.isIntersection = true;
    result.traits = newTrait;
  } else {
    for (let cumulativeTrait of cumulativeTraits) {
      result.traits = cumulativeTrait.filter((item) =>
        result.traits.map((trait) => trait.variableDbId).includes(item.variableDbId),
      );
    }
    if (
      newTrait.filter((item) =>
        result.traits.map((trait) => trait.variableDbId).includes(item.variableDbId),
      ).length > 0
    ) {
      result.isIntersection = true;
    }
  }
  return result;
}

/**
 * @description     - Find intersection of trait in occurrences
 * @param {array}   occurrences - list of occurrences
 * @returns {array} - Array of trait that intersectioned
 */
export function getIntersectionTraitFromOccurrences(occurrences, responseVaraibleOnlyFloat) {
  let traits = [];
  let traitsOccurrence = occurrences.map((occurrence) => {
    if( responseVaraibleOnlyFloat.isActive && occurrence.traits.length > 0){
      return occurrence.traits.filter((trait) => responseVaraibleOnlyFloat.valueShouldTrue.includes(trait.variableDataType.toUpperCase()));
    }else{
      return occurrence.traits;
    }
  });

  if (traitsOccurrence.length > 0) {
    traits = traitsOccurrence[0];
    for (let cumulativeTrait of traitsOccurrence) {
      traits = cumulativeTrait.filter((item) =>
        traits.map((trait) => trait.variableDbId).includes(item.variableDbId),
      );
    }
  }
  traits = traits.map((trait) => {
    return { id: trait.variableDbId, title: trait.variableAbbrev };
  });
  return traits;
}

/**
 * @description       - Array to define the master detail column of occurrences
 * @returns           - Array to define a column
 * @returns {string}  return0.headerName - Title of the column
 * @returns {string}  return0.field - ID to create operation 
 * @returns {string}  return0.fieldMaster - ID to reference to master column
 * @returns {string}  return0.fieldDetail - ID to reference to detail column
 * @returns {boolean} return0.isHidden - Hidde column
 * @returns {boolean} return0.isSortable - Enable isSortable
 * @returns {boolean} return0.isCanBeHidden - Enable if can be fidden or show the column by default true
 * @returns {number}  return0.width - Mini width
 */
export const occurrenceMaterDetailColumns = [
  {
    headerName: 'Occurrence ID',
    field: 'occurrenceDbId',
    fieldMaster: '',
    fieldDetail: 'occurrenceDbId',
    isHidden: true,
  },
  {
    headerName: 'Experiment ID',
    field: 'experimentDbId',
    fieldMaster: 'experimentDbId',
    fieldDetail: 'experimentDbId',
    isHidden: true,
  },
  {
    headerName: 'Experiment',
    field: 'experimentName',
    fieldMaster: 'experimentName',
    fieldDetail: 'experiment',
    isHidden: false,
    isSortable: true,
    isCanBeHidden: false,
    width: 30,

  },
  {
    headerName: 'Location',
    field: 'location',
    fieldMaster: 'location',
    fieldDetail: 'location',
    isHidden: false,
    isSortable: true,
    isCanBeHidden: false,
    width: 40,
  },
  {
    headerName: 'Occurrence',
    field: 'occurrenceName',
    fieldMaster: 'occurrenceName',
    fieldDetail: 'occurrenceName',
    isHidden: false,
    isSortable: true,
    isCanBeHidden: false,
    width: 40,
  },
  {
    headerName: 'Plant Area',
    field: 'plantArea',
    fieldMaster: 'geospatialObject',
    fieldDetail: 'location',
    isHidden: false,
    isSortable: true,
    width: 40,
  },
  {
    headerName: 'Phase',
    field: 'stageName',
    fieldMaster: 'stageName',
    fieldDetail: 'experimentStage',
    isHidden: false,
    isSortable: true,
    width: 10,
  },
  {
    headerName: 'Year',
    field: 'year',
    fieldMaster: 'experimentYear',
    fieldDetail: 'experimentYear',
    isHidden: false,
    isSortable: true,
    width: 10,
  },
  {
    headerName: 'Design',
    field: 'desing',
    fieldMaster: 'experimentDesignType',
    fieldDetail: 'experimentDesignType',
    isHidden: false,
    isSortable: true,
    width: 10,
  },
  {
    headerName: 'Entry Type (*)',
    field: 'entryType',
    fieldMaster: 'entryType',
    fieldDetail: 'entryType',
    isHidden: false,
    isSortable: true,
    width: 10,
  },
  {
    headerName: 'N. Entries',
    field: 'nEntries',
    fieldMaster: 'entryCount',
    fieldDetail: 'entryCount',
    isHidden: false,
    isSortable: true,
    width: 10,
  },
  {
    headerName: 'Harvest (*)',
    field: 'harvest',
    fieldMaster: 'harvestStatus',
    fieldDetail: 'harvestStatus',
    isHidden: false,
    isSortable: true,
    width: 10,
  },
  {
    headerName: 'Creator',
    field: 'creator',
    fieldMaster: 'creator',
    fieldDetail: 'creator',
    isHidden: false,
    isSortable: true,
    width: 10,
  },
];
