import clientAxios from 'utils/client/clientAxiosAF'
import {
  AF_PROPERTIES_ANALYSIS_CONFIGURATION_DETAIL_SPACIALADJUSTING,
  getUrlAFAnalysisConfigurationDetail,
  ID_SPATIAL_ADJUSTED_NO_SPATIAL_ADJUSTMENT,
} from 'utils/helpers/afHelper'

/**
 * @description       - Search Spatial adjusting from API
 * @param {array}     occurrences - List of occurrences
 * @param {number}    analysisConfiguration - ID of analysis configuration
 * @returns {object}  - Information from API
 * @returns {object}  return0.status  - Object status about the request
 * @returns {number}  return0.status.status [200|201|400]  - Number of status GET - 200 => success, POST 201 => succes, 400 => Not found and 500 => error
 * @returns {string}  return0.status.message - Message of error
 * @returns {array}   return0.data - Data get of API
 * @returns {object}  return0.metadata - General information
 * @returns {object}  return0.metadata.pagination - Information about the pagination
 * @returns {number}  return0.metadata.pagination.totalPages - Total of pages
 * @returns {number}  return0.metadata.pagination.totalCount - Total of element in global request
 */
export async function getSpatialAdjusting(occurrences, analysisConfiguration) {
  let dataSet = {
    status: {
      status: 100,
      message: '',
    },
    data: [],
    metadata: {
      pagination: {
        totalPages: 0,
        totalCount: 0,
      },
    },
  }
  const parameter = {}
  const url = getUrlAFAnalysisConfigurationDetail({
    analysisConfigurationId: analysisConfiguration,
    analysisConfigurationDetail: AF_PROPERTIES_ANALYSIS_CONFIGURATION_DETAIL_SPACIALADJUSTING,
  })
  try {
    let result = await clientAxios.get(url, parameter)
    dataSet.data = result.data.result.data
    //filter
    let occurenceWithOutPlotXY = occurrences.filter(
      (occurrence) => occurrence.isPlotXY === false,
    )
    if (occurenceWithOutPlotXY.length > 0) {
      let filterData = dataSet.data.filter(
        (spatial) =>
          spatial.propertyId === ID_SPATIAL_ADJUSTED_NO_SPATIAL_ADJUSTMENT,
      )
      dataSet.data = filterData
    }
    dataSet.status.status = 200
  } catch (ex) {
    try {
      let result = await clientAxios.get(url, parameter)
      dataSet.data = result.data.result.data
      dataSet.status.status = 200
    } catch (ex) {
      //#CHANGE-LOG
      //console.log(`Error axios - Spacial Adjusting:`.toString(), ex)
      dataSet.status.status = ex.request.status
      if (401 === dataSet.status.status) {
        dataSet.status.message = 'Unauthorized to view Spacial Adjusting.'
      } else if (404 === dataSet.status.status) {
        dataSet.status.message = 'No Spacial Adjusting found.'
      } else if (500 <= dataSet.status.status) {
        dataSet.status.message = 'Unable to view Spacial Adjusting.'
      }
    }
  }
  return dataSet
}
