import clientAxios from 'utils/client/clientAxiosAF'
import {
  AF_PROPERTIES_EXPERIMENT_LOCATION_ANALYSIS_PATTERN,
  ID_EXPERIMENT_LOCATION_ANALYSIS_PATTERN_SINGLE_E__SINGLE_L,
  ID_EXPERIMENT_LOCATION_ANALYSIS_PATTERN_SINGLE_E__MULTI_L,
  GetUrlAFPropertiesSearch,
} from 'utils/helpers/afHelper'

/**
 * @description       - Search in the API experiment location analysis pattern
 * @param {array}     occurrences - Array with all occurrences
 * @returns {object}  - Information from API
 * @returns {object}  return0.status  - Object status about the request
 * @returns {number}  return0.status.status [200|201|400]  - Number of status GET - 200 => success, POST 201 => succes, 400 => Not found and 500 => error
 * @returns {string}  return0.status.message - Message of error
 * @returns {array}   return0.data - Data get of API
 * @returns {object}  return0.metadata - General information
 * @returns {object}  return0.metadata.pagination - Information about the pagination
 * @returns {number}  return0.metadata.pagination.totalPages - Total of pages
 * @returns {number}  return0.metadata.pagination.totalCount - Total of element in global request
 */
export async function getExperimentLocationAnalysisPattern(occurrences) {
  let dataSet = {
    status: {
      status: 100,
      message: '',
    },
    data: [],
    metadata: {
      pagination: {
        totalPages: 0,
        totalCount: 0,
      },
    },
  }
  const parameter = {}
  const url = GetUrlAFPropertiesSearch({
    propertyRoot: AF_PROPERTIES_EXPERIMENT_LOCATION_ANALYSIS_PATTERN,
  })
  try {
    let result = await clientAxios.get(url, parameter)
    dataSet.data = filterData(occurrences, result.data.result.data)
    dataSet.status.status = 200
  } catch (ex) {
    try {
      let result = await clientAxios.get(url, parameter)
      dataSet.data = filterData(occurrences, result.data.result.data)
      dataSet.status.status = 200
    } catch (ex) {
      //#CHANGE-LOG
      //console.log(`Error axios - Experiment analisys pattern:`.toString(),ex)
      dataSet.status.status = ex.request.status
      if (401 === dataSet.status.status) {
        dataSet.status.message =
          'Unauthorized to view Experiment location analisys pattern.'
      } else if (404 === dataSet.status.status) {
        dataSet.status.message = 'No Experiment location analisys pattern found.'
      } else if (500 <= dataSet.status.status) {
        dataSet.status.message =
          'Unable to view Experiment location analisys pattern.'
      }
    }
  }
  return dataSet
}

/**
 * @description     - Eliminate experiment and repeat occurrences and only bring in the ones you have an experiment location analysis pattern.
 * @param {array}   occurrences - New occurrences
 * @param {array}   data - All new occurrences
 * @returns         - Array with experiemnt and occurrences 
 */
function filterData(occurrences, data) {
  const experimentIds = [...new Set(occurrences.map((exp) => exp.experimentDbId))]
  const occurenceIds = [...new Set(occurrences.map((exp) => exp.occurrenceDbId))]
  if (experimentIds.length === 1) {
    data = data.filter((item) => {
      if (
        item.propertyId ===
          ID_EXPERIMENT_LOCATION_ANALYSIS_PATTERN_SINGLE_E__SINGLE_L ||
        item.propertyId === ID_EXPERIMENT_LOCATION_ANALYSIS_PATTERN_SINGLE_E__MULTI_L
      )
        return item
    })
  }
  if (occurenceIds.length === 1) {
    data = data.filter((item) => {
      if (
        item.propertyId ===
        ID_EXPERIMENT_LOCATION_ANALYSIS_PATTERN_SINGLE_E__SINGLE_L
      )
        return item
    })
  }
  return data
}
