import clientAxios from 'utils/client/clientAxiosAF'
import {
  AF_PROPERTIES_ANALYSIS_OBJECTIVE,
  GetUrlAFPropertiesSearch,
} from 'utils/helpers/afHelper'

/**
 * @description       - Search Analysis Objective
 * @param {array}     occurrences - Array with all occurrences
 * @returns {object}  - Information from API
 * @returns {object}  return0.status  - Object status about the request
 * @returns {number}  return0.status.status [200|201|400]  - Number of status GET - 200 => success, POST 201 => succes, 400 => Not found and 500 => error
 * @returns {string}  return0.status.message - Message of error
 * @returns {array}   return0.data - Data get of API
 * @returns {object}  return0.metadata - General information
 * @returns {object}  return0.metadata.pagination - Information about the pagination
 * @returns {number}  return0.metadata.pagination.totalPages - Total of pages
 * @returns {number}  return0.metadata.pagination.totalCount - Total of element in global request
 */
export async function getAnalysisObjective(occurrences) {
  let dataSet = {
    status: {
      status: 100,
      message: '',
    },
    data: [],
    metadata: {
      pagination: {
        totalPages: 0,
        totalCount: 0,
      },
    },
  }
  const parameter = {}
  const url = GetUrlAFPropertiesSearch({
    propertyRoot: AF_PROPERTIES_ANALYSIS_OBJECTIVE,
  })
  try {
    let result = await clientAxios.get(url, parameter)
    dataSet.data = result.data.result.data
    dataSet.status.status = 200
  } catch (ex) {
    try {
      let result = await clientAxios.get(url, parameter)
      dataSet.data = result.data.result.data
      dataSet.status.status = 200
    } catch (ex) {
      //#CHANGE-LOG
      //console.log(`Error axios - Analysis Objective:`.toString(), ex)
      dataSet.status.status = ex.request.status
      if (401 === ex.request.status) {
        dataSet.status.message = 'Unauthorized to view Analysis Objective:'
      } else if (404 === ex.request.status) {
        dataSet.status.message = 'No Analysis Objective: found.'
      } else if (500 <= ex.request.status) {
        dataSet.status.message = 'Unable to view Analysis Objective:.'
      }
    }
  }
  return dataSet
}
