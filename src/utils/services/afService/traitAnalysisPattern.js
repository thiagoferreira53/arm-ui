import clientAxios from 'utils/client/clientAxiosAF'
import {
  AF_PROPERTIES_ANALYSIS_PATTERN,
  GetUrlAFPropertiesSearch,
  ID_TRAIT_ANALYSIS_PATTERN_UNIVARIANTE,
} from 'utils/helpers/afHelper'

/**
 * @description       - Search list of trait analysi pattern from API
 * @param {array}     occurrences - List of occurrences
 * @returns {object}  - Information from API
 * @returns {object}  return0.status  - Object status about the request
 * @returns {number}  return0.status.status [200|201|400]  - Number of status GET - 200 => success, POST 201 => succes, 400 => Not found and 500 => error
 * @returns {string}  return0.status.message - Message of error
 * @returns {array}   return0.data - Data get of API
 * @returns {object}  return0.metadata - General information
 * @returns {object}  return0.metadata.pagination - Information about the pagination
 * @returns {number}  return0.metadata.pagination.totalPages - Total of pages
 * @returns {number}  return0.metadata.pagination.totalCount - Total of element in global request
 */
export async function getTraitAnalysisPattern(occurrences) {
  let dataSet = {
    status: {
      status: 100,
      message: '',
    },
    data: [],
    metadata: {
      pagination: {
        totalPages: 0,
        totalCount: 0,
      },
    },
  }
  const parameter = {}
  const url = GetUrlAFPropertiesSearch({
    propertyRoot: AF_PROPERTIES_ANALYSIS_PATTERN,
  })
  try {
    let result = await clientAxios.get(url, parameter)
    dataSet.data = filterData(result.data.result.data)
    dataSet.status.status = 200
  } catch (ex) {
    try {
      let result = await clientAxios.get(url, parameter)
      dataSet.data = filterData(result.data.result.data)
      dataSet.status.status = 200
    } catch (ex) {
      //#CHANGE-LOG
      //console.log(`Error axios - Trait Analysis Pattern:`.toString(), ex)
      dataSet.status.status = ex.request.status
      if (401 === dataSet.status.status) {
        dataSet.status.message = 'Unauthorized to view Trait Analysis Pattern.'
      } else if (404 === dataSet.status.status) {
        dataSet.status.message = 'No Trait Analysis Pattern found.'
      } else if (500 <= dataSet.status.status) {
        dataSet.status.message = 'Unable to view Trait Analysis Pattern.'
      }
    }
  }
  return dataSet
}

function filterData(data) {
  data = data.filter((item) => {
    if (item.propertyId === ID_TRAIT_ANALYSIS_PATTERN_UNIVARIANTE) return item
  })
  return data
}
