import clientAxios from 'utils/client/clientAxiosAF'
import {
  getExperimentOrLocationFromExperimentLocationAnalysisPattern,
  getUrlAFAnalysisConfigurationSearch,
} from 'utils/helpers/afHelper'

/**
 * @description       - Search the Analysis Configuration
 * @param {object}    traitAnalysisPattern - Trait Analysis pattern
 * @param {object}    experimentLocationAnalysisPattern - Experiment Location Analysis Pattern
 * @param {array}     occurrences - List of occurrences
 * @returns {object}  - Information from API
 * @returns {object}  return0.status  - Object status about the request
 * @returns {number}  return0.status.status [200|201|400]  - Number of status GET - 200 => success, POST 201 => succes, 400 => Not found and 500 => error
 * @returns {string}  return0.status.message - Message of error
 * @returns {array}   return0.data - Data get of API
 * @returns {object}  return0.metadata - General information
 * @returns {object}  return0.metadata.pagination - Information about the pagination
 * @returns {number}  return0.metadata.pagination.totalPages - Total of pages
 * @returns {number}  return0.metadata.pagination.totalCount - Total of element in global request
 */
export async function getAnalysisConfiguration(
  traitAnalysisPattern,
  experimentLocationAnalysisPattern,
  occurrences,
) {
  let dataSet = {
    status: {
      status: 100,
      message: '',
    },
    data: [],
    metadata: {
      pagination: {
        totalPages: 0,
        totalCount: 0,
      },
    },
  }
  const parameter = {}
  const traitPattern = JSON.parse(traitAnalysisPattern).label.toLowerCase()
  const expAnalysisPattern = getExperimentOrLocationFromExperimentLocationAnalysisPattern(
    experimentLocationAnalysisPattern,
  ).experiment
  const locAnalysisPattern = getExperimentOrLocationFromExperimentLocationAnalysisPattern(
    experimentLocationAnalysisPattern,
  ).location
  const design = getDesign(occurrences)
  const url = getUrlAFAnalysisConfigurationSearch({
    traitPattern: traitPattern,
    expAnalysisPattern: expAnalysisPattern,
    locAnalysisPattern: locAnalysisPattern,
    design: design,
  })
  try {
    let result = await clientAxios.get(url, parameter)
    dataSet.data = result.data.result.data
    dataSet.status.status = 200
  } catch (ex) {
    try {
      let result = await clientAxios.get(url, parameter)
      dataSet.data = result.data.result.data
      dataSet.status.status = 200
    } catch (ex) {
      //#CHANGE-LOG
      //console.log(`Error axios - Analysis configuration:`.toString(), ex)
      dataSet.status.status = ex.request.status
      if (401 === dataSet.status.status) {
        dataSet.status.message = 'Unauthorized to view Analysis configuration.'
      } else if (404 === dataSet.status.status) {
        dataSet.status.message = 'No Analysis configuration found.'
      } else if (500 <= dataSet.status.status) {
        dataSet.status.message = 'Unable to view Analysis configuration.'
      }
    }
  }
  return dataSet
}

/**
 * @description       - Find in the occurrences all desing
 * @param {array}     occurrences - Array with all occurrences
 * @returns {string}  - Desing of occurences
 */
function getDesign(occurrences) {
  let design = ''
  if (occurrences.length > 0) {
    design = occurrences[0].experimentDesignType
    const arrayDesign = design.split('-')
    let arrayUpperDesing = []
    if (arrayDesign.length > 0) {
      arrayDesign.forEach((element) => {
        arrayUpperDesing.push(
          `${element.charAt(0).toUpperCase()}${element.slice(1)}`.toString(),
        )
      })
      design = arrayUpperDesing.join('-')
    }
  }
  return design
}

