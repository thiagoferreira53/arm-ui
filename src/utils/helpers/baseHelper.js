/** 
 * @description       - function to get attribute from jsonString
 * @param {string}    jsonString - Json in string to get one attributer
 * @param {string}    attribute - Attribute to search
 * @param {string}    defaultResult [''] - Default return in case that not exist the attribute in json, by default is ''
 * @returns {string}  - Resturn the value of attribute or defaultResult
 */
 export function getAttributeFromJsonString(
  jsonString,
  attribute,
  defaultResult = '',
) {
  let result = defaultResult
  if (
    jsonString !== null &&
    jsonString !== '' &&
    (typeof jsonString === 'string' || jsonString instanceof String)
  ) {
    try {
      let json = JSON.parse(jsonString)
      result = json[attribute]
    } catch (ex) {
      //#CHANGE-LOG
      //console.log(`Error to convert string in json: ${jsonString}`)
    }
  }
  return result
}

/** 
 * @description                 - Convert date in specific format
 * @param {string}              dateString - Date in string to convert
 * @param {DATE_FORMAT_VALUE}   format [monthENUS] - Key to select the transformation
 * @return {string}             - Date convert
 * @returns {string} default    ([2021-08-03T14:22:47.808743, monthENUS] => [11-Agu-2021 14:22])
 */
export function dateFormat(dateString, format) {
  let dateFormat = ''
  switch (format) {
    //dd-MMM-yyyy HH:mm
    case 'monthENUS':
      try {
        const date = new Date(dateString)
        dateFormat = `${date
          .toLocaleString('en-US', {
            day: 'numeric',
          })
          .toString()
          .padStart(2, '0')}-${date
          .toLocaleString('en-US', {
            month: 'short',
          })
          .toString()}-${date.getFullYear()} ${date.getHours()}:${date.getMinutes()}`.toString()
      } catch (exception) {
        //#CHANGE-LOG
        //console.log(`Error to convert date: ${exception}`)
      }
      break
  }
  return dateFormat
}
/** 
 * @description - Array to get format value to function dateFormat  
 */
export const DATE_FORMAT_VALUE = { monthENUS: 'monthENUS' }
