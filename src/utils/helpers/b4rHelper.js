import { TOKEN } from 'utils/config';
import { validateTokenExpiration, ExceptionTokenLive } from './coreHelper';
/**
 * @description - parameter to sort by experiment ID desc
 */
export const SORT_EXPERIMENTDBID_DESC = 'experimentDbId:desc';

/**
 * @description - parameter to sort by occurrences ID desc
 */
export const SORT_OCCURRENCEDBID_DESC = 'occurrenceDbId:desc';

/**
 * @description       - Get URL to search Occurrences in the API
 * @param {number}    limit - Delimite the item to search in the API
 * @param {number}    page - Select the page in pagination 
 * @param {string}    sort - String to sort the data
 * @returns {string}  URL to search occurrences
 */
export function getUrlB4rOccurrencesSearch(limit = -1, page = -1, sort = '') {
  let parameters = new Array();
  let url = 'occurrences-search';

  if (limit !== -1) {
    parameters.push(`limit=${limit}`.toString());
  }
  if (page !== -1) {
    parameters.push(`page=${page}`.toString());
  }
  if (sort !== '') {
    parameters.push(`sort=${sort}`.toString());
  }
  if (parameters.length > 0) {
    url += '?' + parameters.join('&');
  }
  return url;
}

/**
 * @description       - Get URL to search general detail about one occurrences
 * @param {number}    ocurrenceDbId - ID of occurenci to search data about the Plot
 * @returns {string}  - URL to search data about the plot 
 */
export function getUrlB4rOccurrencesPlotDataTableSearch(ocurrenceDbId) {
  let url = `occurrences/${ocurrenceDbId}/plot-data-table-search`.toString();
  return url;
}

/**
 * @description       - Get URL to search detail about one occurrences
 * @param {number}    ocurrenceDbId - ID of occurenci to search data about the Plot
 * @returns {string}  - URL to search data about the plot 
 */
export function getUrlB4rOccurrencesPlotDataSearch(ocurrenceDbId) {
  let url = `occurrences/${ocurrenceDbId}/plot-data-search`.toString();
  return url;
}

/**
 * @description       - Get URL to search plot in a occurrences
 * @param {number}    ocurrenceDbId - ID of occurenci to search data about the Plot
 * @returns {string}  - URL to search search plot 
 */
export function getUrlB4rOccurrencesPlotSearch(ocurrenceDbId) {
  let url = `occurrences/${ocurrenceDbId}/plots-search`.toString();
  return url;
}

/**
 * @description       - Get URL to search Experiment in the API
 * @param {number}    limit - Delimite the item to search in the API
 * @param {number}    page - Select the page in pagination 
 * @param {string}    sort [*|experimentDbId:desc] - String to sort the data, by default experimentDbId:desc
 * @returns {string}  URL to search occurrences
 */
export function getUrlB4rExperimentSearch(
  limit,
  page,
  sort = 'experimentDbId:desc',
) {
  const isValidate = validateTokenExpiration(TOKEN);
  if (!isValidate) {
    throw new ExceptionTokenLive('Session Expired, Please close this tab and log in again.');
  }
  let url = `experiments-search?limit=${limit}&page=${page}&sort=${sort}`.toString();
  return url;
}

/**
 * @description       - Return URL to search varaible detail
 * @param {number}    variableID - ID of variable to search
 * @returns {string}  URL to search variable
 */
export function getUrlVariableDetail(variableID){
  let url = `variables/${variableID}`.toString();
  return url;
}