import { isExpired } from 'react-jwt';

/** 
 * @description - function to validate if beare token is valid in time
 * @returns {boolean} - true if is active other case return false
 */
export function validateTokenExpiration(tokenString) {
  return !isExpired(tokenString);
}

/**
 * @description       - Function to launch a personality exception
 * @param {string}    message - Message to show in a exception
 */
export function ExceptionTokenLive(message) {
  this.message = message;
  this.name = 'ExceptionToken';
}
