import React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter, Route } from 'react-router-dom';
import { EBSMaterialUIProvider } from '@ebs/styleguide';
//import { InstanceContext, InstanceProvider } from '@ebs/cs';
// container
import MainView from 'pages/Main';
import AddView from 'pages/Add';
// mock data for development and test purposes
import { configureStore } from 'reducks';
import { ROUTE_ADD_REQUEST, ROUTE_LIST_REQUEST } from './utils/routes';
const store = configureStore();
// mock data for development and test purposes
//import './mock';

export default function App() {
  return (
    <EBSMaterialUIProvider>
      <Provider store={store}>
        <BrowserRouter>
          <Route
            path='/ba'
            render={({ match }) => (
              <>
                <Route
                  exact
                  path={match.url + ROUTE_ADD_REQUEST}
                  component={AddView}
                />
                <Route
                  exact
                  path={match.url + ROUTE_LIST_REQUEST}
                  component={MainView}
                />
              </>
            )}
          ></Route>
        </BrowserRouter>
      </Provider>
    </EBSMaterialUIProvider>
  );
}
