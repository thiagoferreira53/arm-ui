import React, { useEffect, useState } from 'react';
import {
  ADD_ANALISYSC_ACTION,
  ADD_ANALISYSO_ACTION,
  ADD_EXPERIMENTLAP_ACTION,
  ADD_EXPERIMENT_ACTION,
  ADD_MAINM_ACTION,
  ADD_PREDICTION_ACTION,
  ADD_REMOVE_EXPERIMENT_ACTION,
  ADD_RESPONSEVT_ACTION,
  ADD_SPATIALA_ACTION,
  ADD_TRAITAP_ACTION,
  NEW_REQUEST_ACTION,
} from 'reducks/modules/NewRequest';
import {
  getIntersectionTraitFromOccurrences,
  getOccurrenceWithTraitByExperimentDbId,
  intersectionTrait,
  occurrenceMaterDetailColumns,
} from 'utils/services/b4RService/occurrence';
import { getAnalysisObjective } from 'utils/services/afService/analysisObjective';
import { getTraitAnalysisPattern } from 'utils/services/afService/traitAnalysisPattern';
import { getExperimentLocationAnalysisPattern } from 'utils/services/afService/experimentLocationAnalysisPattern';
import { getAnalysisConfiguration } from 'utils/services/afService/analysisConfiguration';
import { getMainModel } from 'utils/services/afService/mainModel';
import { getPrediction } from 'utils/services/b4RService/default';
import { getSpatialAdjusting } from 'utils/services/afService/spacialAdjusting';
import { CB_API, FILTER_ROW, TOKEN, TOKEN_HELP_POPUP, VERSION } from 'utils/config';
import { getExperiment } from 'utils/services/b4RService/experiment';
import {
  createNewRequestData,
  postSaveNewRequest,
} from 'utils/services/afService/request';
import { getPredictionByExpLoc } from 'utils/helpers/afHelper';
import AddRequestTemplate from 'components/ui/templates/AddRequest';
import { getUserProfileByToken } from 'utils/services/coreService/user';
import { decodeToken } from 'react-jwt';
import { Container } from '@material-ui/core';
import { ROUTE_ADD_REQUEST, ROUTE_LIST_REQUEST } from 'utils/routes';

/**
 * @description         - Pages to add new request
 * @property {state}    userProfileStatus - State to save all data of the user profile
 * @property {state}    requestorProfile - State to save only the necessary data of the user profile
 * @property {object}   titleData - Object with the configuration of title
 * @property {object}   selects - Object with the configuration of combo box
 * @property {object}   setting - Object with the configuration to setting all component
 * @property {object}   experiment - Object with the configuration of experiment and occurrence
 * @property {object}   request - Object with the configuration of the request
 * @property {void}     useEffect - Get the user profile
 * @property {string}   data-testid - Id to use inside add.test.js file.
 * @returns {component} Page to add new request
 */
export default function Addview() {
  const [userProfileStatus, setUserProfileStatus] = useState(100);
  const [requestorProfile, setRequestorProfile] = useState({});
  const titleData = {
    title: 'Analytics Request Manager',
    subtitle: 'Add New Request',
    note: '',
  };
  const selects = {
    responseVariableTrait: {
      responseVariableTrait: [],
      functionData: getIntersectionTraitFromOccurrences,
      actionSave: ADD_RESPONSEVT_ACTION,
    },
    analysisObjective: {
      analysisObjective: '',
      functionData: getAnalysisObjective,
      title: 'Analysis objective',
      id: 'ao1',
      actionSave: ADD_ANALISYSO_ACTION,
    },
    traitAnalysisPattern: {
      traitAnalysisPattern: '',
      functionData: getTraitAnalysisPattern,
      title: 'Trait Analysis Pattern',
      id: 'tap1',
      actionSave: ADD_TRAITAP_ACTION,
    },
    experimentLocationAnalysisPattern: {
      experimentLocationAnalysisPattern: '',
      functionData: getExperimentLocationAnalysisPattern,
      title: 'Experiment Location Analysis Pattern',
      id: 'elap1',
      actionSave: ADD_EXPERIMENTLAP_ACTION,
    },
    analysisConfiguration: {
      analysisConfiguration: '',
      functionData: getAnalysisConfiguration,
      title: 'Analysis Configuration',
      id: 'ac1',
      actionSave: ADD_ANALISYSC_ACTION,
    },
    mainModel: {
      mainModel: '',
      functionData: getMainModel,
      title: 'Main Model',
      id: 'mm1',
      actionSave: ADD_MAINM_ACTION,
    },
    prediction: {
      prediction: '',
      functionData: getPrediction,
      title: 'Prediction',
      id: 'p1',
      actionSave: ADD_PREDICTION_ACTION,
    },
    spatialAdjusting: {
      spatialAdjusting: '',
      functionData: getSpatialAdjusting,
      title: 'Spatial Adjusting',
      id: 'sp1',
      actionSave: ADD_SPATIALA_ACTION,
    },
  };
  const setting = {
    tokenHelpPopUp: TOKEN_HELP_POPUP === 1 ? true : false,
    action: {
      NEW_REQUEST_ACTION: NEW_REQUEST_ACTION,
    },
    routes: {
      rotuelist: `/ba${ROUTE_LIST_REQUEST}`,
      routeAdd: `/ba${ROUTE_ADD_REQUEST}`,
    },
    filterRow: FILTER_ROW,
  };
  const experiment = {
    functionData: getExperiment,
    experiment: [],
    columns: occurrenceMaterDetailColumns,
    functionDataDetails: getOccurrenceWithTraitByExperimentDbId,
    actionSave: ADD_EXPERIMENT_ACTION,
    accionAddRemove: ADD_REMOVE_EXPERIMENT_ACTION,
    functionGetTrait: intersectionTrait,
  };
  const request = {
    functionSaveRequest: postSaveNewRequest,
    dataSourceUrl: CB_API,
    functionCreateNewRequest: createNewRequestData,
    functionGetPrediction: getPredictionByExpLoc,
    requestor: requestorProfile,
  };
  useEffect(() => {
    if (userProfileStatus === 100) {
      const decodedToken = decodeToken(TOKEN);
      const token =
        decodedToken !== null
          ? decodedToken['http://wso2.org/claims/emailaddress']
          : '';
      getUserProfileByToken(token).then((result) => {
        setUserProfileStatus(result.status.status);
        let requestor = {
          message: result.status.message,
          requestorId: result.data.requestorId,
          institute: result.data.institute,
          analysisType: 'ANALYZE',
          dataSource: 'EBS',
        };
        setRequestorProfile(requestor);
      });
    }
  }, [
    userProfileStatus,
    setUserProfileStatus,
    setRequestorProfile,
    requestorProfile,
  ]);
  return (
    <Container data-testid={'AddTestId'} component='main' maxWidth={'xl'}>
      <AddRequestTemplate
        setting={setting}
        titleData={titleData}
        selects={selects}
        experiment={experiment}
        request={request}
        version={VERSION}
      />
    </Container>
  );
}
Addview.prototype = {};
