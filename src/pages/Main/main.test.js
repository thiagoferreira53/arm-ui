import React from 'react';
import ReactDOM from 'react-dom';
// Component to be Test
import Main from './mainview';
// Test Library
import { render, cleanup, act } from '@testing-library/react';
import '@testing-library/dom';
import '@testing-library/jest-dom/extend-expect';
import * as user from '../../utils/services/coreService/user';
import * as ApolloMock from '@apollo/client';

afterEach(cleanup);
import 'regenerator-runtime/runtime';
// Props to send component to be rendered
const props = {};
jest.mock('../../reducks/modules/NewRequest', () => {
  return { NEW_REQUEST_ACTION: 1 };
});
jest.spyOn(user, 'getUserProfileByToken').mockImplementation(() =>
  Promise.resolve({
    status: {
      status: 100,
      message: '',
    },
    data: [],
    metadata: {
      pagination: {
        totalPages: 0,
        totalCount: 0,
      },
    },
  }),
);
jest.spyOn(user, 'getUserProfileById').mockImplementation(() =>
  Promise.resolve({
    status: {
      status: 100,
      message: '',
    },
    data: [],
    metadata: {
      pagination: {
        totalPages: 0,
        totalCount: 0,
      },
    },
  }),
);
jest.mock('../../components/ui/templates/MainRequest', () => () => {
  return <div></div>;
});
jest.mock('@apollo/client', () => {
  return {
    ApolloClient: jest.fn().mockImplementation(() => {
      return {};
    }),
    ApolloLink: jest.fn().mockImplementation(() => {
      return {};
    }),
    from: jest.fn().mockImplementation(() => {
      return {};
    }),
    HttpLink: jest.fn().mockImplementation(() => {
      return {};
    }),
    InMemoryCache: jest.fn().mockImplementation(() => {
      return {};
    }),
  };
});
test('Report name', () => {
  const div = document.createElement('div');
  act(() => {
    ReactDOM.render(<Main {...props}></Main>, div);
  });
});

test('Render correctly', () => {
  act(() => {
    const { getByTestId } = render(<Main {...props}></Main>);
    expect(getByTestId('MainTestId')).toBeInTheDocument();
  });
});
