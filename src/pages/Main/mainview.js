import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import Container from '@material-ui/core/Container';
import MainRequestTemplate from 'components/ui/templates/MainRequest';
import {
  getRequestList,
  getRequestListColumns,
  getRequestID,
} from 'utils/services/afService/request';
import { NEW_REQUEST_ACTION } from 'reducks/modules/NewRequest';
import { BA_API, CB_API, TOKEN, TOKEN_HELP_POPUP, TOKEN_KEY_STORAGE_LOCATION, VERSION } from 'utils/config';
import { decodeToken } from 'react-jwt';
import {
  getUserProfileByToken,
  getUserProfileById,
} from 'utils/services/coreService/user';
import {
  STATE_REQUEST_COMPLETE,
  STATE_REQUEST_INPROGRESS,
  STATE_REQUEST_PENDING,
  STATE_REQUEST_FAILURE,
} from 'utils/helpers/afHelper';
import { Alert, AlertTitle } from '@material-ui/lab';
import { ROUTE_ADD_REQUEST } from 'utils/routes';
import { Typography } from '@material-ui/core';

/**
 * @description         Page to display all request that was created
 * @param {object}      props - Children properties
 * @property {number}   userProfileStatus - State to save all data of the user profile
 * @property {object}   requestorProfile - Object to save only the necessary data of the user profile
 * @property {string}   requestorProfile.message - Error message in the API to get the user profile
 * @property {string}   requestorProfile.requestorId - ID the user
 * @property {string}   requestorProfile.institute - Institute assigned to user
 * @property {object}   navBar - Object to config a navigation bar
 * @property {array}    navBar.buttons - MetaInformation to build a button
 * @property {string}   navBar.buttons[].id - ID to identify the button component
 * @property {string}   navBar.buttons[].label - Labe to put into the button component
 * @property {bool}     navBar.buttons[].active - Enable or disable the button component
 * @property {object}   navBar.buttons[].parameter - Object with parameter to work the button component
 * @property {string}   navBar.buttons[].parameter.idStorage - ID to search the access token into localStorage
 * @property {string}   navBar.buttons[].parameter.uriParent - URL to get the access token
 * @property {object}   requestList - Object to config element in request
 * @property {function} requestList.functionData - Get all the information for request created by the user
 * @property {string}   requestList.title - Title of the page
 * @property {array}    requestList.columnsData - Array of the metadata of columns
 * @property {string}   requestList.columnsData[].Header - Title of the column
 * @property {string}   requestList.columnsData[].accessor - ID to look up the value in the row array
 * @property {boolean}  requestList.columnsData[].filter - Flag to enable or disable the filter option
 * @property {boolean}  requestList.columnsData[].hidden - Flag to hidden or show the columns
 * @property {array}    requestList.buttons - Array with information for all buttons
 * @property {string}   requestList.buttons[].id - ID to identify the button component
 * @property {string}   requestList.buttons[].title - Title to display into the button component
 * @property {string}   requestList.buttons[].help - Text to display tool tip into the button component
 * @property {string}   requestList.buttons[].color - Color to put into the button component
 * @property {object}   requestList.buttons[].parameters - Parameter for the button component to work with it
 * @property {object}   requestList.stateRequest - Case to use to convert the status of an application to a badge
 * @property {string}   requestList.stateRequest.complete - Complete status
 * @property {string}   requestList.stateRequest.inprogress - In progress status
 * @property {string}   requestList.stateRequest.pending - Pending status
 * @property {string}   requestList.stateRequest.failure - Failure status
 * @property {object}   requestList.requestorProfile - Object to save only the necessary data of the user profile
 * @property {function} requestList.requestIDFunction - Function to search one request by id
 * @property {function} requestList.profileIdFunction - Function to search one profile user by id
 * @property {string}   requestList.baseUrlAF - Base of path to access the services AF and download the result files
 * @property {string}   data-testId - Id to use inside main.test.js file.
 * @function {void}     getRequestor - Function to get the requestor information
 * @function {void}     useEffect - Load requestor data
 * @returns {component} return all pages from request that was created
 */
export default function MainView(props) {
  const { ...rest } = props;
  const [userProfileStatus, setUserProfileStatus] = useState(100);
  const [requestorProfile, setRequestorProfile] = useState({});
  const navBar = {
    buttons: [
      {
        id: 'getTokenId',
        label: 'Get Token',
        active: TOKEN_HELP_POPUP === 1 ? true : false,
        parameter: {
          idStorage: TOKEN_KEY_STORAGE_LOCATION,
          uriParent: CB_API,
        },
      },
    ],
  };
  const requestList = {
    functionData: getRequestList,
    columnsData: getRequestListColumns,
    title: 'List of Job created',
    buttons: [
      {
        id: 'newRequest',
        title: 'Add new analysis request',
        help: 'Add new analysis request',
        color: 'inherit',
        parameters: {
          actionSave: NEW_REQUEST_ACTION,
          ruteToPage: `/ba${ROUTE_ADD_REQUEST}`,
        },
      },
      {
        id: 'removeRequest',
        title: 'REMOVE REQUEST',
        help: 'Remove request',
        color: 'inherit',
        parameters: {
          actionSave: '',
        },
      },
      {
        id: 'refresh',
        title: 'REFRESH',
        help: 'Refresh the grid',
        color: 'inherit',
        parameters: {},
      },
      {
        id: 'download',
        title: 'DOWNLOAD',
        help: 'Download',
        color: 'inherit',
        parameters: {
          baseUrlAF: BA_API,
        },
      },
      {
        id: 'notice',
        title: 'Notice',
        help: 'Notice',
        color: 'inherit',
        parameters: {
          seconds: 40,
          localStorageId: 'ba-notific',
        },
      },
    ],
    stateRequest: {
      complete: STATE_REQUEST_COMPLETE,
      inprogress: STATE_REQUEST_INPROGRESS,
      pending: STATE_REQUEST_PENDING,
      failure: STATE_REQUEST_FAILURE,
    },
    requestorProfile: requestorProfile,
    requetsIDFunction: getRequestID,
    profileIdFunction: getUserProfileById,
  };

  const getRequestor = async () => {
    const decodedToken = decodeToken(TOKEN);
    const token =
      decodedToken !== null
        ? decodedToken['http://wso2.org/claims/emailaddress']
        : '';
    const result = await getUserProfileByToken(token);
    //#CHANGE-LOG
    //console.log('Result profile', result);
    setUserProfileStatus(result.status.status);
    let requestor = {
      message: result.status.message,
      requestorId: result.data.requestorId,
      institute: result.data.institute,
    };
    setRequestorProfile(requestor);
  };

  useEffect(() => {
    if (userProfileStatus === 100) {
      getRequestor();
    }
  }, [
    userProfileStatus,
    setUserProfileStatus,
    setRequestorProfile,
    requestorProfile,
  ]);

  return (
    <Container data-testid={'MainTestId'} component='main' maxWidth={'xl'}>
      {requestorProfile.requestorId !== undefined ? (
        <MainRequestTemplate
          navbar={navBar}
          requestlist={requestList}
          version={VERSION}
        />
      ) : (
        <Alert severity='error'>
          <AlertTitle>
            {requestorProfile.message !== undefined ? 'Error' : ''}
          </AlertTitle>
          {requestorProfile.message !== undefined ? requestorProfile.message : ''}
        </Alert>
      )}
    </Container>
  );
}
// Type and required properties
MainView.propTypes = {};
// Default properties
MainView.defaultProps = {};
