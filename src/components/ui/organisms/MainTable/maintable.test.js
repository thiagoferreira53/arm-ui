import React from 'react'
import ReactDOM from 'react-dom'
// Component to be Test
import MainTable from './maintable'
// Test Library
import { render, cleanup } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
import * as reactRedux from 'react-redux'

afterEach(cleanup)
// Props to send component to be rendered
const props = {
  functiondata: jest.fn(),
  title: '',
  columnsdata: [],
  buttons: [
    {
      id: '',
      label: '',
      active: true,
      parameter: '',
    },
  ],
  staterequest: {
    complete: '',
    processing: '',
    draft: '',
  },
  classes: {
    class: 'class',
  },
  requestorprofile: {},
  requestidfunction: jest.fn(),
  profileidfunction: jest.fn(),
}
// mock to use dispatch
const mockDispatch = jest.fn()
jest.mock('react-redux', () => ({
  useSelector: jest.fn(),
  useDispatch: () => mockDispatch,
}))
const useSelectorMock = jest.spyOn(reactRedux, 'useSelector')
const useDispatchMock = jest.spyOn(reactRedux, 'useDispatch')
jest.mock('../../molecules/MainDataGrid', () => () => {
  return <div></div>
})

test('Report name', () => {
  const div = document.createElement('div')
  ReactDOM.render(<MainTable {...props}></MainTable>, div)
})

test('Render correctly', () => {
  const { getByTestId } = render(<MainTable {...props}></MainTable>)
  expect(getByTestId('MainTableTestId')).toBeInTheDocument()
})
