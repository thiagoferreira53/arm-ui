import React from 'react';
import ReactDOM, { unmountComponentAtNode } from 'react-dom';
// Component to be Test
import AddRequestTab from './addrequesttab';
// Test Library
import { render, cleanup, screen } from '@testing-library/react';
import '@testing-library/dom';
import '@testing-library/jest-dom/extend-expect';
import * as reactRedux from 'react-redux';
import { act } from 'react-dom/test-utils';

let container = null;
beforeEach(() => {
  container = document.createElement('div');
  document.body.appendChild(container);
});
afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});
// Props to send component to be rendered
const props = {
  setting: {
    tokenHelpPopUp: false,
    action: {
      NEW_REQUEST_ACTION: '',
    },
    routes: {
      rotuelist: '',
    },
    filterRow: {
      keyStorageLocation: 'ba-filter-row',
      configuration: {
        sameExperiemnt: {
          isActive: true,
          isEnable: false,
        },
        responseVaraibleOnlyFloat: {
          isActive: true,
          isEnable: true,
        },
      },
    },
  },
  selects: {
    responseVariableTrait: {
      responseVariableTrait: [],
      functionData: jest.fn().mockImplementation(() =>
        Promise.resolve({
          status: {
            status: 100,
            message: '',
          },
          data: [],
          metadata: {
            pagination: {
              totalPages: 0,
              totalCount: 0,
            },
          },
        }),
      ),
      actionSave: '',
    },
    analysisObjective: {
      analysisObjective: '',
      functionData: jest.fn().mockImplementation(() =>
        Promise.resolve({
          status: {
            status: 100,
            message: '',
          },
          data: [],
          metadata: {
            pagination: {
              totalPages: 0,
              totalCount: 0,
            },
          },
        }),
      ),
      title: '',
      id: 'ao1',
      actionSave: '',
    },
    traitAnalysisPattern: {
      traitAnalysisPattern: '',
      functionData: jest.fn().mockImplementation(() =>
        Promise.resolve({
          status: {
            status: 100,
            message: '',
          },
          data: [],
          metadata: {
            pagination: {
              totalPages: 0,
              totalCount: 0,
            },
          },
        }),
      ),
      title: '',
      id: 'tap1',
      actionSave: '',
    },
    experimentLocationAnalysisPattern: {
      experimentLocationAnalysisPattern: '',
      functionData: jest.fn().mockImplementation(() =>
        Promise.resolve({
          status: {
            status: 100,
            message: '',
          },
          data: [],
          metadata: {
            pagination: {
              totalPages: 0,
              totalCount: 0,
            },
          },
        }),
      ),
      title: '',
      id: 'elap1',
      actionSave: '',
    },
    analysisConfiguration: {
      analysisConfiguration: '',
      functionData: jest.fn().mockImplementation(() =>
        Promise.resolve({
          status: {
            status: 100,
            message: '',
          },
          data: [],
          metadata: {
            pagination: {
              totalPages: 0,
              totalCount: 0,
            },
          },
        }),
      ),
      title: '',
      id: 'ac1',
      actionSave: '',
    },
    mainModel: {
      mainModel: '',
      functionData: jest.fn().mockImplementation(() =>
        Promise.resolve({
          status: {
            status: 100,
            message: '',
          },
          data: [],
          metadata: {
            pagination: {
              totalPages: 0,
              totalCount: 0,
            },
          },
        }),
      ),
      title: '',
      id: 'mm1',
      actionSave: '',
    },
    prediction: {
      prediction: '',
      functionData: jest.fn(),
      title: '',
      id: 'p1',
      actionSave: '',
    },
    spatialAdjusting: {
      spatialAdjusting: '',
      functionData: jest.fn().mockImplementation(() =>
        Promise.resolve({
          status: {
            status: 100,
            message: '',
          },
          data: [],
          metadata: {
            pagination: {
              totalPages: 0,
              totalCount: 0,
            },
          },
        }),
      ),
      title: '',
      id: 'sp1',
      actionSave: '',
    },
  },
  experiment: {
    experiment: [],
    columns: [],
  },
  classes: {
    class: 'class',
  },
  subtitle: '',
  request: {
    functionSaveRequest: jest.fn(),
    dataSourceUrl: '',
    functionCreateNewRequest: jest.fn().mockImplementation(() =>
      Promise.resolve({
        status: {
          status: 100,
          message: '',
        },
        data: [],
        metadata: {
          pagination: {
            totalPages: 0,
            totalCount: 0,
          },
        },
      }),
    ),
    functionGetPrediction: jest.fn(),
    requestor: {
      message: '',
      requestorId: 1,
      institute: '',
      crop: '',
      analysisType: 'ANALYZE',
      dataSource: 'EBS',
    },
  },
};
// mock to use dispatch
const mockDispatch = jest.fn();
jest.mock('react-redux', () => ({
  useSelector: jest.fn(),
  useDispatch: () => mockDispatch,
}));
const useSelectorMock = jest.spyOn(reactRedux, 'useSelector');
const useDispatchMock = jest.spyOn(reactRedux, 'useDispatch');
useSelectorMock.mockReturnValue({ state: { newRequest: { experiment: [] } } });
jest.mock('../../molecules/TabPanel/ExperimentsTabPanel', () => () => {
  return <div></div>;
});
jest.mock('../../molecules/TabPanel/MenuButtonTab', () => () => {
  return <div></div>;
});
jest.mock('../../molecules/TabPanel/SettingsTabPanel', () => () => {
  return <div></div>;
});
jest.mock('../../molecules/TabPanel/ParametersTabPanel', () => () => {
  return <div></div>;
});

test('Report name', async () => {
  const div = document.createElement('div');
  await act(async () => {
    ReactDOM.render(<AddRequestTab {...props}></AddRequestTab>, div);
  });
});

describe('Add request tad - test', () => {
  it('Must be search by test Id', async () => {
    await act(async () => {
      render(<AddRequestTab {...props}></AddRequestTab>, container);
    });
    expect(screen.getByTestId('AddRequestTabTestId')).toBeInTheDocument();
  });
});
