import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
// CORE COMPONENTS AND MOLECULES TO USE
import { Grid, Tab, Tabs } from '@material-ui/core';
import SubTitleMolecule from 'components/ui/molecules/SubTitle';
import { useDispatch, useSelector } from 'react-redux';
import ExperimentsTabPanelMolecule from 'components/ui/molecules/TabPanel/ExperimentsTabPanel';
import MenuButtonTabMolecule from 'components/ui/molecules/TabPanel/MenuButtonTab';
import SettingsTabPanelMolecule from 'components/ui/molecules/TabPanel/SettingsTabPanel';
import ParametersTabPanelMolecule from 'components/ui/molecules/TabPanel/ParametersTabPanel';
import { TOKEN } from 'utils/config';
import { getAttributeFromJsonString } from 'utils/helpers/baseHelper';
import { useHistory } from 'react-router-dom/cjs/react-router-dom.min';

function getMessageFinish(selects, experiment) {
  let messages = [
    {
      step: 0,
      message:
        experiment.experiment.length === 0
          ? 'Please select one or more occurrence'
          : '',
      data: {
        experiment: experiment.experiment.length,
        experimentNameFirst:
          experiment.experiment.length > 0
            ? experiment.experiment[0].experiment
            : '',
      },
    },
    {
      step: 1,
      message: getMessageStep1(selects),
      data: {},
    },
    {
      step: 2,
      message: getMessageStep2(selects),
      data: {},
    },
    {
      step: 3,
      message: '',
      data: {},
    },
  ];
  return messages;
}
function getMessageStep1(selects) {
  if (selects.responseVariableTrait.responseVariableTrait.length === 0)
    return 'Please select one Response Variables';
  if (
    selects.analysisObjective.analysisObjective === 0 ||
    selects.analysisObjective.analysisObjective === ''
  )
    return 'Please select one Analysis Objective';
  return '';
}
function getMessageStep2(selects) {
  if (
    selects.traitAnalysisPattern.traitAnalysisPattern === 0 ||
    selects.traitAnalysisPattern.traitAnalysisPattern === ''
  )
    return 'Please select Analysis Pattern';
  if (
    selects.experimentLocationAnalysisPattern.experimentLocationAnalysisPattern ===
      0 ||
    selects.experimentLocationAnalysisPattern.experimentLocationAnalysisPattern ===
      ''
  )
    return 'Please select Experiment Location Analysis Pattern';
  if (
    selects.analysisConfiguration.analysisConfiguration === 0 ||
    selects.analysisConfiguration.analysisConfiguration === ''
  )
    return 'Please select Analysis Configuration';
  if (selects.mainModel.mainModel === 0 || selects.mainModel.mainModel === '')
    return 'Please select Main Model';
  //# To ability prediction
  //if (selects.prediction.prediction === 0) return 'Please select Prediction'
  if (
    selects.spatialAdjusting.spatialAdjusting === 0 ||
    selects.spatialAdjusting.spatialAdjusting === ''
  )
    return 'Please select Spatial Adjusting';
  return '';
}
function getMenuButtonConfig(messages, value, tapsLength) {
  let message = messages.find((x) => x.step === value);
  let configButton = {
    backDisable: value > 0 ? false : true,
    nextDisable: message.message === '' ? false : true,
    nextText: value + 1 === tapsLength ? 'Submit Analysis Request' : 'Next',
  };
  return configButton;
}

//MAIN FUNCTION
/**
 * @description           Form on step to receive the information about the request
 * @param {object}        props - Component properties
 * @param {reference}     ref - Reference made by React.forward
 * @param {object}        values - Values to save the request.
 * @param {object}        values.occurences - Occurrences select by the user.
 * @param {object}        setting -Object with all parameter to other component.
 * @param {object}        selects -Object with all parameter and values to combo box component.
 * @param {object}        experiment -Object with all parameter and value of experiment and occurrences.
 * @param {string}        subtitle -Object with all values to navigation bar component.
 * @param {object}        request -Object to refer to the requestor data.
 * @param {object}        classes -Classes to put specific style with makeStyle.
 * @property {int}        value -UseState Current index tab.
 * @property {string}     messageError -UseState Message of error.
 * @property {object}     newRequestData -UseState All data to create the new request.
 * @property {array}      tabsArray -List of the tabs.
 * @property {array}      experiment -Star all values in the redux.
 * @property {array}      messages -Get object message into the tabs.
 * @property {string}     severity -Severity icon
 * @property {object}     message -Get the error message.
 * @property {object}     configButton -Get button the configuration to tabs.
 * @function {void}       handleChange -Function to change the current tab.
 * @function {void}       alertMessage -Display a message error to create a new request.
 * @function {void}       useEffect -Validate if exist requestor id.
 * @function {void}       useEffect -To start the component, create all the data to create the request.
 * @function {array}      getMessageFinish -Find all errors in each of the tabs.
 * @function {string}     getMessageStep1 -Get error on specific tab 1.
 * @function {string}     getMessageStep2 -Get error on specific tab 2.
 * @function {object}     getMenuButtonConfig -Get the information for the component MenuButtonTab.
 * @property {string}     data-testId -Id to use inside addRequestTab.test.js file.
 * @returns {component}   Tab to select occurrences asn configuration to create the request
 */
const AddRequestTabOrganism = React.forwardRef((props, ref) => {
  const {
    children,
    setting,
    selects,
    experiment,
    subtitle,
    classes,
    request,
    ...rest
  } = props;
  const [value, setValue] = useState(0);
  const [messageError, setMessageError] = useState('');
  const [newRequestData, setNewRequestData] = useState({});
  const [isSearchRowFilter, setIsSearchRowFilter] = useState(true);
  const [filterEnableRow, setFilterEnableRow] = useState(searchLocationRowFilter());
  const tabsArray = ['Experiments', 'Settings', 'Parameters'];
  const dispatch = useDispatch();
  const history = useHistory();
  
  const localStorare_setFilterRow = () => {
    const filterLocal = localStorage.getItem(setting.filterRow.keyStorageLocation);
    if (filterLocal === undefined || filterLocal === null || filterLocal === '') {
      localStorage.setItem(
        setting.filterRow.keyStorageLocation,
        JSON.stringify(setting.filterRow.configuration),
      );
    }
  };
  useEffect(() => {
    if (request.requestor !== undefined && request.requestor.message !== '') {
      setMessageError(request.requestor.message);
    }
  }, [request, setMessageError]);
  useEffect(() => {
    let isMounted = true;
    request
      .functionCreateNewRequest({
        dataSourceUrl: request.dataSourceUrl,
        token: TOKEN,
        experiments: experiment.experiment,
        responseVariableTraits: selects.responseVariableTrait.responseVariableTrait,
        analysisObjective: selects.analysisObjective.analysisObjective,
        traitAnalysisPattern: getAttributeFromJsonString(
          selects.traitAnalysisPattern.traitAnalysisPattern,
          'id',
        ),
        experimentLocationAnalysisPattern: getAttributeFromJsonString(
          selects.experimentLocationAnalysisPattern
            .experimentLocationAnalysisPattern,
          'id',
        ),
        analysisConfiguration: selects.analysisConfiguration.analysisConfiguration,
        spatialAdjusting: selects.spatialAdjusting.spatialAdjusting,
        prediction: request.functionGetPrediction(
          selects.experimentLocationAnalysisPattern
            .experimentLocationAnalysisPattern,
        ),
        dataSource: request.requestor.dataSource,
        institute: request.requestor.institute,
        analysisType: request.requestor.analysisType,
        requestorId: request.requestor.requestorId,
        mainModel: selects.mainModel.mainModel,
      })
      .then((requestData) => {
        if (isMounted) {
          setNewRequestData(requestData);
        }
      });
    return () => {
      isMounted = false;
    };
  }, [setNewRequestData, request, experiment, selects]);
  useEffect(() => {
    let isMounted = true;
    if(isMounted){
      localStorare_setFilterRow();
      if(isSearchRowFilter){
        setFilterEnableRow(searchLocationRowFilter());
        setIsSearchRowFilter(false)
      }
    }
    return () => {
      isMounted = false;
    };
  }, [localStorare_setFilterRow, isSearchRowFilter, setFilterEnableRow, setIsSearchRowFilter ]);
  experiment.experiment = useSelector((state) => state.newRequest.experiment);
  selects.responseVariableTrait.responseVariableTrait = useSelector(
    (state) => state.newRequest.responseVariableTrait,
  );
  selects.analysisObjective.analysisObjective = useSelector(
    (state) => state.newRequest.analysisObjective,
  );
  selects.traitAnalysisPattern.traitAnalysisPattern = useSelector(
    (state) => state.newRequest.traitAnalysisPattern,
  );
  selects.experimentLocationAnalysisPattern.experimentLocationAnalysisPattern =
    useSelector((state) => state.newRequest.experimentLocationAnalysisPattern);
  selects.analysisConfiguration.analysisConfiguration = useSelector(
    (state) => state.newRequest.analysisConfiguration,
  );
  selects.mainModel.mainModel = useSelector((state) => state.newRequest.mainModel);
  //selects.prediction.prediction = useSelector((state) => state.newRequest.prediction)
  selects.spatialAdjusting.spatialAdjusting = useSelector(
    (state) => state.newRequest.spatialAdjusting,
  );
  const messages = getMessageFinish(selects, experiment);
  let severity = 'warning';
  let message = messages.find((x) => x.step === value);
  const configButton = getMenuButtonConfig(messages, value, tabsArray.length);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  const alertMessage = (messageC, severityC) => {
    alert(messageC);
  };
  const localStorage_overwriteFilterRow = (filter) => {
    dispatch({
      type: setting.action.NEW_REQUEST_ACTION,
      payload: '',
    });
    let filterRow = setting.filterRow.configuration;
    try {
      filterRow = JSON.parse(
        localStorage.getItem(setting.filterRow.keyStorageLocation),
      );
      filter.forEach((element) => {
        if (element.new !== null) filterRow[element.id].isActive = element.new;
      });
    } catch (ex) {}
    localStorage.setItem(
      setting.filterRow.keyStorageLocation,
      JSON.stringify(filterRow),
    );
    setValue(0);
    setFilterEnableRow(filterRow);
  };
  function searchLocationRowFilter (){
    const stringJson = localStorage.getItem(setting.filterRow.keyStorageLocation);
    return stringJson !== undefined && stringJson !== null && stringJson !== ''
      ? JSON.parse(stringJson)
      : setting.filterRow.configuration;
  };

  return (
    <Grid
      ref={ref}
      data-testid={'AddRequestTabTestId'}
      className={classes.grid}
      container
    >
      <Grid item xs={9} className={classes.gridSubtitle}>
        <SubTitleMolecule
          titleText={subtitle}
          message={message.message}
          messageerror={messageError}
          severity={severity}
        />
      </Grid>
      <Grid item xs={3} className={classes.gridButton}>
        <MenuButtonTabMolecule
          cancelAction={setting.action.NEW_REQUEST_ACTION}
          cancelRoute={setting.routes.rotuelist}
          functionSetTab={setValue}
          nextDisable={configButton.nextDisable}
          backDisable={configButton.backDisable}
          nextText={configButton.nextText}
          tapsLength={tabsArray.length}
          valuetab={value}
          newrequestdata={newRequestData}
          routeaftersave={setting.routes.rotuelist}
          functionsave={request.functionSaveRequest}
          alertmessage={alertMessage}
        />
      </Grid>
      <Grid item xs={12} className={classes.gridTable}>
        <Tabs
          value={value}
          indicatorColor='primary'
          textColor='primary'
          onChange={handleChange}
          aria-label='Add New Request'
        >
          {tabsArray.map((tab, index) => {
            const label = `${index + 1} ${tab}`.toString();
            return <Tab value={index} label={label} disabled key={label} />;
          })}
        </Tabs>
        <ExperimentsTabPanelMolecule
          value={value}
          index={0}
          experiment={experiment}
          classes={classes}
          filterenablerow={filterEnableRow}
          functionresetfilterrow={localStorage_overwriteFilterRow}
        />
        <SettingsTabPanelMolecule
          value={value}
          index={1}
          selects={selects}
          experiment={experiment}
          classes={classes}
          filterenablerow={filterEnableRow}
        />
        <ParametersTabPanelMolecule
          value={value}
          index={2}
          selects={selects}
          experiment={experiment}
          classes={classes}
        />
      </Grid>
    </Grid>
  );
});
// Type and required properties
AddRequestTabOrganism.propTypes = {
  setting: PropTypes.object.isRequired,
  selects: PropTypes.object.isRequired,
  experiment: PropTypes.object.isRequired,
  request: PropTypes.object.isRequired,
  subtitle: PropTypes.string.isRequired,
};
// Default properties
AddRequestTabOrganism.defaultProps = {
  subtitle: '',
};

export default AddRequestTabOrganism;
