import React from 'react'
import ReactDOM from 'react-dom'
// Component to be Test
import Footer from './footer'
// Test Library
import { render, cleanup } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'

afterEach(cleanup)
// Props to send component to be rendered
const props = {
  version: '1.1.0.1',
  classes: {
    footer: 'analitical_request-makeStyles-select-6',
  },
}

test('Report name', () => {
  const div = document.createElement('div')
  ReactDOM.render(<Footer {...props}></Footer>, div)
})

test('Render correctly', () => {
  const { getByTestId } = render(<Footer {...props}></Footer>)
  expect(getByTestId('FooterTestId')).toBeInTheDocument()
})
