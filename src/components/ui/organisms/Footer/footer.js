import React from 'react';
import PropTypes from 'prop-types';
// CORE COMPONENTS AND MOLECULES TO USE
import { Avatar, Chip, Grid } from '@material-ui/core';

//MAIN FUNCTION
/**
 * All componet to display in the footer as a version or other component
 * @param {object} props - component properties
 * @param {reference} ref - reference made by React.forward
 * @param {string} props.version - Number of version to display
 * @param {object} props.classes - Classes to put spesific style with makestyle
 * @property data-testid: Id to use inside footer.test.js file
 */
const FooterOrganism = React.forwardRef((props, ref) => {
  const { children, version, classes, ...rest } = props;

  return (
    <Grid
      container
      ref={ref}
      data-testid={'FooterTestId'}
      justifyContent='flex-end'
      direction='row'
      alignItems='center'
      className={classes.footer}
    >
      <Chip avatar={<Avatar>V</Avatar>} label={version} />
    </Grid>
  );
});
// Type and required properties
FooterOrganism.propTypes = {
  version: PropTypes.string.isRequired,
  classes: PropTypes.object.isRequired,
};
// Default properties
FooterOrganism.defaultProps = {};

export default FooterOrganism;
