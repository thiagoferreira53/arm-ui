import React from 'react'
import ReactDOM from 'react-dom'
// Component to be Test
import AnalysisRequestInfoMessageAtom from './analysisrequestinfomessage'
// Test Library
import { render, cleanup } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'

afterEach(cleanup)

// Props to send component to be rendered
const props = {
  requestid: '1',
  parentclosefunction: jest.fn(),
  datafunction: jest.fn().mockImplementation(() =>
    Promise.resolve({
      status: {
        status: 200,
        message: '',
      },
      data: [],
      metadata: {
        pagination: {
          totalPages: 0,
          totalCount: 0,
        },
      },
    }),
  ),
  staterequest: {},
  profileidfunction: jest.fn().mockImplementation(() =>
    Promise.resolve({
      status: {
        status: 200,
        message: '',
      },
      data: [],
      metadata: {
        pagination: {
          totalPages: 0,
          totalCount: 0,
        },
      },
    }),
  ),
  classes: {},
  requestreview: {},
  baseurlaf: '',
}

test('Report name', () => {
  const div = document.createElement('div')
  ReactDOM.render(<AnalysisRequestInfoMessageAtom {...props}></AnalysisRequestInfoMessageAtom>, div)
})

test('Render correctly', () => {
  const { getByTestId } = render(<AnalysisRequestInfoMessageAtom {...props}></AnalysisRequestInfoMessageAtom>)
  expect(getByTestId('AnalysisRequestInfoMessageAtomTestId')).toBeInTheDocument()
})
