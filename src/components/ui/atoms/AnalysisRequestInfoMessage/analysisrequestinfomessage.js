import React, { Fragment, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
// CORE COMPONENTS
import {
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
  IconButton,
  Typography,
} from '@material-ui/core';
import { dateFormat, DATE_FORMAT_VALUE } from 'utils/helpers/baseHelper';
import BoxColumnAtom from '../BoxColumn';
import InfoMessageAtom from '../InfoMessage';
import GetAppIcon from '@material-ui/icons/GetApp';

function getExperimentGrid(experiments) {
  let grid = [];
  if (experiments !== undefined) {
    for (const experiment of experiments) {
      let firstTime = true;
      if (experiment.occurrences !== undefined) {
        for (const occurrence of experiment.occurrences) {
          const data = {
            experiment: firstTime ? experiment.experimentName : '',
            occurrence: occurrence.occurrenceName,
            location: occurrence.locationName,
          };
          firstTime = false;
          grid.push(data);
        }
      }
    }
  }
  return grid;
}

function getExmperimentLengthGrid(experiments) {
  let length = 0;
  if (experiments !== undefined) {
    for (const experiment of experiments) {
      if (experiment.occurrences !== undefined) {
        length += experiment.occurrences.length;
      }
    }
  }
  return length;
}

function getJobColumns(request) {
  let columns = [
    { field: 'jobId', headerName: 'Job Id' },
    { field: 'traitName', headerName: 'Trait Name' },
  ];
  if (
    request.expLocAnalysisPatternProperty !== undefined &&
    request.expLocAnalysisPatternProperty.propertyCode === 'SESL'
  )
    columns.push({ field: 'locationName', headerName: 'Location Name' }); //,//Only for SESL analysis. For SEML, do not add the column.
  columns.push({ field: 'status', headerName: 'Status' });
  columns.push({ field: 'statusMessage', headerName: 'Message' }); //Shown only when finished.
  return columns;
}
function getJobRows(request) {
  return request.jobs === undefined ? [] : request.jobs;
}

//MAIN FUNCTION
/**
 * @description         - Display a pop-up with information from one request selected by the user.
 * @param {object}      props - component properties.
 * @param {reference}   ref - reference made by React.forward.
 * @param {string}      props.requestview - Object with information to display.
 * @param {function}    props.parantclosefuncion - Callback to parent when the component is closed.
 * @param {function}    props.datafunction - Search olny one request by ID.
 * @param {object}      props.classes - Classes to put spesific style with makestyle.
 * @param {object}      props.staterequest - Information about the color of the bages column.
 * @param {function}    props.profileIdFunction - Function to search one profile user by id.
 * @property {bool}     open - Flag to open the dialog component.
 * @property {array}    request - Save the information from one request.
 * @property {array}    experimentColumns - Save metadata from column to display information about the experiment, occurences and locations.
 * @property {arrya}    displayMessageArray - All value to display in the component grid3parts.
 * @property {function} displayMessageArray[].label - Component to display in first grid.
 * @property {function} displayMessageArray[].value - Component to display in second grid.
 * @property {number}   displayMessageArray[].xsize - Size of the grid.
 * @property {string}   displayMessageArray[].id - ID to create the map datas.
 * @property {string}   [displayMessageArray[].direction = row | column] - Direccion of the parent grid to put the child grid into row o column position.
 * @property {string}    data-testid - ID to use inside reviewmessage.test.js file.
 * @function {void}     handleClose - Function to close the dialog.
 * @function {void}     useEffect - Validate if the request ID is different from 0, the component will be displayed.
 * @returns {component} - Pop-up to display information.
  */
const AnalysisRequestInfoMessageAtom = React.forwardRef((props, ref) => {
  const {
    children,
    requestreview,
    parentclosefunction,
    datafunction,
    staterequest,
    profileidfunction,
    classes,
    baseurlaf,
    ...rest
  } = props;
  const [open, setOpen] = useState(false);
  const [message, setMessage] = useState('');
  const isMessage = message !== '';
  const request = requestreview;
  const emptyRequest = Object.keys(request).length === 0;
  if (!emptyRequest && !open) {
    setOpen(true);
  }
  const experimentColumns = [
    { field: 'experiment', headerName: 'Experiment' },
    { field: 'occurrence', headerName: 'Occurrence' },
    { field: 'location', headerName: 'Location' },
  ];
  const traitColumns = [
    { field: 'abbreviation', headerName: 'Abbreviation' },
    { field: 'name', headerName: 'Name' },
  ];
  const jobColumns = getJobColumns(request);
  const jobRows = getJobRows(request);
  const experimentRows = emptyRequest ? [] : getExperimentGrid(request.experiments);
  const traitRows = emptyRequest
    ? []
    : request.traits.map((trait) => {
        return {
          abbreviation: trait.traitId,
          name: trait.traitName,
        };
      });

  const handleClose = () => {
    parentclosefunction(false);
    setOpen(false);
  };

  const handleCloseMessage = () => {
    setMessage('');
  };

  const handleDownload = () => {
    const url = baseurlaf;
    const urlComponent = request.resultDownloadRelativeUrl.substring(1);
    let urlDownload = `${url}${urlComponent}`.toString();
    let windowObjectReference = window.open(urlDownload, '_blank');
  };

  return (
    <div data-testid={'AnalysisRequestInfoMessageAtomTestId'}>
      <Dialog
        onClose={handleClose}
        aria-labelledby='dialog-review-request'
        open={open}
        ref={ref}
        maxWidth={'xl'}
        fullWidth={true}
        ref={ref}
      >
        <DialogTitle
          id='dialog-title-review-request'
          onClose={handleClose}
        ></DialogTitle>
        <DialogContent>
          {emptyRequest ? (
            <Fragment></Fragment>
          ) : (
            <Grid
              container
              direction='column'
              justifyContent='center'
              alignItems='stretch'
            >
              <Grid item>
                <Grid
                  container
                  direction='row-reverse'
                  justifyContent='space-between'
                  alignItems='flex-start'
                >
                  <Grid item>
                    <IconButton
                      onClick={handleDownload}
                      disabled={
                        request.status === staterequest.complete ? false : true
                      }
                    >
                      <GetAppIcon />
                    </IconButton>
                  </Grid>
                </Grid>
              </Grid>
              <Grid item>
                <Grid
                  container
                  direction='row'
                  justifyContent='flex-start'
                  alignItems='flex-start'
                >
                  <Grid item xs={4}>
                    <Typography variant={'h5'}>{'Analysis Request Info'}</Typography>
                  </Grid>
                  <Grid item xs={4}>
                    <Typography variant={'body1'} className={classes.textBold}>
                      {'Request:'}
                    </Typography>
                    <Typography variant={'body1'}>{request.requestId}</Typography>
                  </Grid>
                </Grid>
              </Grid>
              <Grid item>
                <Grid
                  container
                  direction='row'
                  justifyContent='flex-start'
                  alignItems='flex-start'
                >
                  <Grid item xs={4}>
                    <Typography variant={'body1'} className={classes.textBold}>
                      {'Requestor Name'}
                    </Typography>
                    <Typography variant={'body1'}>
                      {request.requestorName}
                    </Typography>
                  </Grid>
                  <Grid item xs={4}>
                    <Typography variant={'body1'} className={classes.textBold}>
                      {'Submission date'}
                    </Typography>
                    <Typography variant={'body1'}>
                      {dateFormat(request.createdOn, DATE_FORMAT_VALUE.monthENUS)}
                    </Typography>
                  </Grid>
                  <Grid item xs={4}>
                    <Typography variant={'body1'} className={classes.textBold}>
                      {'Status'}
                    </Typography>
                    <Box
                      className={
                        request.status === staterequest.complete
                          ? classes.complete
                          : request.status === staterequest.inprogress
                          ? classes.inproccess
                          : request.status === staterequest.pending
                          ? classes.pending
                          : classes.failure
                      }
                      flexWrap='nowrap'
                      alignItems='center'
                      textAlign='center'
                    >
                      <Typography variant={'body1'}>
                        {`${request.status}`.toString()}
                      </Typography>
                    </Box>
                    <Typography variant={'overline'}>
                      {request.statusMessage}
                    </Typography>
                  </Grid>
                </Grid>
              </Grid>
              <Grid item>
                <Grid
                  container
                  direction='row'
                  justifyContent='flex-start'
                  alignItems='flex-start'
                >
                  <Grid item xs={4}>
                    <Typography variant={'body1'} className={classes.textBold}>
                      {'Institute'}
                    </Typography>
                    <Typography variant={'body1'}>{request.institute}</Typography>
                  </Grid>
                  <Grid item xs={4}>
                    <Typography variant={'body1'} className={classes.textBold}>
                      {'Crop'}
                    </Typography>
                    <Typography variant={'body1'}>{request.crop}</Typography>
                  </Grid>
                  <Grid item xs={4}>
                    <Typography variant={'body1'} className={classes.textBold}>
                      {'N. Occurrences'}
                    </Typography>
                    <Typography variant={'body1'}>
                      {getExmperimentLengthGrid(request.experiments)}
                    </Typography>
                  </Grid>
                </Grid>
              </Grid>
              <Grid item>
                <Grid
                  container
                  direction='row'
                  justifyContent='flex-start'
                  alignItems='flex-start'
                >
                  <Grid item xs={4}>
                    <Typography variant={'body1'} className={classes.textBold}>
                      {'N. of Jobs'}
                    </Typography>
                    <Typography variant={'body1'}>{request.jobs.length}</Typography>
                  </Grid>
                  <Grid item xs={4}>
                    <Typography variant={'body1'} className={classes.textBold}>
                      {'N. of Experiments'}
                    </Typography>

                    <Typography variant={'body1'}>
                      {request.experiments.length}
                    </Typography>
                  </Grid>
                  <Grid item xs={4}>
                    <Typography variant={'body1'} className={classes.textBold}>
                      {'N. Traits'}
                    </Typography>
                    <Typography variant={'body1'}>
                      {request.traits.length}
                    </Typography>
                  </Grid>
                </Grid>
              </Grid>
              <Grid item>
                <Grid
                  container
                  direction='row'
                  justifyContent='flex-start'
                  alignItems='flex-start'
                >
                  <Grid item xs={4}>
                    <Typography variant={'body1'} className={classes.textBold}>
                      {'Analysis type'}
                    </Typography>
                    <Typography variant={'body1'}>{request.analysisType}</Typography>
                    <br />
                    <Typography variant={'body1'} className={classes.textBold}>
                      {'Trait Pattern'}
                    </Typography>
                    <Typography variant={'body1'}>{''}</Typography>
                  </Grid>
                  <Grid item xs={4}>
                    <Typography variant={'body1'} className={classes.textBold}>
                      {'Analysis objective'}
                    </Typography>
                    <Typography variant={'body1'}>
                      {request.analysisObjectiveProperty.propertyName}
                    </Typography>
                    <br />
                    <Typography variant={'body1'} className={classes.textBold}>
                      {'Experiment Location Pattern'}
                    </Typography>
                    <Typography variant={'body1'}>
                      {request.expLocAnalysisPatternProperty.propertyName}
                    </Typography>
                  </Grid>
                  <Grid item xs={4}>
                    <Typography variant={'body1'} className={classes.textBold}>
                      {'Jobs'}
                    </Typography>
                    <BoxColumnAtom
                      rows={jobRows}
                      columns={jobColumns}
                      disableSelectionOnClick
                      maxheight={140}
                    />
                  </Grid>
                </Grid>
              </Grid>
              <Grid item>
                <Grid
                  container
                  direction='row'
                  justifyContent='flex-start'
                  alignItems='flex-start'
                >
                  <Grid item xs={6}>
                    <Typography variant={'body1'} className={classes.textBold}>
                      {'Analysis Configuration'}
                    </Typography>
                    <Typography variant={'body1'}>
                      {request.analysisConfigProperty.label}
                    </Typography>
                  </Grid>
                </Grid>
              </Grid>
              <Grid item>
                <Grid
                  container
                  direction='row'
                  justifyContent='flex-start'
                  alignItems='flex-start'
                >
                  <Grid item xs={6}>
                    <Typography variant={'body1'} className={classes.textBold}>
                      {'Model'}
                    </Typography>
                    <Typography variant={'body1'}>
                      {request.configFormulaProperty.label}
                    </Typography>
                  </Grid>
                </Grid>
              </Grid>
              <Grid item>
                <Grid
                  container
                  direction='row'
                  justifyContent='flex-start'
                  alignItems='flex-start'
                >
                  <Grid item xs={6}>
                    <Typography variant={'body1'} className={classes.textBold}>
                      {'Residual'}
                    </Typography>
                    <Typography variant={'body1'}>
                      {request.configResidualProperty.label}
                    </Typography>
                  </Grid>
                </Grid>
              </Grid>
              <Grid item>
                <Grid
                  container
                  direction='row'
                  justifyContent='flex-start'
                  alignItems='flex-start'
                >
                  <Grid item xs={4}>
                    <Typography variant={'body1'} className={classes.textBold}>
                      {'Data'}
                    </Typography>
                    <BoxColumnAtom
                      rows={experimentRows}
                      columns={experimentColumns}
                      disableSelectionOnClick
                    />
                  </Grid>
                  <Grid item xs={4}></Grid>
                  <Grid item xs={4}>
                    <Typography variant={'body1'} className={classes.textBold}>
                      {'Trait'}
                    </Typography>
                    <BoxColumnAtom
                      rows={traitRows}
                      columns={traitColumns}
                      disableSelectionOnClick
                    />
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          )}
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color='primary'>
            Close
          </Button>
        </DialogActions>
      </Dialog>
      <InfoMessageAtom
        open={isMessage}
        closeparent={handleCloseMessage}
        message={message}
      />
    </div>
  );
});
// Type and required properties
AnalysisRequestInfoMessageAtom.propTypes = {
  requestreview: PropTypes.object.isRequired,
  parentclosefunction: PropTypes.func.isRequired,
  datafunction: PropTypes.func.isRequired,
  staterequest: PropTypes.object.isRequired,
  profileidfunction: PropTypes.func.isRequired,
  classes: PropTypes.object.isRequired,
  baseurlaf: PropTypes.string.isRequired,
};
// Default properties
AnalysisRequestInfoMessageAtom.defaultProps = {};

export default AnalysisRequestInfoMessageAtom;
