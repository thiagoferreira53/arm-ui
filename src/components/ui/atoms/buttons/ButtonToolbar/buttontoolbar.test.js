import React from 'react'
import ReactDOM from 'react-dom'
// Component to be Test
import ButtonToolbar from './buttontoolbar'
// Test Library
import { render, cleanup } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'

afterEach(cleanup)

// Props to send component to be rendered
const props = {
  helptext: '',
  classbutton: 'style',
  functiononclick: jest.fn(),
  title: '',
}

test('Report name', () => {
  const div = document.createElement('div')
  ReactDOM.render(<ButtonToolbar {...props}></ButtonToolbar>, div)
})

test('Render correctly', () => {
  const { getByTestId } = render(<ButtonToolbar {...props}></ButtonToolbar>)
  const button = getByTestId('ButtonToolbarTestId')
  expect(button).toBeInTheDocument()
})
