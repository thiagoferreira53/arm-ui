import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
// CORE COMPONENTS
import { IconButton, Tooltip, Typography } from '@material-ui/core';

//MAIN FUNCTION
/**
 * @description         - Create a button and icon with a text when mouse is over it
 * @param {object}      props - Children properties
 * @param {string}      props.helptext - Text to display when the mouse is over it
 * @param {func}        props.functiononclick - Parent function starts when the user clicks the component
 * @param {string}      props.color - Color to put in the component
 * @param {string}      props.classbutton - Object with spesific style with makestyle
 * @param {ref}         ref - Reference made by React.forward
 * @property {string}   data-testid - Id to use inside bottonicontoolbar.test.js file.
 * @returns {component} - Button to display into grid
 */
const ButtonIconToolbarAtom = React.forwardRef((props, ref) => {
  const { children, helptext, classbutton, functiononclick, color, ...rest } = props;

  return (
    <Tooltip
      title={
        <Fragment>
          <Typography variant={'body1'}>{helptext}</Typography>
        </Fragment>
      }
      data-testid={'ButtonIconToolbarTestId'}
      ref={ref}
    >
      <IconButton
        className={classbutton}
        onClick={functiononclick}
        color={color}
        size='medium'
        component='span'
      >
        {children}
      </IconButton>
    </Tooltip>
  );
});
// Type and required properties
ButtonIconToolbarAtom.propTypes = {
  helptext: PropTypes.string.isRequired,
  functiononclick: PropTypes.func.isRequired,
  color: PropTypes.string.isRequired,
  classbutton: PropTypes.string.isRequired,
};
// Default properties
ButtonIconToolbarAtom.defaultProps = {};

export default ButtonIconToolbarAtom;
