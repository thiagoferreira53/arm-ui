import React from 'react'
import ReactDOM from 'react-dom'
// Component to be Test
import ButtonIconToolbar from './buttonicontoolbar'
// Test Library
import { render, cleanup } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
import DeleteIcon from '@material-ui/icons/Delete'

afterEach(cleanup)
const props = {
  helptext: '',
  color: 'primary',
  functiononclick: jest.fn(),
}

test('Report name', () => {
  const div = document.createElement('div')
  ReactDOM.render(
    <ButtonIconToolbar {...props}>
      <DeleteIcon />
    </ButtonIconToolbar>,
    div,
  )
})

test('Render correctly', () => {
  const { getByTestId } = render(
    <ButtonIconToolbar {...props}>
      <DeleteIcon />
    </ButtonIconToolbar>,
  )
  expect(getByTestId('ButtonIconToolbarTestId')).toBeInTheDocument()
})
