import React, { Fragment, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
// CORE COMPONENTS
import {
  Badge,
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  IconButton,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Tooltip,
  Typography,
} from '@material-ui/core';
import MailIcon from '@material-ui/icons/Mail';

//MAIN FUNCTION
/**
 * @description         - Button to display on the toolbar in the main grid with functionality to inject information about the request and status into the storage location
 * @param {object}      props - Children properties
 * @param {ref}         ref - Rreference made by React.forward
 * @param {string}      props.helptext Text to display into the button
 * @param {string}      props.color - Color to aplicate to the button
 * @param {function}    props.functiondata - Function to search the request that was created
 * @param {string}      props.requestorid - Requestor Id to using to search information
 * @param {object}      props.classes - Classes to put spesific style with makestyle
 * @param {object}      props.staterequest - Information about the color of the bages column
 * @param {number}      props.seconds - Time to launch the service to get the new request
 * @param {string}      props.localstorageid - ID to save the varaible into the local storage
 * @property {number}   notification - Flag to display the number of notification
 * @property {array}    notificationData - Notificacion to display
 * @property {bool}     openDialog - Flag to open the list of history
 * @property {number}   time - Time to launche the interval to search changes into request
 * @prop {string}       data-testid - Id to use inside noticebutton.test.js file.
 * @function            handleNotifice - Function to get the notification of new request
 * @function            handleDialogClose - Dialogol componet is closed and NotificationData is saved into local storage
 * @function            handleNotificationClear - NotificationData is cleaned  and saved into local storage
 * @function            clearNotification - NotificationData is saved into local storage
 * @function            useEffect - Mount componet to callback the data every 2 minute
 * @returns {component} - Button to display notification when request change
 */
const NoticeButtonAtom = React.forwardRef((props, ref) => {
  const {
    children,
    helptext,
    color,
    functiondata,
    requestorid,
    classes,
    staterequest,
    seconds,
    localstorageid,
    ...rest
  } = props;
  const [notification, setNotification] = useState(0);
  const [notificationData, setNotificationData] = useState([]);
  const [openDialog, setOpenDialog] = useState(false);
  const time = 1000 * seconds;

  const handleNotifice = () => {
    setOpenDialog(true);
  };
  const handleDialogClose = () => {
    setOpenDialog(false);
    clearNotification();
  };
  const handleNotificationClear = () => {
    setOpenDialog(false);
    clearNotification();
  };
  const clearNotification = () => {
    if (notificationData.length > 0) {
      const localData = JSON.parse(window.localStorage.getItem(localstorageid));
      const newLocalData = [...new Set([...localData, ...notificationData])];
      window.localStorage.setItem(localstorageid, JSON.stringify(newLocalData));
      setNotificationData([]);
      setNotification(0);
    }
  };

  useEffect(() => {
    const interval = setInterval(() => {
      functiondata({
        requestorId: requestorid,
        crop: '',
        organization: '',
        status: '',
        pageSize: 10,
        page: 0,
      }).then((res) => {
        if (res.status.status === 200) {
          const serverData = res.data.map((data) =>
            `${data.requestId}|${data.status}`.toString(),
          );
          const localData = JSON.parse(window.localStorage.getItem(localstorageid));
          if (localData !== undefined && localData !== null) {
            const dataIsNew = serverData.filter((data) => {
              return localData.indexOf(data) == -1;
            });
            if (
              dataIsNew !== null &&
              dataIsNew !== undefined &&
              dataIsNew.length > 0
            ) {
              setNotificationData(dataIsNew);
              setNotification(dataIsNew.length);
            }
          } else {
            window.localStorage.setItem(localstorageid, JSON.stringify(serverData));
          }
        }
      });
    }, time);
    return () => {
      clearInterval(interval);
    };
  }, [
    setNotification,
    notification,
    time,
    requestorid,
    functiondata,
    setNotificationData,
    localstorageid,
  ]);

  return (
    <Fragment>
      <Tooltip
        title={
          <Fragment>
            <Typography variant={'body1'}>{helptext}</Typography>
          </Fragment>
        }
        data-testid={'NoticeButtonTestId'}
        ref={ref}
      >
        <IconButton
          onClick={handleNotifice}
          color={color}
          size='medium'
          component='span'
          className={classes.button}
        >
          <Badge badgeContent={notification} color='primary'>
            <MailIcon />
          </Badge>
        </IconButton>
      </Tooltip>
      <Dialog
        maxWidth='xl'
        onClose={handleDialogClose}
        aria-labelledby='notification-dialog'
        open={openDialog}
      >
        <DialogContent dividers>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell width={100}>Status</TableCell>
                <TableCell width={150}>Request ID</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {notificationData.map((data, index) => {
                if (data !== undefined && data !== null && data !== '') {
                  const datas = data.split('|');
                  const key = `${index}-notification`;
                  let color = classes.failure;
                  if (datas[1] === staterequest.complete) color = classes.complete;
                  if (datas[1] === staterequest.inprogress)
                    color = classes.inproccess;
                  if (datas[1] === staterequest.pending) color = classes.pending;
                  return (
                    <TableRow hover role='button' key={key}>
                      <TableCell>
                        <Box
                          className={color}
                          alignItems='center'
                          textAlign='center'
                        >
                          {`${datas[1]}`.toString()}
                        </Box>
                      </TableCell>
                      <TableCell>{datas[0]}</TableCell>
                    </TableRow>
                  );
                }
              })}
            </TableBody>
          </Table>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleNotificationClear}>Clear</Button>
        </DialogActions>
      </Dialog>
    </Fragment>
  );
});
// Type and required properties
NoticeButtonAtom.propTypes = {
  helptext: PropTypes.string.isRequired,
  color: PropTypes.string.isRequired,
  functiondata: PropTypes.func.isRequired,
  requestorid: PropTypes.string.isRequired,
  classes: PropTypes.object.isRequired,
  staterequest: PropTypes.object.isRequired,
  seconds: PropTypes.number.isRequired,
  localstorageid: PropTypes.string.isRequired,
};
// Default properties
NoticeButtonAtom.defaultProps = {};

export default NoticeButtonAtom;
