import React from 'react'
import ReactDOM from 'react-dom'
// Component to be Test
import ItemCheck from './itemcheck'
// Test Library
import { render, cleanup } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'

afterEach(cleanup)
// Props to send component to be rendered
const props = {
  label: 'test',
  ischeck: true,
  handlechangecolumns: () => {},
  field: 'testfield',
  classes: {
    select: 'analitical_request-makeStyles-select-6',
  },
}

test('Report name', () => {
  const div = document.createElement('div')
  ReactDOM.render(<ItemCheck {...props}></ItemCheck>, div)
})

test('Render correctly', () => {
  const { getByTestId } = render(<ItemCheck {...props}></ItemCheck>)
  expect(getByTestId('ItemCheckTestId')).toBeInTheDocument()
})
