import React from 'react'
import ReactDOM from 'react-dom'
// Component to be Test
import MessageRow from './messagerow'
// Test Library
import { render, cleanup } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'

afterEach(cleanup)
// Props to send component to be rendered
const props = {
  message: '',
  numberVisibleColumn: 1,
}

test('Report name', () => {
  const div = document.createElement('div')
  ReactDOM.render(<MessageRow {...props}></MessageRow>, div)
})

test('Render correctly', () => {
  const { getByTestId } = render(<MessageRow {...props}></MessageRow>)
  expect(getByTestId('MessageRowTestId')).toBeInTheDocument()
})
