import React from 'react'
import PropTypes from 'prop-types'
// CORE COMPONENTS
import { FormControl, MenuItem, Select, Typography } from '@material-ui/core'

//MAIN FUNCTION
/**
 *
 * @param props: component properties
 * @param ref: reference made by React.forward
 */
const SimpleSelectAtom = React.forwardRef((props, ref) => {
  const {
    children,
    idselect,
    valuedisplay,
    handlechange,
    messageerror,
    valuedisplays,
    title,
    valueid,
    valuelabel,
    classes,
    ...rest
  } = props
  const value = valuedisplays.length === 0 || valuedisplay === 0 ? '' : valuedisplay
  const keyForm = `form_create_new_request_${idselect}`.toString()

  return (
    /**
     * @prop data-testid: Id to use inside simpleselect.test.js file.
     */
    <div className={classes.formSelect} data-testid={'SimpleSelectTestId'} ref={ref}>
      <FormControl variant='outlined' key={keyForm} size='small'>
        <Typography variant='body1'>{title}</Typography>
        <Select
          id={idselect}
          value={value}
          fullWidth
          onChange={handlechange}
          helpertext={messageerror}
          className={classes.select}
        >
          {valuedisplays.map((value, index) => {
            const key = `${value[valueid]}_${idselect}_${index}`.toString()
            return (
              <MenuItem value={value[valueid]} key={key}>
                {value[valuelabel]}
              </MenuItem>
            )
          })}
        </Select>
      </FormControl>
    </div>
  )
})
// Type and required properties
SimpleSelectAtom.propTypes = {
  /**ID to identify the component*/
  idselect: PropTypes.string.isRequired,
  /**The function was created by the parent to save the new value by react-redux */
  handlechange: PropTypes.func.isRequired,
  /**Message error to display in case the error*/
  messageerror: PropTypes.string.isRequired,
  /**Array of elements to select one element*/
  valuedisplays: PropTypes.array.isRequired,
  /**Attribute in the array that will be a value display*/
  valueid: PropTypes.string.isRequired,
  /**Attribute in the array that will be a label display*/
  valuelabel: PropTypes.string.isRequired,
  /**Classes to put spesific style with makestyle */
  classes: PropTypes.object.isRequired,
}
// Default properties
SimpleSelectAtom.defaultProps = {}

export default SimpleSelectAtom
