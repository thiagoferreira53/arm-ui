import React from 'react'
import ReactDOM from 'react-dom'
// Component to be Test
import SimpleSelect from './simpleselect'
// Test Library
import { render, cleanup } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'

afterEach(cleanup)
// Props to send component to be rendered
const props = {
  idselect: 'idTest',
  valuedisplay: '1',
  handlechange: () => {},
  messageerror: '',
  valuedisplays: [{ propertyId: '1', propertyName: 'name' }],
  valueid: 'propertyId',
  valuelabel: 'propertyName',
  classes: {
    select: 'analitical_request-makeStyles-select-6',
  },
}

test('Report name', () => {
  const div = document.createElement('div')
  ReactDOM.render(<SimpleSelect {...props}></SimpleSelect>, div)
})

test('Render correctly', () => {
  const { getByTestId } = render(<SimpleSelect {...props}></SimpleSelect>)
  expect(getByTestId('SimpleSelectTestId')).toBeInTheDocument()
})
