import React from 'react'
import ReactDOM from 'react-dom'
// Component to be Test
import ObjectSelect from './objectselect'
// Test Library
import { render, cleanup } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'

afterEach(cleanup)
// Props to send component to be rendered
const props = {
  idselect: 'idTest',
  valuedisplay: '',
  handlechange: () => {},
  messageerror: '',
  valuedisplays: [{ propertyId: '1', propertyName: 'name' }],
  valueid: 'propertyId',
  valuelabel: 'propertyName',
  classes: {
    select: 'analitical_request-makeStyles-select-6',
  },
}

test('Report name', () => {
  const div = document.createElement('div')
  ReactDOM.render(<ObjectSelect {...props}></ObjectSelect>, div)
})

test('Render correctly', () => {
  props.valuedisplay = '{"id":"1","label":"name"}'
  const { getByTestId } = render(<ObjectSelect {...props}></ObjectSelect>)
  expect(getByTestId('ObjectSelectTestId')).toBeInTheDocument()
})
