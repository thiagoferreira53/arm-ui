import React from 'react'
import PropTypes from 'prop-types'
// CORE COMPONENTS
import { FormControl, MenuItem, Select, Typography } from '@material-ui/core'

//MAIN FUNCTION
/**
 * Create a simple select (Combobox) to put the value into a function injected by the parent
 * @param props: component properties
 * @param ref: reference made by React.forward
 */
const ObjectSelectAtom = React.forwardRef((props, ref) => {
  const {
    children,
    idselect,
    valuedisplay,
    handlechange,
    messageerror,
    valuedisplays,
    title,
    valueid,
    valuelabel,
    classes,
    ...rest
  } = props

  /**Key to the form*/
  const keyForm = `form_create_new_request_obj_${idselect}`.toString()
  return (
    /* 
     @prop data-testid: Id to use inside objectselect.test.js file.
     */
    <div data-testid={'ObjectSelectTestId'} ref={ref} className={classes.formSelect}>
      <FormControl variant='outlined' key={keyForm} size='small'>
        <Typography variant='body1'>{title}</Typography>
        <Select
          id={idselect}
          value={valuedisplay === null ? '' : valuedisplay}
          fullWidth
          onChange={handlechange}
          helpertext={messageerror}
          className={classes.select}
        >
          {valuedisplays.map((value, index) => {
            const key = `${value[valueid]}_${idselect}_${index}`.toString()
            const valueToMenu = JSON.stringify({
              id: value[valueid],
              label: value[valuelabel],
            })
            return (
              <MenuItem value={valueToMenu} key={key}>
                {value[valuelabel]}
              </MenuItem>
            )
          })}
        </Select>
      </FormControl>
    </div>
  )
})
// Type and required properties
ObjectSelectAtom.propTypes = {
  /**ID to identify the component*/
  idselect: PropTypes.string.isRequired,
  /**Value to display in the select component is a JSON string*/
  valuedisplay: PropTypes.string.isRequired,
  /**The function was created by the parent to save the new value by react-redux */
  handlechange: PropTypes.func.isRequired,
  /**Message error to display in case the error*/
  messageerror: PropTypes.string.isRequired,
  /**Array of elements to select one element*/
  valuedisplays: PropTypes.array.isRequired,
  /**Attribute in the array that will be a value display*/
  valueid: PropTypes.string.isRequired,
  /**Attribute in the array that will be a label display*/
  valuelabel: PropTypes.string.isRequired,
  /**Classes to put spesific style with makestyle */
  classes: PropTypes.object.isRequired,
}
// Default properties
ObjectSelectAtom.defaultProps = {}

export default ObjectSelectAtom
