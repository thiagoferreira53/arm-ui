import React, { Component } from 'react';
import PropTypes from 'prop-types';
// CORE COMPONENTS AND MOLECULES TO USE
import { Grid, Paper } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import { lightBlue } from '@material-ui/core/colors';
import MainTableOrganism from 'components/ui/organisms/MainTable';
import MainNavBarOrganism from 'components/ui/organisms/MainNavBar';
import FooterOrganism from 'components/ui/organisms/Footer';

const useStyles = makeStyles((theme) => ({
  complete: {
    background: '#2e7d32',
    borderRadius: 3,
    border: 0,
    color: 'white',
    alignContent: 'center',
    width: 70,
    height: 18,
  },
  inproccess: {
    background: lightBlue[800],
    borderRadius: 3,
    border: 0,
    color: 'white',
    width: 70,
    height: 18,
  },
  pending: {
    background: '#283593',
    borderRadius: 3,
    border: 0,
    color: 'white',
    width: 70,
    height: 18,
  },
  failure: {
    background: '#c62828',
    borderRadius: 3,
    border: 0,
    color: 'white',
    width: 70,
    height: 18,
  },
  grid: {
    margin: '0 -15px',
    width: 'calc(100% + 30px)',
    padding: '0 15px !important',
  },
  textBold: {
    fontWeight: 'bold',
  },
  button: {
    '&:hover': {
      backgroundColor: theme.palette.secondary.main,
    },
    color: theme.palette.common.white,
    backgroundColor: theme.palette.primary.main,
    marginLeft: theme.spacing(1),
    borderRadius: 4,
  },
}));
//MAIN FUNCTION
/**
 * @description         - Integrate all organisms components and all spesific style to it
 * @param {object}      props - Children properties
 * @param {object}      props.requestList - Object to configurate elemente in request
 * @param {function}    props.requestList.functionData - Get all the information for request created by the user
 * @param {string}      props.requestList.title - Title of the page
 * @param {array}       props.requestList.columnsData - Array of the metadata of columns
 * @param {string}      props.requestList.columnsData[].Header - Title of the column
 * @param {string}      props.requestList.columnsData[].accessor - ID to look up the value in the row array
 * @param {boolean}     props.requestList.columnsData[].filter - Flag to enable or disable the filter option
 * @param {boolean}     props.requestList.columnsData[].hidden - Flag to hiden or show the columns
 * @param {array}       props.requestList.buttons - Array with information for all buttons
 * @param {string}      props.requestList.buttons[].id - ID to identify the button component
 * @param {string}      props.requestList.buttons[].title - Title to display into the button component
 * @param {string}      props.requestList.buttons[].help - Text to display tool tip into the button component
 * @param {string}      props.requestList.buttons[].color - Color to put into the button component
 * @param {object}      props.requestList.buttons[].parameters - Parameter for the button component to work with it
 * @param {object}      props.requestList.stateRequest - Case to use to convert the status of an application to a badge
 * @param {string}      props.requestList.stateRequest.complete - Complete status
 * @param {string}      props.requestList.stateRequest.inprogress - In progress status
 * @param {string}      props.requestList.stateRequest.pending - Pending status
 * @param {string}      props.requestList.stateRequest.failure - Failure status
 * @param {object}      props.requestList.requestorProfile - Object to save only the neccesary data of the user profile
 * @param {function}    props.requestList.requetsIDFunction - Function to search one request by id
 * @param {function}    props.requestList.profileIdFunction - Function to search one profile user by id
 * @param {object}      props.navBar - Object to configurate a navigation bar
 * @param {array}       props.navBar.buttons - Metainformation to build a button
 * @param {string}      props.navBar.buttons[].id - ID to identify the button component
 * @param {string}      props.navBar.buttons[].label - Labe to put into the button component
 * @param {bool}        props.navBar.buttons[].active - Enable or disable the button component
 * @param {object}      props.navBar.buttons[].parameter - Object with parameter to work the button component
 * @param {string}      props.navBar.buttons[].parameter.idStorage - ID to search the access token into localstorage
 * @param {string}      props.navBar.buttons[].parameter.uriParent - URL to get the access token
 * @param {string}      props.version - Version of the system
 * @param {reference}   ref - reference made by React.forward
 * @property {string}   data-testid: Id to use inside mainrequest.test.js file.
 * @property {object}   classes - Crea a classes by the style
 * @returns {Component} - Return 3 organisms Main Navigation Bar, Main Table and Footer
 */
const MainRequestTemplate = React.forwardRef((props, ref) => {
  const { children, requestlist, navbar, version, ...rest } = props;
  const classes = useStyles();
  return (
    <Grid
      container
      ref={ref}
      data-testid={'MainRequestTestId'}
      className={classes.grid}
    >
      <Grid item xs={12} className={classes.grid}>
        <Paper>
          <MainNavBarOrganism buttons={navbar.buttons} classes={classes} />
          <MainTableOrganism
            functiondata={requestlist.functionData}
            title={requestlist.title}
            columnsdata={requestlist.columnsData}
            buttons={requestlist.buttons}
            staterequest={requestlist.stateRequest}
            requestorprofile={requestlist.requestorProfile}
            classes={classes}
            requestidfunction={requestlist.requetsIDFunction}
            profileidfunction={requestlist.profileIdFunction}
          />
          <FooterOrganism version={version} classes={classes}></FooterOrganism>
        </Paper>
      </Grid>
    </Grid>
  );
});
// Type and required properties
MainRequestTemplate.propTypes = {
  navbar: PropTypes.object.isRequired,
  requestlist: PropTypes.object.isRequired,
  version: PropTypes.string.isRequired,
};
// Default properties
MainRequestTemplate.defaultProps = {};

export default MainRequestTemplate;
