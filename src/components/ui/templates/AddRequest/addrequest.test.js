import React from 'react'
import ReactDOM from 'react-dom'
// Component to be Test
import AddRequest from './addrequest'
// Test Library
import { render, cleanup } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'

afterEach(cleanup)
// Props to send component to be rendered
const props = {
  setting: {
    tokenHelpPopUp: false,
    action: {
      NEW_REQUEST_ACTION: '',
    },
    routes: {
      rotuelist: '',
    },
  },
  selects: {
    responseVariableTrait: {},
    analysisObjective: {},
    traitAnalysisPattern: {},
    experimentLocationAnalysisPattern: {},
    analysisConfiguration: {},
    mainModel: {},
    prediction: {},
    spatialAdjusting: {},
  },
  experiment: {},
  titleData: {
    title: '',
    subtitle: '',
    note: '',
  },
  request: {},
  version: '',
}
jest.mock('@material-ui/styles', () => ({
  ...jest.requireActual('@material-ui/styles'),
  makeStyles: jest.fn().mockReturnValue(
    jest.fn().mockImplementation(() =>
      Promise.resolve({
        grid: {},
      }),
    ),
  ),
}))
jest.mock('../../organisms/AddRequestTab', () => () => {
  return <div></div>
})
test('Report name', () => {
  const div = document.createElement('div')
  ReactDOM.render(<AddRequest {...props}></AddRequest>, div)
})

test('Render correctly', () => {
  const { getByTestId } = render(<AddRequest {...props}></AddRequest>)
  expect(getByTestId('AddRequestTestId')).toBeInTheDocument()
})
