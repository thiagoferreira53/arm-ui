import React from 'react'
import PropTypes from 'prop-types'
// CORE COMPONENTS AND MOLECULES TO USE
import { Grid, Paper } from '@material-ui/core'
import AddRequestTabOrganism from 'components/ui/organisms/AddRequestTab'
import { makeStyles } from '@material-ui/styles'
import FooterOrganism from 'components/ui/organisms/Footer'
import { grey } from '@material-ui/core/colors'

const useStyles = makeStyles((theme) => ({
  grid: {
    margin: '0 -15px',
    width: 'calc(100% + 30px)',
    padding: '0 15px !important',
  },
  gridSubtitle: {
    margin: '0 -15px',
    width: 'calc(100% + 30px)',
    padding: '0 15px !important',
    marginTop: 8,
    marginLeft: 3,
  },
  gridButton: {
    margin: '0 -15px',
    width: 'calc(100% + 30px)',
    padding: '0 15px !important',
    marginTop: 8,
    marginLeft: 3,
  },
  gridTable: {
    margin: '0 -15px',
    width: 'calc(100% + 30px)',
    padding: '0 15px !important',
    marginLeft: 3,
  },
  tableExperiment: {
    width: '100%',
  },
  tablecontainerExperiment: {
    width: '100%',
    maxHeight: 660,
  },
  cellExpriment: {
    width: '100%',
  },
  rowOccurrence: {
    backgroundColor: grey[300],
  },
  cellRowWithOccurrence: {
    paddingBottom: 0,
    paddingTop: 0,
    paddingLeft: 0,
    paddingRight: 0,
  },
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1,
  },
  formSelect: {
    width: 500,
    '& > * + *': {
      marginTop: theme.spacing(0),
    },
    marginBottom: 20,
  },
  select: {
    minWidth: 380,
  },
  listItem: {
    display: 'flex',
    alignItems: 'center',
    flexWrap: 'wrap',
  },
  footer: {
    margin: '0 -15px',
    width: 'calc(100% + 30px)',
    padding: '0 15px !important',
    marginBottom: 4,
    marginTop: 4,
  },
  tab: {
    marginTop: 15,
  },
  selectGrid: {
    marginBottom: 20,
  },
  button: {
    '&:hover': {
      backgroundColor: theme.palette.secondary.main,
    },
    color: theme.palette.common.white,
    backgroundColor: theme.palette.primary.main,
    marginLeft: theme.spacing(1),
    borderRadius: 4,
  },
}))
//MAIN FUNCTION
/**
 * @description           To page Add Request integrate all organisms and all spesific style to it
 * @param {object}        props - Component properties
 * @param {object}        setting - Object with all paramtert and configuration to app
 * @param {object}        selects - Object with all parameter and configuration to combobox
 * @param {object}        experiment - Object with all parameter and configuration to experiment and occurence
 * @param {object}        request - Object with all parameter and configuration to request
 * @param {string}        version - Number of the version
 * @param {string}        ref - reference made by React.forward
 * @property {object}     classes - Crea a classes by the style
 * @property {string}     data-testid - Id to use inside addrequest.test.js file.
 * @returns {component}   Page with information give by the page the page can create new request
 */
const AddRequestTemplate = React.forwardRef((props, ref) => {
  const {
    children,
    setting,
    selects,
    experiment,
    titleData,
    request,
    version,
    ...rest
  } = props
  const classes = useStyles()

  return (
    <Grid
      container
      ref={ref}
      data-testid={'AddRequestTestId'}
      className={classes.grid}
    >
      <Grid item xs={12} className={classes.grid}>
        <Paper>
          <AddRequestTabOrganism
            setting={setting}
            selects={selects}
            experiment={experiment}
            subtitle={titleData.subtitle}
            classes={classes}
            request={request}
          ></AddRequestTabOrganism>
          <FooterOrganism version={version} classes={classes}></FooterOrganism>
        </Paper>
      </Grid>
    </Grid>
  )
})
// Type and required properties
AddRequestTemplate.propTypes = {
  setting: PropTypes.object.isRequired,
  selects: PropTypes.object.isRequired,
  experiment: PropTypes.object.isRequired,
  request: PropTypes.object.isRequired,
  version: PropTypes.string.isRequired,
}
// Default properties
AddRequestTemplate.defaultProps = {}

export default AddRequestTemplate
