import React, { Fragment, useState } from 'react';
import PropTypes from 'prop-types';
// CORE COMPONENTS AND ATOMS TO USE
import { Grid, IconButton, Tooltip, Typography } from '@material-ui/core';
import { EbsGrid } from '@ebs/components';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import VisibilityIcon from '@material-ui/icons/Visibility';
import GetAppIcon from '@material-ui/icons/GetApp';
import RefreshIcon from '@material-ui/icons/Refresh';
import InfoMessageAtom from 'components/ui/atoms/InfoMessage/infomessage';
import ButtonToolbarAtom from 'components/ui/atoms/buttons/ButtonToolbar';
import ButtonIconToolbarAtom from 'components/ui/atoms/buttons/ButtonIconToolbar';
import NoticeButtonAtom from 'components/ui/atoms/buttons/NoticeButton';
import AnalysisRequestInfoMessageAtom from 'components/ui/atoms/AnalysisRequestInfoMessage';

//MAIN FUNCTION
/**
 * @description         - Grid to display the request was created
 * @param {object}      props - Children properties
 * @param {function}    props.functiondata - Function to get all request created by the user
 * @param {string}      props.title - Text to put into the title
 * @param {array}       props.columnsdata - Array of column into the table
 * @param {array}       props.buttons - Array of object to create a buttons
 * @param {object}      props.staterequest - Object with information about the status of each request [status]
 * @param {object}      props.requestorprofile - Information of the user
 * @param {function}    props.requestidfunction - Search olny one request by ID
 * @param {object}      props.classes - Classes to put spesific style with makestyle
 * @param {function}    props.profileIdFunction - Function to search one profile user by id
 * @param {ref}         ref - Reference made by React.forward
 * @property {object}   newRequestButton - Search the button to create a new request
 * @property {object}   removeRequestButton - Search the button to remove the request
 * @property {object}   refreshButton - Search the property and configuration to refresh button
 * @property {object}   downloadButton - Search the property and configuration to download button
 * @property {object}   noticeButton - Search the property and configuration to notice button
 * @property {boolean}  info - Display info in snackbar
 * @property {string}   requestIdReview - Variable to put the requestor ID to send the review message component by default 0 to hiden the component
 * @function {void}     fetch - Function to get the data and create statusButton, and  createdOn
 * @function {void}     handleNewRequest - Redux to start new request
 * @function {void}     handleRemoveRequest - Function to remove one request
 * @function {void}     handleRefresh - Function to refresh the grid
 * @function {void}     handleInfoClose - Function to close snackbar
 * @function {void}     handelCloseReview - Function to close a review message atom component if get a false value
 * @function {void}     toolbarActions - Function in react to put action icon per table
 * @function {void}     rowActions - Function in react to put action icon per row [Open request, and download request]
 * @property {string}   data-testid: Id to use inside maindatagrid.test.js file.
 */
const MainDataGridMolecule = React.forwardRef((props, ref) => {
  const {
    children,
    functiondata,
    title,
    columns,
    classes,
    staterequest,
    requestorprofile,
    buttons,
    requestidfunction,
    profileidfunction,
    ...rest
  } = props;
  const history = useHistory();
  const dispatch = useDispatch();
  const newRequestButton = buttons.find((button) => button.id === 'newRequest');
  const removeRequestButton = buttons.find(
    (button) => button.id === 'removeRequest',
  );
  const refreshButton = buttons.find((button) => button.id === 'refresh');
  const downloadButton = buttons.find((button) => button.id === 'download');
  const noticeButton = buttons.find((button) => button.id === 'notice');
  const [info, setInfo] = useState(false);
  const [requestReview, setRequestReview] = useState({});

  const fetch = async ({ page, sort, filters }) => {
    const status = filters.find((element) => element.id === 'status');
    return new Promise(async (resolve, reject) => {
      let result = await functiondata({
        requestorId: requestorprofile.requestorId,
        crop: '',
        organization: '',
        status: status === undefined ? '' : status.value,
        pageSize: page.size,
        page: page.number,
      });
      //Filter local for other columns
      let data = result.data;
      filters.map((filter) => {
        data = data.filter(
          (element) =>
            element[filter.id].toLowerCase().indexOf(filter.value.toLowerCase()) >
            -1,
        );
      });
      //Sort data
      sort.map((sortE) => {
        data = data.sort((a, b) => {
          if (a[sortE.col] !== undefined) {
            if (a[sortE.col].toLowerCase() > b[sortE.col].toLowerCase()) {
              return sortE.mod === 'ASC' ? 1 : -1;
            }
            if (a[sortE.col].toLowerCase() < b[sortE.col].toLowerCase()) {
              return sortE.mod === 'ASC' ? -1 : 1;
            }
          }
          return 0;
        });
      });
      resolve({
        pages: result.metadata.pagination.totalPages,
        data: data,
        elements: result.metadata.pagination.totalElements,
      });
    });
  };
  const handleNewRequest = () => {
    dispatch({
      type: newRequestButton.parameters.actionSave,
      payload: '',
    });
    history.push(newRequestButton.parameters.ruteToPage);
  };
  const handleInfoClose = () => {
    setInfo(false);
  };
  const handelCloseReview = (action) => {
    if (!action) {
      setRequestReview({});
    }
  };
  const toolbarActions = (selectedRows, refresh) => {
    const handleRefresh = () => {
      refresh();
      setInfo(true);
    };

    return (
      <Fragment>
        <ButtonToolbarAtom
          helptext={newRequestButton.help}
          classbutton={classes.button}
          functiononclick={handleNewRequest}
          title={newRequestButton.title}
        ></ButtonToolbarAtom>
        <ButtonIconToolbarAtom
          helptext={refreshButton.help}
          functiononclick={handleRefresh}
          color={refreshButton.color}
          classbutton={classes.button}
        >
          <RefreshIcon />
        </ButtonIconToolbarAtom>
        <NoticeButtonAtom
          helptext={noticeButton.help}
          color={noticeButton.color}
          functiondata={functiondata}
          requestorid={requestorprofile.requestorId}
          classes={classes}
          staterequest={staterequest}
          seconds={noticeButton.parameters.seconds}
          localstorageid={noticeButton.parameters.localStorageId}
        ></NoticeButtonAtom>
      </Fragment>
    );
  };
  const rowActions = (rowData, refresh) => {
    const handleViewClick = () => {
      setRequestReview(rowData);
    };
    const handleDownloadClick = () => {
      const url = downloadButton.parameters.baseUrlAF;
      const urlComponent = rowData.resultDownloadRelativeUrl.substring(1);
      let urlDownload = `${url}${urlComponent}`.toString();
      let windowObjectReference = window.open(urlDownload, '_blank');
    };
    return (
      <Grid container>
        <Grid item>
          <Tooltip
            title={
              <Fragment>
                <Typography variant={'body1'}>
                  {'View Analysis Request Info'}
                </Typography>
              </Fragment>
            }
          >
            <IconButton size='small' onClick={handleViewClick} color='primary'>
              <VisibilityIcon />
            </IconButton>
          </Tooltip>
        </Grid>
        <Grid item>
          <Tooltip
            title={
              <Fragment>
                <Typography variant={'body1'}>
                  {'Download Request files and Results Report'}
                </Typography>
              </Fragment>
            }
          >
            <IconButton
              size='small'
              onClick={handleDownloadClick}
              color='primary'
              disabled={
                rowData.resultDownloadRelativeUrl === undefined ? true : false
              }
            >
              <GetAppIcon />
            </IconButton>
          </Tooltip>
        </Grid>
      </Grid>
    );
  };

  return (
    <Grid container ref={ref} data-testid={'MainDataGridTestId'}>
      <Grid item xs={12}>
        <EbsGrid
          title={
            <Typography variant={'h4'} component={'h4'}>
              {title}
            </Typography>
          }
          select='multi'
          height='80vh'
          columns={columns({ classes: classes })}
          toolbar={true}
          indexing={false}
          rowactions={rowActions}
          toolbaractions={toolbarActions}
          fetch={fetch}
          csvfilename={'request'}
        />
        <InfoMessageAtom
          message={'Update data'}
          time={3000}
          open={info}
          closeparent={handleInfoClose}
        />
        <AnalysisRequestInfoMessageAtom
          requestreview={requestReview}
          parentclosefunction={handelCloseReview}
          datafunction={requestidfunction}
          staterequest={staterequest}
          profileidfunction={profileidfunction}
          classes={classes}
          baseurlaf={downloadButton.parameters.baseUrlAF}
        ></AnalysisRequestInfoMessageAtom>
      </Grid>
    </Grid>
  );
});
// Type and required properties
MainDataGridMolecule.propTypes = {
  functiondata: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
  columns: PropTypes.func.isRequired,
  classes: PropTypes.object.isRequired,
  staterequest: PropTypes.object.isRequired,
  requestorprofile: PropTypes.shape({
    requestorId: PropTypes.string.isRequired,
  }).isRequired,
  requestidfunction: PropTypes.func.isRequired,
  profileidfunction: PropTypes.func.isRequired,
};
// Default properties
MainDataGridMolecule.defaultProps = {};

export default MainDataGridMolecule;
