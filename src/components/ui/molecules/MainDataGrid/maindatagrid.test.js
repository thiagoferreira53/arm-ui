import React from 'react';
import ReactDOM from 'react-dom';
// Component to be Test
import MainDataGrid from './maindatagrid';
// Test Library
import { render, cleanup } from '@testing-library/react';
import '@testing-library/dom';
import '@testing-library/jest-dom/extend-expect';
import * as reactRedux from 'react-redux';

afterEach(cleanup);
// Props to send component to be rendered
const props = {
  functiondata: () => {},
  title: 'title',
  rowactions: () => {},
  columns: () => [],
  toolbaractions: () => {},
  classes: {
    select: 'analitical_request-makeStyles-select-6',
  },
  staterequest: {},
  requestorprofile: {
    requestorId: '1',
  },
  buttons: [
    {
      id: 'newRequest',
      title: 'Add new analysis request',
      help: 'Add new analysis request',
      color: 'inherit',
      parameters: {
        actionSave: '',
        ruteToPage: '',
      },
    },
    {
      id: 'removeRequest',
      title: 'REMOVE REQUEST',
      help: 'Remove request',
      color: 'inherit',
      parameters: {
        actionSave: '',
      },
    },
    {
      id: 'refresh',
      title: 'REFRESH',
      help: 'Refresh the grid',
      color: 'inherit',
      parameters: {},
    },
    {
      id: 'download',
      title: 'DOWNLOAD',
      help: 'Download',
      color: 'inherit',
      parameters: {
        baseUrlAF: '',
      },
    },
    {
      id: 'notice',
      title: 'Notice',
      help: 'Notice',
      color: 'inherit',
      parameters: {
        seconds: 1,
        localStorageId: 'ba-notific',
      },
    },
  ],
  requestidfunction: jest.fn(),
  profileidfunction: jest.fn(),
};
// mock to use dispatch
const mockDispatch = jest.fn();
jest.mock('react-redux', () => ({
  useSelector: jest.fn(),
  useDispatch: () => mockDispatch,
}));
const useSelectorMock = jest.spyOn(reactRedux, 'useSelector');
const useDispatchMock = jest.spyOn(reactRedux, 'useDispatch');
test('Report name', () => {
  const div = document.createElement('div');
  ReactDOM.render(<MainDataGrid {...props}></MainDataGrid>, div);
});

test('Render correctly', () => {
  const { getByTestId } = render(<MainDataGrid {...props}></MainDataGrid>);
  expect(getByTestId('MainDataGridTestId')).toBeInTheDocument();
});
