import React from 'react'
import PropTypes from 'prop-types'
// CORE COMPONENTS AND ATOMS TO USE
import { Grid, Typography } from '@material-ui/core'
import { Alert } from '@material-ui/lab'

//MAIN FUNCTION
/**
 * Navigation bar with subtitle in the organism
 * @param props: component properties
 * @param ref: reference made by React.forward
 */
const SubTitleMolecule = React.forwardRef((props, ref) => {
  const { children, titleText, message, messageerror, severity, ...rest } = props

  /**Message to display in box to help user */
  let messageDisplayed = message
  /**Put the severity into a message in box to help user */
  let severityDisplayed = severity

  if (messageerror !== '') {
    messageDisplayed = messageerror
    severityDisplayed = 'error'
  }

  return (
    /**
     * @prop data-testid: Id to use inside subtitle.test.js file.
     */
    <Grid
      container
      ref={ref}
      data-testid={'SubTitleTestId'}
      justifyContent={'flex-start'}
      spacing={3}
    >
      <Grid item>
        <Typography variant='h4'>{titleText}</Typography>
      </Grid>
      <Grid item>
        {messageDisplayed !== '' ? (
          <Alert severity={severityDisplayed}>{messageDisplayed}</Alert>
        ) : null}
      </Grid>
    </Grid>
  )
})
// Type and required properties
SubTitleMolecule.propTypes = {
  /**Text to put into subtitle*/
  titleText: PropTypes.string.isRequired,
  /**Severity to put icon in textbox to help user*/
  severity: PropTypes.string,
  /**Text to put into textbox to help user*/
  message: PropTypes.string,
  /**Text to put into textbox to help user and automatic put the severity in error*/
  messageerror: PropTypes.string.isRequired,
}
// Default properties
SubTitleMolecule.defaultProps = {
  titleText: '',
  messageerror: '',
}

export default SubTitleMolecule
