import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
// CORE COMPONENTS AND ATOMS TO USE
import { useDispatch } from 'react-redux'
import SimpleSelectAtom from 'components/ui/atoms/Select/SimpleSelect'

//MAIN FUNCTION
/**
 * @description         - Select to display information with rule in setting configuration
 * @param {object}      props - Component properties.
 * @param {reference}   ref - Reference made by React.forward.
 * @param {string}      props.id - ID to identify the child component
 * @param {string}      props.title - Title to put to the child component.
 * @param {string}      props.actionSave - Action to save values into the react-redux.
 * @param {string}      props.functionData - Function to get the array data to displays.
 * @param {array}       props.occurrences - Array of the occurrences select by the user.
 * @param {string}      props.experimentLocationAnalysisPattern - Variable to save the experiment analysis pattern.
 * @param {string}      props.traitAnalysisPattern - Variable to save the trait analysis pattern.
 * @param {number}      props.analysisObjective - Variable to save the analysis objective.
 * @param {object}      props.classes - Classes to put spesific style with makestyle.
 * @property {string}   idSelect - Id from  the component child.
 * @property {string}   messageError - UseState Message error to display.
 * @property {arrya}    valueDisplays - UseState Array to values displays.
 * @property {boolena}  isRequest - UseState Flag to define when the data is obtained from the service.
 * @property {boolean}  isOnlyOneValue - Usestate Flag to identify when only one value exists and auto-select it.
 * @property {string}   exp - UseState Variable to save the experiment analysis pattern.
 * @property {string}   trait - UseState Variable to save the trait analysis pattern.
 * @function {}         useEffect - Mount coponent and search the data with the experiment and trait analysis pattern.
 * @function {}         useEffect - Mount validate if only existe one item in valueDisplays and auto-select it.
 * @function {}         handleChange - Function to save the element selected into datadisplays.
 * @property {string}   data-testid - Id to use inside analysisconfigurationautoselect.test.js file.
 * @returns             - Combobox component
 */
const AnalysisConfigurationAutoSelectMolecule = React.forwardRef((props, ref) => {
  const {
    children,
    id,
    title,
    actionSave,
    valueDisplay,
    functionData,
    occurrences,
    experimentLocationAnalysisPattern,
    traitAnalysisPattern,
    analysisObjective,
    classes,
    ...rest
  } = props

  const dispatch = useDispatch()
  const idSelect = `${id}-analysis-simple-select`.toString()
  const [messageError, setMessageError] = useState('')
  const [valueDisplays, setValueDisplays] = useState([])
  const [isRequest, setIsRequest] = useState(false)
  const [isOnlyOneValue, setIsOnlyOneValue] = useState(false)
  const [exp, setExp] = useState('')
  const [trait, setTrait] = useState('')

  useEffect(() => {
    if (experimentLocationAnalysisPattern !== '' && traitAnalysisPattern !== '') {
      if (!isRequest) {
        functionData(
          traitAnalysisPattern,
          experimentLocationAnalysisPattern,
          occurrences,
        ).then((res) => {
          setIsRequest(true)
          if (res.status.status === 200) {
            setValueDisplays(res.data)
            setExp(experimentLocationAnalysisPattern)
            setTrait(traitAnalysisPattern)
            if (valueDisplays.length === 1) {
              setIsOnlyOneValue(false)
            }
          } else {
            setMessageError(res.status.message)
          }
        })
      } else {
        if (
          exp != experimentLocationAnalysisPattern ||
          trait != traitAnalysisPattern
        ) {
          setIsRequest(false)
          dispatch({
            type: actionSave,
            payload: 0,
          })
        }
      }
    }
  }, [
    functionData,
    setValueDisplays,
    setMessageError,
    occurrences,
    setIsRequest,
    isRequest,
    experimentLocationAnalysisPattern,
    traitAnalysisPattern,
    analysisObjective,
    exp,
    setExp,
    trait,
    setTrait,
    dispatch,
    actionSave,
    setIsOnlyOneValue,
    valueDisplays,
  ])
  useEffect(() => {
    if (valueDisplays.length === 1 && isRequest && !isOnlyOneValue) {
      setIsOnlyOneValue(true)
      dispatch({
        type: actionSave,
        payload: parseInt(valueDisplays[0].propertyId),
      })
    }
  }, [
    valueDisplays,
    isRequest,
    isOnlyOneValue,
    setIsOnlyOneValue,
    dispatch,
    actionSave,
  ])
  const handleChange = (event) => {
    dispatch({
      type: actionSave,
      payload: parseInt(event.target.value),
    })
  }

  return (
    <div ref={ref} data-testid={'AnalysisConfigurationAutoSelectTestId'}>
      <SimpleSelectAtom
        idselect={idSelect}
        valuedisplay={valueDisplay}
        handlechange={handleChange}
        messageerror={messageError}
        valuedisplays={valueDisplays}
        valueid={'propertyId'}
        valuelabel={'propertyName'}
        title={title}
        classes={classes}
      />
    </div>
  )
})
// Type and required properties
AnalysisConfigurationAutoSelectMolecule.propTypes = {
  id: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  actionSave: PropTypes.string.isRequired,
  functionData: PropTypes.func.isRequired,
  occurrences: PropTypes.array.isRequired,
  experimentLocationAnalysisPattern: PropTypes.string.isRequired,
  traitAnalysisPattern: PropTypes.string.isRequired,
  analysisObjective: PropTypes.number.isRequired,
  classes: PropTypes.object.isRequired,
}
// Default properties
AnalysisConfigurationAutoSelectMolecule.defaultProps = {}

export default AnalysisConfigurationAutoSelectMolecule
