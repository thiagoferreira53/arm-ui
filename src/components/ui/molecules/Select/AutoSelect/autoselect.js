import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
// CORE COMPONENTS AND ATOMS TO USE
import { Grid } from '@material-ui/core'
import { useDispatch } from 'react-redux'
import SimpleSelectAtom from 'components/ui/atoms/Select/SimpleSelect'

//MAIN FUNCTION
/**
 * Combobox that work with simple value and label
 * @param props: component properties
 * @param ref: reference made by React.forward
 */
const AutoSelectMolecule = React.forwardRef((props, ref) => {
  const {
    children,
    id,
    title,
    actionSave,
    valueDisplay,
    functionData,
    occurrences,
    classes,
    ...rest
  } = props

  const dispatch = useDispatch()
  /**Id to put in child component*/
  const idSelect = `${id}-simple-select`.toString()
  /**Array to values displays*/
  const [valueDisplays, setValueDisplays] = useState([])
  /**Message error to display*/
  const [messageError, setMessageError] = useState('')
  /**Flag to define when the data is obtained from the service*/
  const [isRequest, setIsRequest] = useState(false)
  /**Flag to identify when only one value exists and auto-select it*/
  const [isOnlyOneValue, setIsOnlyOneValue] = useState(false)

  /**Mount component and search the data*/
  useEffect(() => {
    if (valueDisplays.length === 0 && !isRequest) {
      functionData(occurrences).then((res) => {
        setIsRequest(true)
        if (res.status.status === 200) {
          setValueDisplays(res.data)
        } else {
          setMessageError(res.status.message)
        }
      })
    }
  }, [
    functionData,
    setValueDisplays,
    setMessageError,
    occurrences,
    setIsRequest,
    isRequest,
    valueDisplays,
  ])

  /**Mount validate if only existe one item in valueDisplays and auto-select it*/
  useEffect(() => {
    if (valueDisplays.length === 1 && isRequest && !isOnlyOneValue) {
      setIsOnlyOneValue(true)
      dispatch({
        type: actionSave,
        payload: valueDisplays[0].propertyId,
      })
    }
  }, [
    valueDisplays,
    isRequest,
    isOnlyOneValue,
    setIsOnlyOneValue,
    dispatch,
    actionSave,
  ])

  /** Function to save the element selected into valuedisplays*/
  const handleChange = (event) => {
    dispatch({
      type: actionSave,
      payload: event.target.value,
    })
  }

  return (
    /**
     * @prop data-testid: Id to use inside autoselect.test.js file.
     */
    <div ref={ref} data-testid={'AutoSelectTestId'}>
      <SimpleSelectAtom
        idselect={idSelect}
        valuedisplay={valueDisplay}
        handlechange={handleChange}
        messageerror={messageError}
        valuedisplays={valueDisplays}
        valueid={'propertyId'}
        valuelabel={'propertyName'}
        title={title}
        classes={classes}
      />
    </div>
  )
})
// Type and required properties
AutoSelectMolecule.propTypes = {
  /**ID to identify the child component*/
  id: PropTypes.string.isRequired,
  /**Title to put to the child component*/
  title: PropTypes.string.isRequired,
  /**Action to save values into the react-redux*/
  actionSave: PropTypes.string.isRequired,
  /**Function to get the array data to displays*/
  functionData: PropTypes.func.isRequired,
  /**Array of the occurrences select by the user*/
  occurrences: PropTypes.array.isRequired,
  /**Classes to put spesific style with makestyle*/
  classes: PropTypes.object.isRequired,
}
// Default properties
AutoSelectMolecule.defaultProps = {}

export default AutoSelectMolecule
