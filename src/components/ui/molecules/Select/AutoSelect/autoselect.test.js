import React from 'react'
import ReactDOM from 'react-dom'
// Component to be Test
import AutoSelect from './autoselect'
// Test Library
import { render, cleanup } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
import * as reactRedux from 'react-redux'

afterEach(cleanup)
// Props to send component to be rendered
const props = {
  id: 'id',
  title: 'title',
  actionSave: 'accion',
  valueDisplay: '',
  functionData: jest.fn().mockImplementation(() =>
    Promise.resolve({
      status: {
        status: 100,
        message: '',
      },
      data: [],
      metadata: {
        pagination: {
          totalPages: 0,
          totalCount: 0,
        },
      },
    }),
  ),
  occurrences: [],
  classes: {
    class: 'class',
  },
}
// mock to use dispatch
const mockDispatch = jest.fn()
jest.mock('react-redux', () => ({
  useSelector: jest.fn(),
  useDispatch: () => mockDispatch,
}))
const useSelectorMock = jest.spyOn(reactRedux, 'useSelector')
const useDispatchMock = jest.spyOn(reactRedux, 'useDispatch')

test('Report name', () => {
  const div = document.createElement('div')
  ReactDOM.render(<AutoSelect {...props}></AutoSelect>, div)
})

test('Render correctly', () => {
  const { getByTestId } = render(<AutoSelect {...props}></AutoSelect>)
  expect(getByTestId('AutoSelectTestId')).toBeInTheDocument()
})
