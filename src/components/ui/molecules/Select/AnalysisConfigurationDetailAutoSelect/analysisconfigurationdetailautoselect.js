import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
// CORE COMPONENTS AND ATOMS TO USE
import { Grid } from '@material-ui/core'
import { useDispatch } from 'react-redux'
import SimpleSelectAtom from 'components/ui/atoms/Select/SimpleSelect'

//MAIN FUNCTION
/**
 * @description -Select to display information with rule in setting configuration and depend the analysis configuration selected.
 * @param {object} props -Component properties.
 * @param {reference} ref  -Reference made by React.forward.
 * @param {string} props.id -ID to identify the child component.
 * @param {string} props.title -Title to put to the child component.
 * @param {string} props.actionSave -Action to save values into the react-redux*.
 * @param {function} props.functionData -Function to get the array data to displays.
 * @param {array} props.occurrences -Array of the occurrences select by the user.
 * @param {number} props.analysisConfiguration -Analysis configuration seleted in previous step.
 * @param {object} props.classes -Classes to put spesific style with makestyle.
 * @property {string} idSelect -Id to put in child component.
 * @property {array} valueDisplays -UseState Array to values displays.
 * @property {string} messageError -Message error to display.
 * @property {boolean} isRequest -UseSatet Flag to define when the data is obtained from the service.
 * @property {boolean} isOnlyOneValue -UseSate Flag to identify when only one value exists and auto-select it.
 * @property {integer} analysisConfigurationLocal -UseState Analysis configuration selected in preview step.
 * @property {array} existValueDisplay -Search if the valueDisplay exist in the valuedisplays array.
 * @function {} useEffect -Mount component and search the data with the analysis configuration.
 * @function {} useEffect -Mount validate if only existe one item in valueDisplays and auto-select it.
 * @function {} handleChange -Function to save the element selected into valuedisplays.
 * @property {string} data-testid -Id to use inside analysisconfigurationdetailautoselect.test.js file.
 * @returns -Combobox component
 */

const AnalysisConfigurationDetailAutoSelectMolecule = React.forwardRef(
  (props, ref) => {
    const {
      children,
      id,
      title,
      actionSave,
      valueDisplay,
      functionData,
      occurrences,
      analysisConfiguration,
      classes,
      ...rest
    } = props
    const dispatch = useDispatch()
    const idSelect = `${id}-analys-config-simple-select`.toString()
    const [valueDisplays, setValueDisplays] = useState([])
    const [messageError, setMessageError] = useState('')
    const [isRequest, setIsRequest] = useState(false)
    const [isOnlyOneValue, setIsOnlyOneValue] = useState(false)
    const [analysisConfigurationLocal, setAnalysisConfigurationLocal] = useState(0)
    const existValueDisplay = valueDisplays.some(
      (value) => value.propertyId === valueDisplay,
    )

    useEffect(() => {
      if (analysisConfiguration !== 0) {
        if (!isRequest) {
          functionData(occurrences, analysisConfiguration).then((res) => {
            setIsRequest(true)
            if (res.status.status === 200) {
              setValueDisplays(res.data)
              setAnalysisConfigurationLocal(analysisConfiguration)
              setIsOnlyOneValue(false)
            } else {
              setMessageError(res.status.message)
            }
          })
        } else {
          if (analysisConfigurationLocal !== analysisConfiguration) {
            setIsRequest(false)
            setIsOnlyOneValue(false)
            dispatch({
              type: actionSave,
              payload: 0,
            })
          }
        }
      } else {
        setIsOnlyOneValue(false)
        setIsRequest(false)
      }
    }, [
      functionData,
      setValueDisplays,
      setMessageError,
      occurrences,
      setIsRequest,
      isRequest,
      analysisConfiguration,
      analysisConfigurationLocal,
      setAnalysisConfigurationLocal,
      actionSave,
      dispatch,
      setIsOnlyOneValue,
    ])
    useEffect(() => {
      if (valueDisplays.length === 1 && isRequest && !isOnlyOneValue) {
        setIsOnlyOneValue(true)
        dispatch({
          type: actionSave,
          payload: valueDisplays[0].propertyId,
        })
      }
    }, [
      valueDisplays,
      isRequest,
      isOnlyOneValue,
      setIsOnlyOneValue,
      dispatch,
      actionSave,
    ])
    const handleChange = (event) => {
      dispatch({
        type: actionSave,
        payload: event.target.value,
      })
    }

    return (
      <div ref={ref} data-testid={'AnalysisConfigurationDetailAutoSelectTestId'}>
        <SimpleSelectAtom
          idselect={idSelect}
          valuedisplay={existValueDisplay ? valueDisplay : ''}
          handlechange={handleChange}
          messageerror={messageError}
          valuedisplays={valueDisplays}
          title={title}
          valueid={'propertyId'}
          valuelabel={'propertyName'}
          classes={classes}
        />
      </div>
    )
  },
)
// Type and required properties
AnalysisConfigurationDetailAutoSelectMolecule.propTypes = {
  id: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  actionSave: PropTypes.string.isRequired,
  functionData: PropTypes.func.isRequired,
  occurrences: PropTypes.array.isRequired,
  analysisConfiguration: PropTypes.number.isRequired,
  classes: PropTypes.object.isRequired,
}
// Default properties
AnalysisConfigurationDetailAutoSelectMolecule.defaultProps = {}

export default AnalysisConfigurationDetailAutoSelectMolecule
