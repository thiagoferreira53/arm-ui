import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
// CORE COMPONENTS AND ATOMS TO USE
import { Grid } from '@material-ui/core'
import ObjectSelectAtom from 'components/ui/atoms/Select/ObjectSelect'
import { useDispatch } from 'react-redux'

//MAIN FUNCTION
/**
 * Combobox that work with object and the id is the object convert into json string
 * @param props: component properties
 * @param ref: reference made by React.forward
 */
const AutoObjectSelectMolecule = React.forwardRef((props, ref) => {
  const {
    children,
    id,
    title,
    actionSave,
    valueDisplay,
    functionData,
    occurrences,
    classes,
    ...rest
  } = props

  const dispatch = useDispatch()
  /**Id to put in child component*/
  const idSelect = `${id}-simple-select`.toString()
  /**Array to values displays*/
  const [valueDisplays, setValueDisplays] = useState([])
  /**Message error to display*/
  const [messageError, setMessageError] = useState('')
  /**Flag to define when the data is obtained from the service*/
  const [isRequest, setIsRequest] = useState(false)
  /**Flag to identify when only one value exists and auto-select it*/
  const [isOnlyOneValue, setIsOnlyOneValue] = useState(false)

  /**Mount component and search the data*/
  useEffect(() => {
    if (valueDisplays.length === 0 && !isRequest) {
      functionData(occurrences).then((res) => {
        setIsRequest(true)
        if (res.status.status === 200) {
          setValueDisplays(res.data)
        } else {
          setMessageError(res.status.message)
        }
      })
    }
  }, [
    functionData,
    setValueDisplays,
    setMessageError,
    occurrences,
    setIsRequest,
    isRequest,
    valueDisplays,
  ])
  /**Mount validate if only existe one item in valueDisplays and auto-select it*/
  useEffect(() => {
    if (valueDisplays.length === 1 && isRequest && !isOnlyOneValue) {
      setIsOnlyOneValue(true)
      dispatch({
        type: actionSave,
        payload: JSON.stringify({
          id: valueDisplays[0].propertyId,
          label: valueDisplays[0].propertyName,
        }),
      })
    }
  }, [
    valueDisplays,
    isRequest,
    isOnlyOneValue,
    setIsOnlyOneValue,
    dispatch,
    actionSave,
  ])

  /** Function to save the element selected into valuedisplays*/
  const handleChange = (event) => {
    dispatch({
      type: actionSave,
      payload: event.target.value,
    })
  }

  /**Validate if valueDisplays has elements if it hasn't elements the valueDisplay will be empty*/
  const value = valueDisplays.length === 0 ? '' : valueDisplay

  return (
    /**
     * @prop data-testid: Id to use inside autoobjectselect.test.js file.
     */
    <Grid
      container
      ref={ref}
      direction='row'
      justify='flex-start'
      alignItems='flex-start'
      data-testid={'AutoObjectSelectTestId'}
    >
      <Grid item xs={'auto'} sm={'auto'} md={'auto'} lg={'auto'} xl={'auto'}>
        <ObjectSelectAtom
          idselect={idSelect}
          valuedisplay={value}
          handlechange={handleChange}
          messageerror={messageError}
          valuedisplays={valueDisplays}
          valueid={'propertyId'}
          valuelabel={'propertyName'}
          title={title}
          classes={classes}
        />
      </Grid>
    </Grid>
  )
})
// Type and required properties
AutoObjectSelectMolecule.propTypes = {
  /**ID to identify the child component*/
  id: PropTypes.string.isRequired,
  /**Title to put to the child component*/
  title: PropTypes.string.isRequired,
  /**Action to save values into the react-redux*/
  actionSave: PropTypes.string.isRequired,
  /**Function to get the array data to displays*/
  functionData: PropTypes.func.isRequired,
  /**Array of the occurrences select by the user*/
  occurrences: PropTypes.array.isRequired,
  /**Classes to put spesific style with makestyle*/
  classes: PropTypes.object.isRequired,
}
// Default properties
AutoObjectSelectMolecule.defaultProps = {}

export default AutoObjectSelectMolecule
