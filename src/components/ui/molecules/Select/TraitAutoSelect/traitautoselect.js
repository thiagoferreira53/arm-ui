import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
// CORE COMPONENTS AND ATOMS TO USE
import { Grid, TextField, Typography } from '@material-ui/core';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { makeStyles } from '@material-ui/core/styles';
import { useDispatch } from 'react-redux';

//MAIN FUNCTION
/**
 * Multi combox to select crop trait
 * @param props: component properties
 * @param ref: reference made by React.forward
 */
const TraitAutoSelectMolecule = React.forwardRef((props, ref) => {
  const {
    children,
    actionSave,
    responseVariableTrait,
    functionData,
    occurrences,
    classes,
    filterenablerow,
    ...rest
  } = props;

  const dispatch = useDispatch();
  /**Data array to select in the combobox  */
  const [responseVariableTraits, setResponseVariableTraits] = React.useState([]);
  /**Flag to define when the data is obtained from the service*/
  const [isRequest, setIsRequest] = React.useState(false);
  /**Validate if the value array was previous selected in case that not put empty array*/
  const value =
    responseVariableTrait.length === 0 || responseVariableTraits.length === 0
      ? []
      : responseVariableTrait;

  /**Mount component and search the data*/
  useEffect(() => {
    if (responseVariableTraits.length === 0 && !isRequest) {
      let traits = functionData(
        occurrences,
        filterenablerow.responseVaraibleOnlyFloat,
      );
      setResponseVariableTraits(traits);
      setIsRequest(true);
    }
  }, [
    functionData,
    setResponseVariableTraits,
    occurrences,
    responseVariableTraits,
    isRequest,
    setIsRequest,
  ]);

  /** Function to save the element selected into valuedisplays*/
  const traitHandleChange = (event, value, reason) => {
    dispatch({
      type: actionSave,
      payload: value,
    });
  };

  return (
    /**
     * @prop data-testid: Id to use inside traitautoselect.test.js file.
     */
    <Grid
      container
      ref={ref}
      direction='row'
      justifyContent='flex-start'
      alignItems='flex-start'
      data-testid={'TraitAutoSelectTestId'}
      className={classes.selectGrid}
    >
      <div className={classes.formSelect}>
        <Typography variant='body1'>{'Response variables'}</Typography>
        <Autocomplete
          multiple
          id='tags-outlined'
          options={responseVariableTraits}
          getOptionLabel={(responseVariableTrait) => responseVariableTrait.title}
          getOptionSelected={(option, value) => option.id === value.id}
          filterSelectedOptions
          size='small'
          value={value}
          onChange={traitHandleChange}
          renderInput={(params) => (
            <TextField {...params} variant='outlined' placeholder='Traits' />
          )}
        />
      </div>
    </Grid>
  );
});
// Type and required properties
TraitAutoSelectMolecule.propTypes = {
  /**Action to save values into the react-redux*/
  actionSave: PropTypes.string.isRequired,
  /**Value array was previous selected*/
  responseVariableTrait: PropTypes.array.isRequired,
  /** Function to get the array data to displays*/
  functionData: PropTypes.func.isRequired,
  /**Array of the occurrences select by the user*/
  occurrences: PropTypes.array.isRequired,
  /**Classes to put spesific style with makestyle*/
  classes: PropTypes.object.isRequired,
  filterenablerow: PropTypes.object.isRequired,
};
// Default properties
TraitAutoSelectMolecule.defaultProps = {
  responseVariableTrait: [],
};

export default TraitAutoSelectMolecule;
