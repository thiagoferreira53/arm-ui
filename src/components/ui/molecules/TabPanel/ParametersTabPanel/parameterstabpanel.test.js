import React from 'react'
import ReactDOM from 'react-dom'
// Component to be Test
import ParametersTabPanel from './parameterstabpanel'
// Test Library
import { render, cleanup } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
import * as reactRedux from 'react-redux'

afterEach(cleanup)
// Props to send component to be rendered
const props = {
  value: 1,
  index: 1,
  selects: {
    responseVariableTrait: {
      responseVariableTrait: [],
      functionData: jest.fn().mockImplementation(() =>
        Promise.resolve({
          status: {
            status: 100,
            message: '',
          },
          data: [],
          metadata: {
            pagination: {
              totalPages: 0,
              totalCount: 0,
            },
          },
        }),
      ),
      actionSave: '',
    },
    analysisObjective: {
      analysisObjective: '',
      functionData: jest.fn().mockImplementation(() =>
        Promise.resolve({
          status: {
            status: 100,
            message: '',
          },
          data: [],
          metadata: {
            pagination: {
              totalPages: 0,
              totalCount: 0,
            },
          },
        }),
      ),
      title: '',
      id: 'ao1',
      actionSave: '',
    },
    traitAnalysisPattern: {
      traitAnalysisPattern: '',
      functionData: jest.fn().mockImplementation(() =>
        Promise.resolve({
          status: {
            status: 100,
            message: '',
          },
          data: [],
          metadata: {
            pagination: {
              totalPages: 0,
              totalCount: 0,
            },
          },
        }),
      ),
      title: '',
      id: 'tap1',
      actionSave: '',
    },
    experimentLocationAnalysisPattern: {
      experimentLocationAnalysisPattern: '',
      functionData: jest.fn().mockImplementation(() =>
        Promise.resolve({
          status: {
            status: 100,
            message: '',
          },
          data: [],
          metadata: {
            pagination: {
              totalPages: 0,
              totalCount: 0,
            },
          },
        }),
      ),
      title: '',
      id: 'elap1',
      actionSave: '',
    },
    analysisConfiguration: {
      analysisConfiguration: '',
      functionData: jest.fn().mockImplementation(() =>
        Promise.resolve({
          status: {
            status: 100,
            message: '',
          },
          data: [],
          metadata: {
            pagination: {
              totalPages: 0,
              totalCount: 0,
            },
          },
        }),
      ),
      title: '',
      id: 'ac1',
      actionSave: '',
    },
    mainModel: {
      mainModel: '',
      functionData: jest.fn().mockImplementation(() =>
        Promise.resolve({
          status: {
            status: 100,
            message: '',
          },
          data: [],
          metadata: {
            pagination: {
              totalPages: 0,
              totalCount: 0,
            },
          },
        }),
      ),
      title: '',
      id: 'mm1',
      actionSave: '',
    },
    prediction: {
      prediction: '',
      functionData: jest.fn(),
      title: '',
      id: 'p1',
      actionSave: '',
    },
    spatialAdjusting: {
      spatialAdjusting: '',
      functionData: jest.fn().mockImplementation(() =>
        Promise.resolve({
          status: {
            status: 100,
            message: '',
          },
          data: [],
          metadata: {
            pagination: {
              totalPages: 0,
              totalCount: 0,
            },
          },
        }),
      ),
      title: '',
      id: 'sp1',
      actionSave: '',
    },
  },
  experiment: {},
  classes: {
    class: 'class',
  },
}
// mock to use dispatch
const mockDispatch = jest.fn()
jest.mock('react-redux', () => ({
  useSelector: jest.fn(),
  useDispatch: () => mockDispatch,
}))
const useSelectorMock = jest.spyOn(reactRedux, 'useSelector')
const useDispatchMock = jest.spyOn(reactRedux, 'useDispatch')
jest.mock('../../Select/AutoObjectSelect', () => () => {
  return <div></div>
})
jest.mock('../../Select/AnalysisConfigurationAutoSelect', () => () => {
  return <div></div>
})
jest.mock('../../Select/AnalysisConfigurationDetailAutoSelect', () => () => {
  return <div></div>
})
test('Report name', () => {
  const div = document.createElement('div')
  ReactDOM.render(<ParametersTabPanel {...props}></ParametersTabPanel>, div)
})

test('Render correctly', () => {
  const { getByTestId } = render(
    <ParametersTabPanel {...props}></ParametersTabPanel>,
  )
  expect(getByTestId('ParametersTabPanelTestId')).toBeInTheDocument()
})
