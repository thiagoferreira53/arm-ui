import React from 'react';
import PropTypes from 'prop-types';
// CORE COMPONENTS AND ATOMS TO USE
import { Collapse, Grid } from '@material-ui/core';
import TraitAutoSelectMolecule from '../../Select/TraitAutoSelect';
import AutoSelectMolecule from '../../Select/AutoSelect';

//MAIN FUNCTION
/**
 * Tab to display setting to the experiment
 * @param props: component properties
 * @param ref: reference made by React.forward
 */
const SettingsTabPanelMolecule = React.forwardRef((props, ref) => {
  const {
    children,
    value,
    index,
    selects,
    experiment,
    classes,
    filterenablerow,
    ...rest
  } = props;
  /**Flag to mont this componet */
  const isIn = value === index;

  return (
    /**
     * @prop data-testid: Id to use inside settingstabpanel.test.js file.
     */
    <Grid
      container
      ref={ref}
      data-testid={'SettingsTabPanelTestId'}
      className={classes.tab}
    >
      <Grid item xs={1}></Grid>
      <Grid item xs={11}>
        <Collapse in={isIn} timeout='auto' unmountOnExit>
          <TraitAutoSelectMolecule
            actionSave={selects.responseVariableTrait.actionSave}
            responseVariableTrait={
              selects.responseVariableTrait.responseVariableTrait
            }
            functionData={selects.responseVariableTrait.functionData}
            occurrences={experiment.experiment}
            classes={classes}
            filterenablerow={filterenablerow}
          />
          <AutoSelectMolecule
            actionSave={selects.analysisObjective.actionSave}
            id={selects.analysisObjective.id}
            title={selects.analysisObjective.title}
            valueDisplay={selects.analysisObjective.analysisObjective}
            functionData={selects.analysisObjective.functionData}
            occurrences={experiment.experiment}
            classes={classes}
          />
        </Collapse>
      </Grid>
    </Grid>
  );
});
// Type and required properties
SettingsTabPanelMolecule.propTypes = {
  /**Index to set the tab selected */
  value: PropTypes.number.isRequired,
  /**Number to identify this tab */
  index: PropTypes.number.isRequired,
  /**Object to create the combobox in parameters */
  selects: PropTypes.object.isRequired,
  /**Experiment selected in the previous tab */
  experiment: PropTypes.object.isRequired,
  /**Classes to put spesific style with makestyle */
  classes: PropTypes.object.isRequired,
  filterenablerow: PropTypes.object.isRequired,
};
// Default properties
SettingsTabPanelMolecule.defaultProps = {};

export default SettingsTabPanelMolecule;
