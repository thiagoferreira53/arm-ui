import React from 'react'
import ReactDOM from 'react-dom'
// Component to be Test
import SettingsTabPanel from './settingstabpanel'
// Test Library
import { render, cleanup } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
import * as reactRedux from 'react-redux'

afterEach(cleanup)
// Props to send component to be rendered
const props = {
  value: 1,
  index: 1,
  selects: {
    responseVariableTrait: {
      actionSave: '',
      functionData: jest.fn().mockImplementation(() =>
        Promise.resolve({
          status: {
            status: 100,
            message: '',
          },
          data: [],
          metadata: {
            pagination: {
              totalPages: 0,
              totalCount: 0,
            },
          },
        }),
      ),
    },
    analysisObjective: {
      actionSave: '',
      functionData: jest.fn().mockImplementation(() =>
        Promise.resolve({
          status: {
            status: 100,
            message: '',
          },
          data: [],
          metadata: {
            pagination: {
              totalPages: 0,
              totalCount: 0,
            },
          },
        }),
      ),
      id: '',
      title: '',
    },
  },
  experiment: {
    experiment: [],
  },
  classes: {
    class: 'class',
  },
}
// mock to use dispatch
const mockDispatch = jest.fn()
jest.mock('react-redux', () => ({
  useSelector: jest.fn(),
  useDispatch: () => mockDispatch,
}))
const useSelectorMock = jest.spyOn(reactRedux, 'useSelector')
const useDispatchMock = jest.spyOn(reactRedux, 'useDispatch')
jest.mock('../../Select/AutoSelect', () => () => {
  return <div></div>
})
jest.mock('../../Select/TraitAutoSelect', () => () => {
  return <div></div>
})
test('Report name', () => {
  const div = document.createElement('div')
  ReactDOM.render(<SettingsTabPanel {...props}></SettingsTabPanel>, div)
})

test('Render correctly', () => {
  const { getByTestId } = render(<SettingsTabPanel {...props}></SettingsTabPanel>)
  expect(getByTestId('SettingsTabPanelTestId')).toBeInTheDocument()
})
