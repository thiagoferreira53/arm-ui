import React from 'react';
import PropTypes from 'prop-types';
// CORE COMPONENTS AND ATOMS TO USE
import { Grid, Collapse } from '@material-ui/core';
import TableMDMolecule from '../../TableExperiment/TableMD';
import { useDispatch } from 'react-redux';

//MAIN FUNCTION
/**
 * @description         Tab componet to show the table master detail with the experiment
 * @param {number}      value - Index to set the tab selected
 * @param {number}      index - Number to identify this tab
 * @param {object}      experiment - Experiment to save the items in the redux
 * @param {object}      classes - Classes to put spesific style with makestyle
 * @param {object}      filterenablerow - Setting for access row filter
 * @param {function}    functionresetfilterrow - Parent function to set setting in local storage
 * @param {string}      ref - reference made by React.forward
 * @property {string}   data-testid - Id to use inside experimentstabpanel.test.js file.
 * @property {bool}     isIn - Flag to mount this componet
 */
const ExperimentsTabPanelMolecule = React.forwardRef(
  (
    { value, index, experiment, classes, filterenablerow, functionresetfilterrow },
    ref,
  ) => {
    const isIn = value === index;


    return (
      <Grid container ref={ref} data-testid={'ExperimentsTabPanelTestId'}>
        <Grid item xs={12}>
          <Collapse in={isIn} timeout='auto' unmountOnExit>
            <TableMDMolecule
              functionData={experiment.functionData}
              columns={experiment.columns}
              actionSave={experiment.accionAddRemove}
              title='Select experiment occurrences for analysis'
              functionDataDetails={experiment.functionDataDetails}
              functionGetTrait={experiment.functionGetTrait}
              classes={classes}
              filterenablerow={filterenablerow}
              functionresetfilterrow={functionresetfilterrow}
            />
          </Collapse>
        </Grid>
      </Grid>
    );
  },
);
// Type and required properties
ExperimentsTabPanelMolecule.propTypes = {
  value: PropTypes.number.isRequired,
  index: PropTypes.number.isRequired,
  experiment: PropTypes.object.isRequired,
  classes: PropTypes.object.isRequired,
  filterenablerow: PropTypes.object.isRequired,
  functionresetfilterrow: PropTypes.func.isRequired,
};
// Default properties
ExperimentsTabPanelMolecule.defaultProps = {};

export default ExperimentsTabPanelMolecule;
