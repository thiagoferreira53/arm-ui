import React from 'react'
import ReactDOM from 'react-dom'
// Component to be Test
import ExperimentsTabPanel from './experimentstabpanel'
// Test Library
import { render, cleanup } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'

afterEach(cleanup)
// Props to send component to be rendered
const props = {
  value: 1,
  index: 1,
  experiment: {
    functionData: jest.fn().mockImplementation(() =>
      Promise.resolve({
        status: {
          status: 100,
          message: '',
        },
        data: [],
        metadata: {
          pagination: {
            totalPages: 0,
            totalCount: 0,
          },
        },
      }),
    ),
    columns: [],
    accionAddRemove: '',
    functionDataDetails: jest.fn().mockImplementation(() =>
      Promise.resolve({
        status: {
          status: 100,
          message: '',
        },
        data: [],
        metadata: {
          pagination: {
            totalPages: 0,
            totalCount: 0,
          },
        },
      }),
    ),
    columnsDetails: [],
    functionGetTrait: jest.fn().mockImplementation(() =>
      Promise.resolve({
        status: {
          status: 100,
          message: '',
        },
        data: [],
        metadata: {
          pagination: {
            totalPages: 0,
            totalCount: 0,
          },
        },
      }),
    ),
  },
  classes: {
    class: 'class',
  },
  filterenablerow: {
    sameExperiemnt: {
      isActive: true,
      isEnable: false,
    },
    responseVaraibleOnlyFloat: {
      isActive: true,
      isEnable: true,
    },
  },
  functionresetfilterrow: jest.fn(),
}
test('Report name', () => {
  const div = document.createElement('div')
  ReactDOM.render(<ExperimentsTabPanel {...props}></ExperimentsTabPanel>, div)
})

test('Render correctly', () => {
  const { getByTestId } = render(
    <ExperimentsTabPanel {...props}></ExperimentsTabPanel>,
  )
  expect(getByTestId('ExperimentsTabPanelTestId')).toBeInTheDocument()
})
