import React from 'react'
import ReactDOM from 'react-dom'
// Component to be Test
import MenuButtonTab from './menubuttontab'
// Test Library
import { render, cleanup } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
import * as reactRedux from 'react-redux'

afterEach(cleanup)
// Props to send component to be rendered
const props = {
  cancelAction: '',
  cancelRoute: '',
  functionSetTab: jest.fn(),
  backDisable: false,
  nextDisable: false,
  nextText: '',
  tapsLength: 1,
  newrequestdata: {},
  routeaftersave: '',
  functionsave: jest.fn(),
}
// mock to use dispatch
const mockDispatch = jest.fn()
jest.mock('react-redux', () => ({
  useSelector: jest.fn(),
  useDispatch: () => mockDispatch,
}))
const useSelectorMock = jest.spyOn(reactRedux, 'useSelector')
const useDispatchMock = jest.spyOn(reactRedux, 'useDispatch')

test('Report name', () => {
  const div = document.createElement('div')
  ReactDOM.render(<MenuButtonTab {...props}></MenuButtonTab>, div)
})

test('Render correctly', () => {
  const { getByTestId } = render(<MenuButtonTab {...props}></MenuButtonTab>)
  expect(getByTestId('MenuButtonTabTestId')).toBeInTheDocument()
})
