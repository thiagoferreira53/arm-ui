import React from 'react';
import PropTypes from 'prop-types';
// CORE COMPONENTS AND ATOMS TO USE
import { Box, Button, Grid, IconButton } from '@material-ui/core';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';

//MAIN FUNCTION
/**
 * Menu to move into the tabs
 * @param props: component properties
 * @param ref: reference made by React.forward
 */
const MenuButtonTabMolecule = React.forwardRef((props, ref) => {
  const {
    children,
    cancelAction,
    cancelRoute,
    functionSetTab,
    backDisable,
    nextDisable,
    nextText,
    tapsLength,
    newrequestdata,
    valuetab,
    routeaftersave,
    functionsave,
    recivemessage,
    alertmessage,
    ...rest
  } = props;
  const dispatch = useDispatch();
  const history = useHistory();

  /**Function to change to the next tab */
  const handleNext = () => {
    if (valuetab + 1 === tapsLength) {
      functionsave({ newRequestData: newrequestdata }).then((res) => {
        if (res.status.status === 201) {
          history.push(routeaftersave);
        } else {
          //console.log(res);
          alertmessage(res.status.message, 'error');
        }
      });
    } else {
      functionSetTab((value) => value + 1);
    }
  };
  /**Function to change to the back tab */
  const handleBack = () => {
    functionSetTab((value) => value - 1);
  };
  /**Functio to cancel and close this component */
  const handleCancel = () => {
    dispatch({
      type: cancelAction,
      payload: '',
    });
    functionSetTab(0);
    history.push(cancelRoute);
  };

  return (
    /**
     * @prop data-testid: Id to use inside menubuttontab.test.js file.
     */
    <Grid
      container
      ref={ref}
      direction='row'
      justifyContent='flex-end'
      data-testid={'MenuButtonTabTestId'}
      spacing={1}
    >
      <Grid item>
        <Button onClick={handleCancel} size={'small'} variant={'contained'}>
          {'Cancel'}
        </Button>
      </Grid>
      <Grid item>
        <Button
          onClick={handleBack}
          variant={'contained'}
          size={'small'}
          color={'secondary'}
          disabled={backDisable}
        >
          {'Back'}
        </Button>
      </Grid>
      <Grid item>
        <Button
          onClick={handleNext}
          variant={'contained'}
          size={'small'}
          color={'primary'}
          disabled={nextDisable}
        >
          {nextText}
        </Button>
      </Grid>
    </Grid>
  );
});
// Type and required properties
MenuButtonTabMolecule.propTypes = {
  /**Redux to action cancel */
  cancelAction: PropTypes.string.isRequired,
  /**Path to change the url in to action cancel */
  cancelRoute: PropTypes.string.isRequired,
  /**Function to set the actual tab */
  functionSetTab: PropTypes.func.isRequired,
  /**Flag to enable back button */
  backDisable: PropTypes.bool.isRequired,
  /**Flag to enable next button */
  nextDisable: PropTypes.bool.isRequired,
  /**Text to next button */
  nextText: PropTypes.string.isRequired,
  /**Number of all tabs */
  tapsLength: PropTypes.number.isRequired,
  /**Object to work the second tab parameters*/
  newrequestdata: PropTypes.object.isRequired,
  /**Path to change the url when finish all tabs */
  routeaftersave: PropTypes.string.isRequired,
  /**Function to save all data */
  functionsave: PropTypes.func.isRequired,
};
// Default properties
MenuButtonTabMolecule.defaultProps = {
  backDisable: true,
  nextDisable: true,
  nextText: 'Next',
};

export default MenuButtonTabMolecule;
