import React from 'react'
import ReactDOM from 'react-dom'
// Component to be Test
import TableSettingButtonMolecule from './tablesettingbutton'
// Test Library
import { render, cleanup } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'

afterEach(cleanup)
// Props to send component to be rendered
const props = {
  columns: [],
  classes: {
    class: 'class',
  },
  handlechangecolumns: jest.fn(),
}

test('Report name', () => {
  const div = document.createElement('div')
  ReactDOM.render(<TableSettingButtonMolecule {...props}></TableSettingButtonMolecule>, div)
})

test('Render correctly search by button', () => {
  const { getByTestId } = render(
    <TableSettingButtonMolecule {...props}></TableSettingButtonMolecule>,
  )
  expect(getByTestId('ButtonIconToolbarTestId')).toBeInTheDocument()
})

test('Render correctly', () => {
  const { getByTestId } = render(
    <TableSettingButtonMolecule {...props}></TableSettingButtonMolecule>,
  )
  expect(getByTestId('TableSettingButtonMoleculeId')).toBeInTheDocument()
})
