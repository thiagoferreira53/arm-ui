import React, { Fragment, useState } from 'react';
import PropTypes from 'prop-types';
// CORE COMPONENTS AND ATOMS TO USE
import {
  Box,
  Dialog,
  DialogContent,
  DialogTitle,
  Grid,
  IconButton,
  Typography,
} from '@material-ui/core';
import SettingsIcon from '@material-ui/icons/Settings';
import CloseIcon from '@material-ui/icons/Close';
import ItemCheckAtom from 'components/ui/atoms/ItemCheck';
import ButtonIconToolbarAtom from 'components/ui/atoms/buttons/ButtonIconToolbar';
//MAIN FUNCTION
/**
 * @description          - Button to configure the table, in this case set visible and hidden column
 * @param {object}       props - Children properties
 * @param {array}        props.columns - Array of the column to set whether it is visible or hidden
 * @param {object}       props.classes - Classes to put spesific style with makestyle
 * @param {function}     props.handlechangecolumns - Parent function to change visible property on columns
 * @param {string}       ref - reference made by React.forward
 * @property {string}    data-testid - Id to use inside configmenutablemaster.test.js file.
 * @property {state}     open - Flag to validate if is open the component
 * @function {void}      handleClose - Function to close the component
 * @function {void}      handleConfigButtonClick - Function to open the component
 * @returns {component}  - Button to configurate columns
 */
const TableSettingButtonMolecule = React.forwardRef((props, ref) => {
  const { children, columns, classes, handlechangecolumns, ...rest } = props;
  const [open, setOpen] = useState(false);

  const handleClose = () => {
    setOpen(false);
  };
  const handleConfigButtonClick = () => {
    setOpen(true);
  };

  return (
    <Box component='div' ref={ref} data-testid={'TableSettingButtonMoleculeId'}>
      <ButtonIconToolbarAtom
        helptext={'Personalize grid settings'}
        functiononclick={handleConfigButtonClick}
        color={'primary'}
        classbutton={classes.button}
      >
        <SettingsIcon />
      </ButtonIconToolbarAtom>
      <Dialog
        onClose={handleClose}
        aria-labelledby='dialog-configuration-column'
        open={open}
        fullWidth={true}
        maxWidth={'sm'}
      >
        <DialogTitle id='dialog-configuration-column-title'>
          <Grid
            container
            direction='row'
            justifyContent='space-between'
            alignItems='flex-start'
          >
            <Grid item>
              <Typography variant='h5'>Costumize columns</Typography>
            </Grid>
            <Grid item>
              <IconButton
                edge='end'
                color='inherit'
                onClick={handleClose}
                aria-label='close'
              >
                <CloseIcon />
              </IconButton>
            </Grid>
          </Grid>
        </DialogTitle>
        <DialogContent dividers>
          {columns.map((column, index) => {
            const key = `${index}_${column.field}`.toString();
            return (
              <Fragment key={key}>
                <ItemCheckAtom
                  label={column.headerName}
                  field={column.field}
                  ischeck={!column.isHidden}
                  handlechangecolumns={handlechangecolumns}
                  iscancheck={
                    column.isCanBeHidden !== undefined ? column.isCanBeHidden : true
                  }
                  classes={classes}
                />
              </Fragment>
            );
          })}
        </DialogContent>
      </Dialog>
    </Box>
  );
});
// Type and required properties
TableSettingButtonMolecule.propTypes = {
  columns: PropTypes.array.isRequired,
  classes: PropTypes.object.isRequired,
  handlechangecolumns: PropTypes.func.isRequired,
};
// Default properties
TableSettingButtonMolecule.defaultProps = {};

export default TableSettingButtonMolecule;
