import React, { Fragment } from 'react'
import ReactDOM from 'react-dom'
// Component to be Test
import RowMaster from './rowmaster'
// Test Library
import { render, cleanup } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
import * as reactRedux from 'react-redux'

afterEach(cleanup)
// Props to send component to be rendered
const props = {
  row: { experimentDbId: 0 },
  columns: [],
  columnsDetails: [],
  functionDataDetails: jest.fn().mockImplementation(() =>
    Promise.resolve({
      status: {
        status: 100,
        message: '',
      },
      data: [],
      metadata: {
        pagination: {
          totalPages: 0,
          totalCount: 0,
        },
      },
    }),
  ),
  actionSave: 'accion',
  functionGetTrait: jest.fn().mockImplementation(() =>
    Promise.resolve({
      status: {
        status: 100,
        message: '',
      },
      data: [],
      metadata: {
        pagination: {
          totalPages: 0,
          totalCount: 0,
        },
      },
    }),
  ),
  numberVisibleColumn: 1,
  columnwidth: 1,
  actionColumnWidth: 1,
  classes: {
    class: 'class',
  },
  issearchrow: false,
  filterenablerow: {
    sameExperiemnt: {
      isActive: true,
      isEnable: false
    },
    responseVaraibleOnlyFloat: {
      isActive: true,
      isEnable: true,
    },
  },
}
// mock to use dispatch
const mockDispatch = jest.fn()
jest.mock('react-redux', () => ({
  useSelector: jest.fn(),
  useDispatch: () => mockDispatch,
}))
const useSelectorMock = jest.spyOn(reactRedux, 'useSelector')
const useDispatchMock = jest.spyOn(reactRedux, 'useDispatch')
useSelectorMock.mockReturnValue({ state: { newRequest: { experiment: [] } } })

test('Report name', () => {
  const div = document.createElement('table')
  ReactDOM.render(
    <tbody>
      <RowMaster {...props}></RowMaster>
    </tbody>,
    div,
  )
})

test('Render correctly', () => {
  const { getByTestId } = render(
    <table>
      <tbody>
        <Fragment>
          <RowMaster {...props}></RowMaster>
        </Fragment>
      </tbody>
    </table>,
  )
  expect(getByTestId('RowMasterTestId')).toBeInTheDocument()
})
