import React, { Fragment, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
// CORE COMPONENTS AND ATOMS TO USE
import {
  IconButton,
  LinearProgress,
  Radio,
  TableCell,
  TableRow,
} from '@material-ui/core';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import RowGroupDetailMolecule from '../RowGroupDetail';
import { useSelector } from 'react-redux';

//MAIN FUNCTION
/**
 * @description           Row of the master table.
 * @param {object}        row -Row master.
 * @param {array}         columns -Array of the columns.
 * @param {function}      functionDataDetails -Function to get the row details.
 * @param {function}      functionGetTrait -Function to get the trait.
 * @param {number}        actionColumnWidth -Width of the action column.
 * @param {object}        classes -Classes to put spesific style with makestyle.
 * @param {number}        columnwidth -Width of the column in css style.
 * @param {boolean}       issearchrow -Flag to indicate when to find the row, if true, the rows are in the process of searching, on the other hand when a false row was found.
 * @param {object}        filterenablerow - Object to enable or disable selection row
 * @param {object}        filterenablerow.sameExperiemnt - Filter to select only one experiment
 * @param {boolean}       filterenablerow.sameExperiemnt.isActive - Activate or disable filter
 * @param {boolean}       filterenablerow.sameExperiemnt.isEnalbe - Enable component to activate or disable it
 * @param {object}        filterenablerow.responseVaraibleOnlyFloat - Filter to select only respose varaible (trait) type float
 * @param {boolean}       filterenablerow.responseVaraibleOnlyFloat.isActive - Activate or disable filter
 * @param {boolean}       filterenablerow.responseVaraibleOnlyFloat.isEnalbe - Enable component to activate or disable it
 * @param {reference}     ref - Reference made by React.forward.
 * @property {boolean}    open -UseState Flag to open or hidden the row detail.
 * @property {array}      rowsDetails -UseSatet Row detail of eahc row master.
 * @property {integer}    idToSearch -ID to search experiment DB.
 * @property {boolean}    check -UseState Flag to put the radio button when select one experiment [occurence].
 * @property {array}      experiments -UseSelector Redux to get the list of experiment selected.
 * @property {integer}    actionCheck=[-1,0,1] -UseState Action to indicate 3 states -1 = unchecked radio button, 0 = is not selected the radio button, 1= check the radio button.
 * @property {boolean}    disableCheck -UseState, enable or disable the check button when is selecte another experiment.
 * @function {void}       useEffect -If the experiement [occurrences] selected contains the experimentDbId put the check in true.
 * @function {void}       handelArrowOpen -Function to open row details.
 * @function {void}       handelCheck -Function to set flag check by child compoment.
 * @property {string}     data-testid -Id to use inside rowmaster.test.js file.
 * @returns {component}   Row to display in the table with master values
 */
const RowMasterMolecule = React.forwardRef(
  (
    {
      row,
      columns,
      functionDataDetails,
      functionGetTrait,
      actionColumnWidth,
      classes,
      columnwidth,
      issearchrow,
      actionSave,
      filterenablerow,
    },
    ref,
  ) => {
    const [open, setOpen] = useState(false);
    const [rowsDetails, setRowsDetails] = useState([]);
    const idToSearch = row.experimentDbId;
    const [check, setCheck] = useState(false);
    const experiments = useSelector((state) => state.newRequest.experiment);
    const [actionCheck, setActionCheck] = useState(0);
    const [disableCheck, setDisableCheck] = useState(false);

    useEffect(() => {
      if (experiments.length > 0) {
        if (
          experiments.findIndex(
            (experiment) => experiment.experimentDbId === row.experimentDbId,
          ) > -1
        ) {
          setCheck(true);
        }
      }
    }, [experiments, row, setCheck, setOpen, open]);
    useEffect(() => {
      if (issearchrow && rowsDetails.length > 0) {
        setRowsDetails([]);
        setOpen(false);
      }
    }, [issearchrow, rowsDetails, setRowsDetails, setOpen]);
    const handelArrowOpen = () => {
      setOpen(!open);
      functionDataDetails(idToSearch).then((result) => {
        if (result.status.status === 200) {
          setRowsDetails(result.data);
        }
      });
    };
    const handelCheck = () => {
      setCheck(!check);
      setActionCheck(!check ? 1 : -1);
      if (!open) {
        setOpen(true);
        functionDataDetails(idToSearch).then((result) => {
          if (result.status.status === 200) {
            setRowsDetails(result.data);
          }
        });
      }
    };

    return (
      <Fragment>
        <TableRow
          hover
          role='checkbox'
          tabIndex={-1}
          ref={ref}
          data-testid={'RowMasterTestId'}
        >
          <TableCell width={actionColumnWidth}>
            <IconButton
              aria-label='expand row'
              size='small'
              onClick={handelArrowOpen}
            >
              {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowRight />}
            </IconButton>
            <Radio checked={check} onClick={handelCheck} disabled={disableCheck} />
          </TableCell>
          <TableCell width={actionColumnWidth}></TableCell>
          {columns.map((column, index) => {
            const value = column.fieldMaster === '' ? '' : row[column.fieldMaster];
            const width = column.width !== undefined ? column.width : columnwidth;
            const key = `${index}${row.occurrenceDbId}${column.field}`.toString();
            if (!column.isHidden) {
              return (
                <TableCell key={key} width={width}>
                  {value}
                </TableCell>
              );
            }
          })}
        </TableRow>
        {rowsDetails.length === 0 && open ? (
          <Fragment>
            <TableRow>
              <TableCell colSpan={columns.length + 2}>
                <LinearProgress />
              </TableCell>
            </TableRow>
          </Fragment>
        ) : (
          <Fragment />
        )}
        {open && (
          <RowGroupDetailMolecule
            rowdetails={rowsDetails}
            columns={columns}
            actionSave={actionSave}
            onlyoneexperiment={true}
            functiongettrait={functionGetTrait}
            actioncolumnwidth={actionColumnWidth}
            columnwidth={columnwidth}
            classes={classes}
            actioncheck={actionCheck}
            filterenablerow={filterenablerow}
          ></RowGroupDetailMolecule>
        )}
      </Fragment>
    );
  },
);
// Type and required properties
RowMasterMolecule.propTypes = {
  row: PropTypes.object.isRequired,
  columns: PropTypes.array.isRequired,
  functionDataDetails: PropTypes.func.isRequired,
  functionGetTrait: PropTypes.func.isRequired,
  actionColumnWidth: PropTypes.number.isRequired,
  classes: PropTypes.object.isRequired,
  columnwidth: PropTypes.number.isRequired,
  issearchrow: PropTypes.bool.isRequired,
  saveoccurrence: PropTypes.func,
  actionSave: PropTypes.string,
  filterenablerow: PropTypes.shape({
    sameExperiemnt: PropTypes.shape({
      isActive: PropTypes.bool.isRequired,
      isEnable: PropTypes.bool.isRequired,
    }).isRequired,
    responseVaraibleOnlyFloat: PropTypes.shape({
      isActive: PropTypes.bool.isRequired,
      isEnable: PropTypes.bool.isRequired,
    }).isRequired,
  }).isRequired,
};
// Default properties
RowMasterMolecule.defaultProps = {};

export default RowMasterMolecule;
