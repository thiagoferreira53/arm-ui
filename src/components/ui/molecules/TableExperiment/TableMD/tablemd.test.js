import React from 'react';
import ReactDOM from 'react-dom';
// Component to be Test
import TableMD from './tablemd';
// Test Library
import { render, cleanup } from '@testing-library/react';
import '@testing-library/dom';
import '@testing-library/jest-dom/extend-expect';

afterEach(cleanup);
// Props to send component to be rendered
const props = {
  functionData: jest.fn().mockImplementation(() =>
    Promise.resolve({
      status: {
        status: 100,
        message: '',
      },
      data: [],
      metadata: {
        pagination: {
          totalPages: 0,
          totalCount: 0,
        },
      },
    }),
  ),
  columns: [],
  functionDataDetails: jest.fn().mockImplementation(() =>
    Promise.resolve({
      status: {
        status: 100,
        message: '',
      },
      data: [],
      metadata: {
        pagination: {
          totalPages: 0,
          totalCount: 0,
        },
      },
    }),
  ),
  columnsDetails: [],
  title: '',
  actionSave: 'accion',
  functionGetTrait: jest.fn().mockImplementation(() =>
    Promise.resolve({
      status: {
        status: 100,
        message: '',
      },
      data: [],
      metadata: {
        pagination: {
          totalPages: 0,
          totalCount: 0,
        },
      },
    }),
  ),
  classes: {
    class: 'class',
  },
  filterenablerow: {
    sameExperiemnt: {
      isActive: true,
      isEnable: false,
    },
    responseVaraibleOnlyFloat: {
      isActive: true,
      isEnable: true,
    },
  },
};

test('Report name', () => {
  const div = document.createElement('div');
  ReactDOM.render(<TableMD {...props}></TableMD>, div);
});

test('Render correctly', () => {
  const { getByTestId } = render(<TableMD {...props}></TableMD>);
  expect(getByTestId('TableMDTestId')).toBeInTheDocument();
});
