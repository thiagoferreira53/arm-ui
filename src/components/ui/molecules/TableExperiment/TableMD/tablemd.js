import React, { Fragment, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
// CORE COMPONENTS AND ATOMS TO USE
import {
  Grid,
  TableBody,
  TableContainer,
  Table,
  TableHead,
  LinearProgress,
} from '@material-ui/core';
import MessageRowAtom from 'components/ui/atoms/MessageRow';
import HeadTableMasterMolecule from '../HeadTableMaster';
import ColumnMasterMolecule from '../ColumnMaster';
import RowMasterMolecule from '../RowMaster';

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}
function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}
function stableSort(array, comparator) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map((el) => el[0]);
}

//MAIN FUNCTION
/**
 * @description         A table with function to hide and show rows by the definition of the master details table, and pagination table.
 * @param {function}    functiondata -Function to get the data for rows masters.
 * @param {array}       columns -Array of the columns.
 * @param {function}    functiondatadetails -Function to get the data for rows details.
 * @param {string}      title -Title of the table.
 * @param {string}      saveoccurrence - Function to save occurrences.
 * @param {function}    functiongettrait -Function to get the traits.
 * @param {object}      classes -Classes to put spesific style with makestyle.
 * @param {object}      filterenablerow - Object to enable or disable selection row
 * @param {function}    functionresetfilterrow - Parent function to reset filter row
 * @param {reference}   ref -Reference made by React.forward.
 * @property {integer}  actionColumnWidth -Width of the action column.
 * @property {array}    paginationSizes -Array of the pagination to selected one.
 * @property {array}    rows - UseState Master rows to display it.
 * @property {boolean}  isSearchRow -UseState Flag to indicate that need update the data from the service API.
 * @property {string}   messageError -UseState Text to display in case of error.
 * @property {integer}  totalCount -UseState Total of the rows in the service.
 * @property {integer}  rowsPerPage -UseState Row por page to display.
 * @property {integer}  page -UseState Actual page.
 * @property {string}   order=[asc|desc] -UseState Order the table [asc, desc].
 * @property {string}   orderBy -UseState ID column to sort the data.
 * @property {arrya}    searchParameters -UseState Parameter to filter the columns.
 * @property {array}    columnsWork -UseState Copy of the columns to be modified by the component.
 * @property {boolean}  isVisibleHide -UseState Flag to put the column in hidden.
 * @property {string}   fileHide -UseState Hide a specific column.
 * @property {boolean}  isHide -UseState  Flag to hidden columns.
 * @property {array}    numberVisibleColumn -Number of the visible column to alig other interna.
 * @property {integer}  columnWidth -Default of the width for the columns.
 * @property {string}   data-testid -Id to use inside tablemd.test.js file.
 * @function {void}     useEffect -Get the firs time the row master.
 * @function {void}     useEffect -If flag isHiden change the columns change the atribute hide.
 * @function {void}     handleChangePage -Function to change the page.
 * @function {void}     handleChangeRowsPerPage -Function to chnage the number of the row per page.
 * @function {void}     handleRequestSort -Function to change the order of the sort.
 * @function {void}     handleSetFilter -Function to search into the rows.
 * @function {void}     handleChangeColumns -Function to change the visible columns porperty.
 * @function {integer}  descendingComparator -Functio to order one column.
 * @function {array}    getComparator -Function to compare asc or desc.
 * @function {array}    stableSort -Function to order the wor by a select column.
 * @function {object}   Search local storage filter row configuration
 * @returns {component} Return a table with master and detailt properties
 */
const TableMDMolecule = React.forwardRef(
  (
    {
      functionData,
      columns,
      functionDataDetails,
      title,
      actionSave,
      functionGetTrait,
      classes,
      filterenablerow,
      functionresetfilterrow,
    },
    ref,
  ) => {
    const actionColumnWidth = 4;
    const paginationSizes = [10, 25, 50, 100];
    const [rows, setRows] = useState([]);
    const [isSearchRow, setIsSearchRow] = useState(true);
    const [messageError, setMessageError] = useState('');
    const [totalCount, setTotalCount] = useState(0);
    const [rowsPerPage, setRowsPerPage] = useState(10);
    const [page, setPage] = useState(0);
    const [order, setOrder] = useState('asc');
    const [orderBy, setOrderBy] = useState('experimentName');
    const [searchParameters, setSearchParameters] = useState([]);
    const [columnsWork, setColumnsWork] = useState(columns);
    const [isVisibleHide, setIsVisibleHide] = useState(false);
    const [fileHide, setFieldHide] = useState('');
    const [isHide, setIsHide] = useState(false);
    const numberVisibleColumn = columnsWork.filter(
      (column) => !column.isHidden,
    ).length;
    const columnWidth = 100;


    useEffect(() => {
      let isMounted = true;
      if (isSearchRow) {
        functionData(rowsPerPage, 1, searchParameters).then((res) => {
          if (isMounted) {
            if (res.status.status === 200) {
              setRows(res.data);
              setIsSearchRow(false);
              setPage(0);
              setTotalCount(res.metadata.pagination.totalCount);
            } else {
              setMessageError(res.status.message);
            }
          }
        });
      }
      return () => {
        isMounted = false;
      };
    }, [
      rows,
      isSearchRow,
      functionData,
      setRows,
      setIsSearchRow,
      setPage,
      setTotalCount,
      rowsPerPage,
      searchParameters,
    ]);
    useEffect(() => {
      if (isHide) {
        let newColumn = columnsWork;
        newColumn.find((column) => {
          if (column.field === fileHide) {
            column.isHidden = isVisibleHide;
            return column;
          }
        });
        setColumnsWork(newColumn);
        setIsHide(false);
      }
    }, [fileHide, isVisibleHide, setColumnsWork, columnsWork, setIsHide, isHide]);

    const handleChangePage = (event, newPage) => {
      let pageService = newPage;
      pageService++;
      functionData(rowsPerPage, pageService, searchParameters).then((res) => {
        if (res.status.status === 200) {
          setRows(res.data);
          setPage(newPage);
          setTotalCount(res.metadata.pagination.totalCount);
        } else {
          setMessageError(res.status.message);
        }
      });
    };
    const handleChangeRowsPerPage = (event) => {
      setRowsPerPage(parseInt(event.target.value, 10));
      setRows([]);
      functionData(parseInt(event.target.value, 10), 1, searchParameters).then(
        (res) => {
          if (res.status.status === 200) {
            setRows(res.data);
            setPage(0);
            setTotalCount(res.metadata.pagination.totalCount);
          } else {
            setMessageError(res.status.message);
          }
        },
      );
    };
    const handleRequestSort = (event, property) => {
      const isAsc = orderBy === property && order === 'asc';
      setOrder(isAsc ? 'desc' : 'asc');
      setOrderBy(property);
    };
    const handleSetFilter = (event, property) => {
      let parameters = searchParameters;
      let search = event.target.value;
      if (search === '') {
        delete parameters[property];
      } else {
        parameters[property] = `${search}%`.toString();
      }
      setSearchParameters(parameters);
      setIsSearchRow(true);
      setRows([]);
    };
    const handleChangeColumns = (field, isVisible) => {
      setIsVisibleHide(isVisible);
      setFieldHide(field);
      setIsHide(true);
    };

    const handelClearRowAndSearch = () =>{
      setRows([]);
      setIsSearchRow(true);
    }

    return (
      <Grid container ref={ref} data-testid={'TableMDTestId'}>
        <Grid item xs={12}>
          <HeadTableMasterMolecule
            title={title}
            paginationSizes={paginationSizes}
            totalCount={totalCount}
            rowsPerPage={rowsPerPage}
            page={page}
            columns={columnsWork}
            classes={classes}
            handlechangepage={handleChangePage}
            handlechangerowsperpage={handleChangeRowsPerPage}
            handlechangecolumns={handleChangeColumns}
            filterenablerow={filterenablerow}
            functionresetfilterrow={functionresetfilterrow}
            handleclearrownandsearch={handelClearRowAndSearch}
          />
          <TableContainer>
            <Table>
              <TableHead>
                <ColumnMasterMolecule
                  columns={columnsWork}
                  onRequestSort={handleRequestSort}
                  onFilterColumn={handleSetFilter}
                  order={order}
                  orderBy={orderBy}
                  rowCount={columnsWork.length}
                  actionColumnWidth={actionColumnWidth}
                  classes={classes}
                  columnwidth={columnWidth}
                ></ColumnMasterMolecule>
              </TableHead>
              <TableBody>
                {stableSort(rows, getComparator(order, orderBy)).map(
                  (row, index) => {
                    const key = `rowMaster_${index}`.toString();
                    return (
                      <Fragment key={key}>
                        <RowMasterMolecule
                          row={row}
                          columns={columnsWork}
                          functionDataDetails={functionDataDetails}
                          functionGetTrait={functionGetTrait}
                          actionColumnWidth={actionColumnWidth}
                          classes={classes}
                          columnwidth={columnWidth}
                          issearchrow={isSearchRow}
                          actionSave={actionSave}
                          filterenablerow={filterenablerow}
                        ></RowMasterMolecule>
                      </Fragment>
                    );
                  },
                )}
              </TableBody>
            </Table>
            <MessageRowAtom
              message={messageError}
              numberVisibleColumn={numberVisibleColumn}
            ></MessageRowAtom>
            {rows.length === 0 ? <LinearProgress /> : <Fragment />}
          </TableContainer>
        </Grid>
      </Grid>
    );
  },
);
// Type and required properties
TableMDMolecule.propTypes = {
  functionData: PropTypes.func.isRequired,
  columns: PropTypes.array.isRequired,
  functionDataDetails: PropTypes.func,
  title: PropTypes.string.isRequired,
  actionSave: PropTypes.string.isRequired,
  functionGetTrait: PropTypes.func.isRequired,
  classes: PropTypes.object.isRequired,
  filterenablerow: PropTypes.object.isRequired,
  functionresetfilterrow: PropTypes.func.isRequired,
};
// Default properties
TableMDMolecule.defaultProps = {};

export default TableMDMolecule;
