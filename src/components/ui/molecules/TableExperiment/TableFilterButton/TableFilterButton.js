import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
// GLOBALIZATION COMPONENT
import { Core } from '@ebs/styleguide';
// CORE COMPONENTS
const { Box, Typography } = Core;
import ButtonIconToolbarAtom from '../../../atoms/buttons/ButtonIconToolbar';
import FilterListIcon from '@material-ui/icons/FilterList';
import {
  Button,
  Dialog,
  DialogContent,
  DialogTitle,
  Grid,
  IconButton,
} from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import ItemCheckAtom from '../../../atoms/ItemCheck';
import ButtonToolbarAtom from '../../../atoms/buttons/ButtonToolbar';

//MAIN FUNCTION
/**
 * @description           Button to select filter to select the occurrences
 * @param {object}        classes - Classes to put spesific style with makestyle
 * @param {object}        filterenablerow - Object to enable or disable selection row
 * @param {object}        filterenablerow.sameExperiemnt - Filter to select only one experiment
 * @param {boolean}       filterenablerow.sameExperiemnt.isActive - Activate or disable filter
 * @param {boolean}       filterenablerow.sameExperiemnt.isEnalbe - Enable component to activate or disable it
 * @param {object}        filterenablerow.responseVaraibleOnlyFloat - Filter to select only respose varaible (trait) type float
 * @param {boolean}       filterenablerow.responseVaraibleOnlyFloat.isActive - Activate or disable filter
 * @param {boolean}       filterenablerow.responseVaraibleOnlyFloat.isEnalbe - Enable component to activate or disable it
 * @param {function}      functionresetfilterrow - Parent function to reset filter row
 * @param {function}      handleclearrownandsearch - Clear all row and search agait
 * @param {string}        ref - reference made by React.forward
 * @property {string}     data-testid - Id to use inside TableFilterButton.test.js file.
 * @returns {componet}    Button to display filter component
 */
const TableFilterButtonMolecule = React.forwardRef(
  ({ classes, filterenablerow, functionresetfilterrow, handleclearrownandsearch }, ref) => {
    const [open, setOpen] = useState(false);
    const [isEnableSave, setIsEnableSave] = useState(false);
    const [filter, setFilter] = useState([]);
    const [isFillFilter, setIsFillFilter] = useState(true);

    useEffect(() => {
      if (isFillFilter) {
        setFilter([
          {
            id: 'sameExperiemnt',
            previous: filterenablerow.sameExperiemnt.isActive,
            new: null,
          },
          {
            id: 'responseVaraibleOnlyFloat',
            previous: filterenablerow.responseVaraibleOnlyFloat.isActive,
            new: null,
          },
        ]);
        setIsFillFilter(false);
      }
    }, [filter, setFilter, filterenablerow,isFillFilter, setIsFillFilter]);

    const handleClose = () => {
      setOpen(false);
    };
    const handleButtonClick = () => {
      setOpen(true);
    };
    const handleButtonCheck = (field, isCheck) => {
      const checked =!isCheck;
      setFilter((item) => {
        const pos = item.findIndex((x) => x.id === field);
        if (pos > -1) item[pos].new = checked;
        return item;
      });
      let newFilter = filter.filter((item) => {
        if( item.id === field ) {
          if (item.previous !== checked ) return item;
        }else {
          if (item.new !== null && item.new !== item.previous) return item;
        }
      });
      setIsEnableSave(newFilter.length > 0 ? true : false);
    };
    const handleSave = () => {
      setIsFillFilter(true);
      setOpen(false);
      handleclearrownandsearch();
      functionresetfilterrow(filter);
      
    };

    return (
      <Box component='div' ref={ref} data-testid={'TableFilterButtonTestId'}>
        <ButtonIconToolbarAtom
          helptext={'Update filter for occurrences'}
          functiononclick={handleButtonClick}
          color={'primary'}
          classbutton={classes.button}
        >
          <FilterListIcon />
        </ButtonIconToolbarAtom>
        <Dialog
          onClose={handleClose}
          aria-labelledby='dialog-configuration-column'
          open={open}
          fullWidth={true}
          maxWidth={'sm'}
        >
          <DialogTitle id='dialog-configuration-column-title'>
            <Grid
              container
              direction='row'
              justifyContent='space-between'
              alignItems='flex-start'
            >
              <Grid item>
                <Typography variant='h5'>Filter for select occurrences</Typography>
              </Grid>
              <Grid item>
                <IconButton
                  edge='end'
                  color='inherit'
                  onClick={handleClose}
                  aria-label='close'
                >
                  <CloseIcon />
                </IconButton>
              </Grid>
            </Grid>
          </DialogTitle>
          <DialogContent dividers>
            <ItemCheckAtom
              label={'Select only one experiment'}
              field={'sameExperiemnt'}
              ischeck={filterenablerow.sameExperiemnt.isActive}
              handlechangecolumns={handleButtonCheck}
              iscancheck={filterenablerow.sameExperiemnt.isEnable}
              classes={classes}
            ></ItemCheckAtom>
            <ItemCheckAtom
              label={'Select only occurrences that have traits that are continuous variables'}
              field={'responseVaraibleOnlyFloat'}
              ischeck={filterenablerow.responseVaraibleOnlyFloat.isActive}
              handlechangecolumns={handleButtonCheck}
              iscancheck={filterenablerow.responseVaraibleOnlyFloat.isEnable}
              classes={classes}
            ></ItemCheckAtom>
            <Grid
              container
              direction='row-reverse'
              justifyContent='space-between'
              alignItems='flex-end'
            >
              <Grid item>
                <ButtonToolbarAtom
                  helptext={'Save new setting to filter'}
                  classbutton={classes.button}
                  functiononclick={handleSave}
                  title={'Save'}
                  isenable={isEnableSave}
                ></ButtonToolbarAtom>
              </Grid>
            </Grid>
          </DialogContent>
        </Dialog>
      </Box>
    );
  },
);
// Type and required properties
TableFilterButtonMolecule.propTypes = {
  classes: PropTypes.object.isRequired,
  filterenablerow: PropTypes.shape({
    sameExperiemnt: PropTypes.shape({
      isActive: PropTypes.bool.isRequired,
      isEnable: PropTypes.bool.isRequired,
    }).isRequired,
    responseVaraibleOnlyFloat: PropTypes.shape({
      isActive: PropTypes.bool.isRequired,
      isEnable: PropTypes.bool.isRequired,
    }).isRequired,
  }).isRequired,
  functionresetfilterrow:  PropTypes.func.isRequired,
  handleclearrownandsearch: PropTypes.func.isRequired,  
};
// Default properties
TableFilterButtonMolecule.defaultProps = {};

export default TableFilterButtonMolecule;
