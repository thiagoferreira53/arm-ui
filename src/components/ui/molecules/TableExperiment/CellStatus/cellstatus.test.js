import React from 'react'
import ReactDOM from 'react-dom'
// Component to be Test
import CellStatus from './cellstatus'
// Test Library
import { render, cleanup } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'

afterEach(cleanup)
// Props to send component to be rendered
const props = {
  isError: false,
  severity: 'error',
  message: '',
}

test('Report name', () => {
  const div = document.createElement('div')
  ReactDOM.render(<CellStatus {...props}></CellStatus>, div)
})

test('Render correctly', () => {
  const { getByTestId } = render(<CellStatus {...props}></CellStatus>)
  expect(getByTestId('CellStatusTestId')).toBeInTheDocument()
})
