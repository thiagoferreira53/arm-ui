import React from 'react'
import ReactDOM from 'react-dom'
// Component to be Test
import RowGroupDetail from './rowgroupdetail'
// Test Library
import { render, cleanup } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
import * as reactRedux from 'react-redux'

afterEach(cleanup)
// Props to send component to be rendered
const props = {
  rowdetails: [{ experimentDbId: 0, traits: [] }],
  columns: [],
  actionSave: 'save',
  actioncheck: 0,
  onlyoneexperiment: false,
  functiongettrait: jest.fn().mockImplementation(() =>
    Promise.resolve({
      status: {
        status: 100,
        message: '',
      },
      data: [],
      metadata: {
        pagination: {
          totalPages: 0,
          totalCount: 0,
        },
      },
    }),
  ),
  numbervisiblecolumn: 1,
  columnwidth: 1,
  actioncolumnwidth: 1,
  classes: {
    class: 'class',
  },
  filterenablerow: {
    sameExperiemnt: {
      isActive: true,
      isEnable: false
    },
    responseVaraibleOnlyFloat: {
      isActive: true,
      isEnable: true,
    },
  },
}
// mock to use dispatch
const mockDispatch = jest.fn()
jest.mock('react-redux', () => ({
  useSelector: jest.fn(),
  useDispatch: () => mockDispatch,
}))
const useSelectorMock = jest.spyOn(reactRedux, 'useSelector')
const useDispatchMock = jest.spyOn(reactRedux, 'useDispatch')
useSelectorMock.mockReturnValue({ state: { newRequest: { experiment: [] } } })

test('Report name', () => {
  const div = document.createElement('div')
  ReactDOM.render(
    <table>
      <tbody>
        <RowGroupDetail {...props}></RowGroupDetail>
      </tbody>
    </table>,
    div,
  )
})

test('Render correctly', () => {
  const { getByTestId } = render(
    <table>
      <tbody>
        <RowGroupDetail {...props}></RowGroupDetail>
      </tbody>
    </table>,
  )
  //expect(getByTestId('RowGroupDetailTestId')).toBeInTheDocument()
})
