import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
// CORE COMPONENTS AND ATOMS TO USE
import { TableRow } from '@material-ui/core';
import RowDetailMolecule from '../RowDetail';

//MAIN FUNCTION
/**
 * @description           Group of row detail
 * @param {object}        rowdetail - Object with values od the row
 * @param {array}         columns - Array of the columns
 * @param {string}        actionSave - Action to save values into the react-redux
 * @param {function}      functionGetTrait - Width of the action column
 * @param {number}        actionColumnWidth - Width of the action column
 * @param {number}        columnwidth - Width of the column
 * @param {object}        classes - Classes to put spesific style with makestyle
 * @param {number}        actioncheck [-1|0|1] - Parrent action to indicate 3 states -1 = unchecked radio button, 0 = is not selected the radio button, 1= check the radio button
 * @param {object}        filterenablerow - Object to enable or disable selection row
 * @param {object}        filterenablerow.sameExperiemnt - Filter to select only one experiment
 * @param {boolean}       filterenablerow.sameExperiemnt.isActive - Activate or disable filter
 * @param {boolean}       filterenablerow.sameExperiemnt.isEnalbe - Enable component to activate or disable it
 * @param {object}        filterenablerow.responseVaraibleOnlyFloat - Filter to select only respose varaible (trait) type float
 * @param {boolean}       filterenablerow.responseVaraibleOnlyFloat.isActive - Activate or disable filter
 * @param {boolean}       filterenablerow.responseVaraibleOnlyFloat.isEnalbe - Enable component to activate or disable it
 * @param {string}        ref - Reference made by React.forward
 * @property {string}     data-testid - Id to use inside rowgroupdetail.test.js file.
 * @returns {component}   Return array of row details
 */
const RowGroupDetailMolecule = React.forwardRef(
  (
    {
      rowdetails,
      columns,
      actionSave,
      functiongettrait,
      actioncolumnwidth,
      columnwidth,
      classes,
      actioncheck,
      filterenablerow,
    },
    ref,
  ) => {
    return (
      <Fragment>
        {rowdetails.map((rowDetail, index) => {
          const key = `rowGroupDetail_${index}`.toString();
          return (
            <TableRow key={key} className={classes.rowOccurrence}>
              <RowDetailMolecule
                rowDetail={rowDetail}
                columns={columns}
                actionSave={actionSave}
                functionGetTrait={functiongettrait}
                actionColumnWidth={actioncolumnwidth}
                columnwidth={columnwidth}
                classes={classes}
                actioncheck={actioncheck}
                filterenablerow={filterenablerow}
              />
            </TableRow>
          );
        })}
      </Fragment>
    );
  },
);
// Type and required properties
RowGroupDetailMolecule.propTypes = {
  rowdetails: PropTypes.array.isRequired,
  columns: PropTypes.array.isRequired,
  actionSave: PropTypes.string.isRequired,
  functiongettrait: PropTypes.func.isRequired,
  actioncolumnwidth: PropTypes.number.isRequired,
  columnwidth: PropTypes.number.isRequired,
  classes: PropTypes.object.isRequired,
  actioncheck: PropTypes.number.isRequired,
  filterenablerow: PropTypes.shape({
    sameExperiemnt: PropTypes.shape({
      isActive: PropTypes.bool.isRequired,
      isEnable: PropTypes.bool.isRequired,
    }).isRequired,
    responseVaraibleOnlyFloat: PropTypes.shape({
      isActive: PropTypes.bool.isRequired,
      isEnable: PropTypes.bool.isRequired,
    }).isRequired,
  }).isRequired,
};
// Default properties
RowGroupDetailMolecule.defaultProps = {};

export default RowGroupDetailMolecule;
