import React from 'react';
import ReactDOM from 'react-dom';
// Component to be Test
import HeadTableMaster from './headtablemaster';
// Test Library
import { render, cleanup } from '@testing-library/react';
import '@testing-library/dom';
import '@testing-library/jest-dom/extend-expect';

afterEach(cleanup);
// Props to send component to be rendered
const props = {
  title: '',
  paginationSizes: [5, 10, 15],
  totalCount: 10,
  rowsPerPage: 5,
  page: 0,
  columns: [],
  classes: {
    class: 'class',
  },
  handlechangepage: jest.fn(),
  handlechangerowsperpage: jest.fn(),
  handlechangecolumns: jest.fn(),
  filterenablerow: {
    sameExperiemnt: {
      isActive: true,
      isEnable: false
    },
    responseVaraibleOnlyFloat: {
      isActive: true,
      isEnable: true,
    },
  },
  handleclearrownandsearch: jest.fn,
};

test('Report name', () => {
  const div = document.createElement('div');
  ReactDOM.render(<HeadTableMaster {...props}></HeadTableMaster>, div);
});

test('Render correctly', () => {
  const { getByTestId } = render(<HeadTableMaster {...props}></HeadTableMaster>);
  expect(getByTestId('HeadTableMasterTestId')).toBeInTheDocument();
});
