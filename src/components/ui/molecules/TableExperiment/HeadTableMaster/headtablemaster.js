import React from 'react';
import PropTypes from 'prop-types';
// CORE COMPONENTS AND ATOMS TO USE
import { Grid, TablePagination, Typography } from '@material-ui/core';
import TableSettingButtonMolecule from '../TableSettingButton';
import TableFilterButtonMolecule from '../TableFilterButton';

//MAIN FUNCTION
/**
 * @description           Head component to display the title of table, pagination, and other componentes
 * @param {string}        title - Text to set in the title
 * @param {array}         paginationSizes - Array of the size per pages
 * @param {number}        totalCount - Total of element
 * @param {number}        rowsPerPage - Total number of item per page
 * @param {number}        page - Actual page
 * @param {array}         columns - Column array
 * @param {object}        classes - Classes to put spesific style with makestyle
 * @param {object}        filterenablerow - Object to enable or disable selection row
 * @param {object}        filterenablerow.sameExperiemnt - Filter to select only one experiment
 * @param {boolean}       filterenablerow.sameExperiemnt.isActive - Activate or disable filter
 * @param {boolean}       filterenablerow.sameExperiemnt.isEnalbe - Enable component to activate or disable it
 * @param {object}        filterenablerow.responseVaraibleOnlyFloat - Filter to select only respose varaible (trait) type float
 * @param {boolean}       filterenablerow.responseVaraibleOnlyFloat.isActive - Activate or disable filter
 * @param {boolean}       filterenablerow.responseVaraibleOnlyFloat.isEnalbe - Enable component to activate or disable it
 * @param {function}      functionresetfilterrow - Parent function to reset filter row
 * @param {function}      handleclearrownandsearch - Clear all row and search agait
 * @function {void}       handlechangepage - Parent function to change the page
 * @function {void}       handlechangerowsperpage - Parent function to change the row per page
 * @function {void}       handlechangecolumns - Parent function to change the visible of columns
 * @param {string}        ref - reference made by React.forward
 * @property {string}     data-testid - Id to use inside headtablemaster.test.js file.
 * @returns {component}   Head of table
 */
const HeadTableMasterMolecule = React.forwardRef(
  (
    {
      title,
      paginationSizes,
      totalCount,
      rowsPerPage,
      page,
      columns,
      classes,
      filterenablerow,
      handlechangepage,
      handlechangerowsperpage,
      handlechangecolumns,
      functionresetfilterrow,
      handleclearrownandsearch,
    },
    ref,
  ) => {
    return (
      <Grid container ref={ref} data-testid={'HeadTableMasterTestId'}>
        <Grid item xs={12}>
          <Grid container spacing={10}>
            <Grid item xs={11}>
              <Typography variant='h6'>{title}</Typography>
            </Grid>
          </Grid>
          <Grid
            container
            direction='row'
            justifyContent='space-between'
            alignItems='stretch'
          >
            <Grid item>
              <TablePagination
                component={Grid}
                rowsPerPageOptions={paginationSizes}
                count={totalCount}
                rowsPerPage={rowsPerPage}
                page={page}
                onPageChange={handlechangepage}
                onRowsPerPageChange={handlechangerowsperpage}
              />
            </Grid>
            <Grid item>
              <Grid container>
                <Grid item>
                  <TableFilterButtonMolecule
                    classes={classes}
                    filterenablerow={filterenablerow}
                    functionresetfilterrow={functionresetfilterrow}
                    handleclearrownandsearch={handleclearrownandsearch}
                  ></TableFilterButtonMolecule>
                </Grid>
                <Grid item>
                  <TableSettingButtonMolecule
                    columns={columns}
                    classes={classes}
                    handlechangecolumns={handlechangecolumns}
                  ></TableSettingButtonMolecule>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    );
  },
);
// Type and required properties
HeadTableMasterMolecule.propTypes = {
  title: PropTypes.string.isRequired,
  paginationSizes: PropTypes.array.isRequired,
  totalCount: PropTypes.number.isRequired,
  rowsPerPage: PropTypes.number.isRequired,
  page: PropTypes.number.isRequired,
  columns: PropTypes.array.isRequired,
  classes: PropTypes.object.isRequired,
  handlechangepage: PropTypes.func.isRequired,
  handlechangerowsperpage: PropTypes.func.isRequired,
  handlechangecolumns: PropTypes.func.isRequired,
  filterenablerow: PropTypes.shape({
    sameExperiemnt: PropTypes.shape({
      isActive: PropTypes.bool.isRequired,
      isEnable: PropTypes.bool.isRequired,
    }).isRequired,
    responseVaraibleOnlyFloat: PropTypes.shape({
      isActive: PropTypes.bool.isRequired,
      isEnable: PropTypes.bool.isRequired,
    }).isRequired,
  }).isRequired,
  functionresetfilterrow:  PropTypes.func.isRequired,
  handleclearrownandsearch: PropTypes.func.isRequired,
};
// Default properties
HeadTableMasterMolecule.defaultProps = {};

export default HeadTableMasterMolecule;
