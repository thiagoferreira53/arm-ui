import React from 'react'
import ReactDOM from 'react-dom'
// Component to be Test
import ColumnMaster from './columnmaster'
// Test Library
import { render, cleanup } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'

afterEach(cleanup)
// Props to send component to be rendered
const props = {
  columns: [],
  actionColumnWidth: 1,
  orderBy: '',
  order: 'asc',
  onRequestSort: jest.fn(),
  onFilterColumn: jest.fn(),
  columnWidth: 1,
  columnwidth: 1,
  classes: {
    class: 'class',
  },
}

test('Report name', () => {
  const div = document.createElement('table')
  ReactDOM.render(
    <thead>
      <ColumnMaster {...props}></ColumnMaster>
    </thead>,
    div,
  )
})

test('Render correctly', () => {
  const { getByTestId } = render(
    <table>
      <thead>
        <ColumnMaster {...props}></ColumnMaster>
      </thead>
    </table>,
  )
  expect(getByTestId('ColumnMasterTestId')).toBeInTheDocument()
})
