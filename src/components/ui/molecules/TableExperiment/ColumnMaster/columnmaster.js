import React from 'react'
import PropTypes from 'prop-types'
// CORE COMPONENTS AND ATOMS TO USE
import {
  TableHead,
  TableRow,
  TableSortLabel,
  TableCell,
  TextField,
  Table,
} from '@material-ui/core'

//MAIN FUNCTION
/**
 * @description         - Header column that display the name of column, the search input and short button
 * @param {object}      props - Children properties
 * @param {function}    onRequestSort - Function to sort the row under the column
 * @param {function}    onFilterColumn - Function to filter the rows
 * @param {object}      classes - Classes to put spesific style with makestyle
 * @param {string}      order - Atribute to define if the order is asc ord desc
 * @param {string}      orderBy - ID the column to order this
 * @param {number}      actionColumnWidth - The width of the action column is defined in the style code
 * @param {number}      columnwidth - The width of all other column is defined in the style code
 * @param {string}      ref - Reference made by React.forward
 * @function {void}     createSortHandler - Function to launch the sort action
 * @function {void}     filterSortHandler - Function to lanunch the filter action
 * @property {string}   data-testid - Id to use inside columntablem.test.js file.
 * @returns {componet}  Return Columna mater of table
 */
const ColumnMasterMolecule = React.forwardRef((props, ref) => {
  const {
    children,
    columns,
    actionColumnWidth,
    orderBy,
    order,
    onRequestSort,
    onFilterColumn,
    columnwidth,
    classes,
    ...rest
  } = props

  const createSortHandler = (property) => (event) => {
    onRequestSort(event, property)
  }
  const filterSortHandler = (property) => (event) => {
    onFilterColumn(event, property)
  }

  return (
    <TableRow ref={ref} data-testid={'ColumnMasterTestId'}>
      <TableCell width={actionColumnWidth}></TableCell>
      <TableCell width={actionColumnWidth}></TableCell>
      {columns.map((column, index) => {
        if (!column.isHidden) {
          const width = column.width !== undefined ? column.width : columnwidth
          return (
            <TableCell
              key={`${index}_${column.field}`}
              sortDirection={orderBy === column.field ? order : false}
              width={width}
            >
              <TableSortLabel
                active={orderBy === column.field}
                direction={orderBy === column.field ? order : 'asc'}
                onClick={createSortHandler(column.field)}
              >
                {column.headerName}
                {orderBy === column.field ? (
                  <span className={classes.visuallyHidden}>
                    {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                  </span>
                ) : null}
              </TableSortLabel>
              <TextField
                id={`search-input${index}_${column.field}`.toString()}
                label='Search'
                variant='outlined'
                onChange={filterSortHandler(column.fieldMaster)}
                size='small'
              />
            </TableCell>
          )
        }
      })}
    </TableRow>
  )
})
// Type and required properties
ColumnMasterMolecule.propTypes = {
  onRequestSort: PropTypes.func.isRequired,
  onFilterColumn: PropTypes.func.isRequired,
  classes: PropTypes.object.isRequired,
  order: PropTypes.oneOf(['asc', 'desc']).isRequired,
  orderBy: PropTypes.string.isRequired,
  actionColumnWidth: PropTypes.number.isRequired,
  columnwidth: PropTypes.number.isRequired,
}
// Default properties
ColumnMasterMolecule.defaultProps = {}

export default ColumnMasterMolecule
