import React from 'react'
import ReactDOM from 'react-dom'
// Component to be Test
import RowDetail from './rowdetail'
// Test Library
import { render, cleanup } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
import * as reactRedux from 'react-redux'

afterEach(cleanup)
// Props to send component to be rendered
const props = {
  rowDetail: { experimentDbId: 0, traits: [] },
  columns: [],
  actionSave: 'save',
  functionGetTrait: jest.fn().mockImplementation(() =>
    Promise.resolve({
      status: {
        status: 100,
        message: '',
      },
      data: [],
      metadata: {
        pagination: {
          totalPages: 0,
          totalCount: 0,
        },
      },
    }),
  ),
  numberVisibleColumn: 1,
  columnWidth: 1,
  actionColumnWidth: 1,
  classes: {
    class: 'class',
  },
  columnwidth: 10,
  actioncheck: 1,
  filterenablerow: {
    sameExperiemnt: {
      isActive: true,
      isEnable: false
    },
    responseVaraibleOnlyFloat: {
      isActive: true,
      isEnable: true,
    },
  },
}
// mock to use dispatch
const mockDispatch = jest.fn()
jest.mock('react-redux', () => ({
  useSelector: jest.fn(),
  useDispatch: () => mockDispatch,
}))
const useSelectorMock = jest.spyOn(reactRedux, 'useSelector')
const useDispatchMock = jest.spyOn(reactRedux, 'useDispatch')
useSelectorMock.mockReturnValue({ state: { newRequest: { experiment: [] } } })

test('Report name', () => {
  const div = document.createElement('table')
  ReactDOM.render(
    <tbody>
      <tr>
        <RowDetail {...props}></RowDetail>
      </tr>
    </tbody>,
    div,
  )
})

test('Render correctly', () => {
  const { getByTestId } = render(
    <table>
      <tbody>
        <tr>
          <RowDetail {...props}></RowDetail>
        </tr>
      </tbody>
    </table>,
  )
  expect(getByTestId('RowDetailTestId')).toBeInTheDocument()
})
