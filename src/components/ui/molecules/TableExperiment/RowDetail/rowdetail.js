import React, { Fragment, useEffect } from 'react';
import PropTypes from 'prop-types';
// CORE COMPONENTS AND ATOMS TO USE
import { TableCell, Table, TableBody, TableRow, Checkbox } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import CellStatusMolecule from '../CellStatus';

/**
 * @description       - Function to validate if it can check the row
 * @param {objetc}    rowDetail - Information about the occurrence
 * @param {objetc}    experiments - Information about the other occurrences was be selected
 * @param {function}  functionGetTrait - Function to intersect new trait
 * @param {objetc}    filter - Fiter to occurrences
 * @param {string}    severity ['warning', 'error'] - Constant to display to put class in message default warning
 * @returns {object}  - Configuration to enable or disable occurrence
 * @returns {bool}    return0.isCheckDisable - Flat for enable combobox in the row
 * @returns {bool}    return0.message - Message for display in case the combobox is disable
 * @returns {bool}    return0.severity - Class to put in the message
 */
export function validateCheckRow(
  rowDetail,
  experiments,
  functionGetTrait,
  filter,
  severity = 'warning',
) {
  let validateCheckRow = {
    isCheckDisable: false,
    message: '',
    severity: severity,
  };
  let traitsRow = rowDetail.traits;
  const isSameExperiment =
    experiments.length > 0
      ? experiments[0].experimentDbId === rowDetail.experimentDbId
        ? true
        : false
      : true;
  if (filter.responseVaraibleOnlyFloat.isActive) {
    traitsRow = traitsRow.filter(
      (response) => filter.responseVaraibleOnlyFloat.valueShouldTrue.includes(response.variableDataType.toUpperCase()),
    );
  }
  // Search origin response varaible row
  const isEmptyTrait = rowDetail.traits.length === 0;
  // Search origin response varaible row
  const isEmptyFilterTrait = traitsRow.length === 0;
  const isIntersectionTrait = functionGetTrait(
    traitsRow,
    experiments.length > 0 ? experiments.map((item) => item.traits) : [],
  );

  if (filter.sameExperiemnt.isActive && !isSameExperiment) {
    validateCheckRow.message = 'This occurrence is no the same experiment';
    validateCheckRow.isCheckDisable = true;
  }
  if (!validateCheckRow.isCheckDisable && !isIntersectionTrait.isIntersection) {
    validateCheckRow.message = 'This occurrence has no Crop trait in common';
    validateCheckRow.isCheckDisable = true;
  }
  if (!validateCheckRow.isCheckDisable && isEmptyTrait) {
    validateCheckRow.message = 'This occurrence has no Crop trait';
    validateCheckRow.isCheckDisable = true;
  }
  if (!validateCheckRow.isCheckDisable && isEmptyFilterTrait) {
    validateCheckRow.message =
      'The occurrences has no Crop trait that meet the filters';
    validateCheckRow.isCheckDisable = true;
  }
  return validateCheckRow;
}

//MAIN FUNCTION
/**
 * @description         Rows of details
 * @param {object}      rowDetail - Object with values od the row
 * @param {array}       columns - Array of the columns
 * @param {string}      actionSave - Action to save values into the react-redux
 * @param {function}    functionGetTrait - Width of the action column
 * @param {number}      actionColumnWidth - Width of the action column
 * @param {object}      classes - Classes to put spesific style with makestyle
 * @param {number}      columnwidth - Width of the column
 * @param {number}      actioncheck [-1|0|1] - Parrent action to indicate 3 states -1 = unchecked radio button, 0 = is not selected the radio button, 1= check the radio button
 * @param {object}      filterenablerow - Object to enable or disable selection row
 * @param {object}      filterenablerow.sameExperiemnt - Filter to select only one experiment
 * @param {boolean}     filterenablerow.sameExperiemnt.isActive - Activate or disable filter
 * @param {boolean}     filterenablerow.sameExperiemnt.isEnalbe - Enable component to activate or disable it
 * @param {object}      filterenablerow.responseVaraibleOnlyFloat - Filter to select only respose varaible (trait) type float
 * @param {boolean}     filterenablerow.responseVaraibleOnlyFloat.isActive - Activate or disable filter
 * @param {boolean}     filterenablerow.responseVaraibleOnlyFloat.isEnalbe - Enable component to activate or disable it
 * @param {string}      ref - Reference made by React.forward
 * @property {boolean}  check - State Variable to check the selected row
 * @property {array}    experiments - Redux to get the list of experiment selected
 * @property {boolean}  isValidateCheckRow - Validate if row is posible select the row
 * @function {void}     useEffect - Put the radio button to check or uncheck by event into the parent
 * @function {void}     handelCheck - Function to save the row selected
 * @property {string}   data-testid - Id to use inside rowdetail.test.js file.
 * @returns {componetn} Return row deaitl
 */
const RowDetailMolecule = React.forwardRef(
  (
    {
      rowDetail,
      columns,
      actionSave,
      functionGetTrait,
      actionColumnWidth,
      columnwidth,
      classes,
      actioncheck,
      filterenablerow,
    },
    ref,
  ) => {
    const [check, setCheck] = React.useState(false);
    const dispatch = useDispatch();
    const experiments = useSelector((state) => state.newRequest.experiment);
    const isValidateCheckRow = validateCheckRow(
      rowDetail,
      experiments,
      functionGetTrait,
      filterenablerow,
    );
    const isCheckEnable = !isValidateCheckRow.isCheckDisable;

    useEffect(() => {
      if (experiments.length > 0) {
        if (
          experiments.findIndex(
            (experiment) => experiment.occurrenceDbId === rowDetail.occurrenceDbId,
          ) > -1
        ) {
          setCheck(true);
        }
      }
    }, [experiments, rowDetail, setCheck]);

    useEffect(() => {
      if (isCheckEnable) {
        switch (actioncheck) {
          case 1:
            setCheck(true);
            dispatch({
              type: actionSave,
              payload: {
                add: true,
                data: rowDetail,
              },
            });
            break;
          case -1:
            setCheck(false);
            dispatch({
              type: actionSave,
              payload: {
                add: false,
                data: rowDetail,
              },
            });
            break;
          default:
            break;
        }
      }
    }, [actioncheck, setCheck, dispatch, actionSave, rowDetail, isCheckEnable]);

    const handelCheck = () => {
      dispatch({
        type: actionSave,
        payload: {
          add: !check,
          data: rowDetail,
        },
      });
      setCheck(!check);
    };

    return (
      <Fragment ref={ref}>
        <TableCell width={actionColumnWidth} data-testid={'RowDetailTestId'}>
          <Checkbox
            checked={check}
            onClick={handelCheck}
            disabled={isValidateCheckRow.isCheckDisable}
          />
        </TableCell>
        <TableCell width={actionColumnWidth}>
          <CellStatusMolecule
            isError={isValidateCheckRow.isCheckDisable}
            severity={isValidateCheckRow.severity}
            message={isValidateCheckRow.message}
          />
        </TableCell>
        {columns.map((column, index) => {
          let key = `cell_detail_${index}`.toString();
          const width = column.width !== undefined ? column.width : columnwidth;
          if (!column.isHidden) {
            return (
              <TableCell key={key} width={width}>
                {rowDetail[column.fieldDetail]}
              </TableCell>
            );
          }
        })}
      </Fragment>
    );
  },
);
// Type and required properties
RowDetailMolecule.propTypes = {
  rowDetail: PropTypes.object.isRequired,
  columns: PropTypes.array.isRequired,
  actionSave: PropTypes.string.isRequired,
  functionGetTrait: PropTypes.func.isRequired,
  actionColumnWidth: PropTypes.number.isRequired,
  classes: PropTypes.object.isRequired,
  columnwidth: PropTypes.number.isRequired,
  actioncheck: PropTypes.oneOf([-1, 1, 0]).isRequired,
  filterenablerow: PropTypes.shape({
    sameExperiemnt: PropTypes.shape({
      isActive: PropTypes.bool.isRequired,
      isEnable: PropTypes.bool.isRequired,
    }).isRequired,
    responseVaraibleOnlyFloat: PropTypes.shape({
      isActive: PropTypes.bool.isRequired,
      isEnable: PropTypes.bool.isRequired,
    }).isRequired,
  }).isRequired,
};
// Default properties
RowDetailMolecule.defaultProps = {};

export default RowDetailMolecule;
