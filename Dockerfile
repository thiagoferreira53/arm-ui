ARG baseImage=ebsproject/arm-ui
ARG baseImageTag=21.11.18

FROM node:16.13.0-buster AS builder

COPY . .

RUN npm install &&  npm run build

FROM ${baseImage}:${baseImageTag}

LABEL maintainer="Kenichii A. Ana <k.ana@irri.org> & Renee Arianne F. Lat <r.lat@irri.org>"

ARG sourceDir=dist
ARG libVersion=xxxx
ARG libMainFile=ebs-ba.js
ARG nginxDir=/usr/share/nginx/html
ARG latestDir=dev
ARG importMapFilename=app.importmap

#RUN apk add jq
RUN apt update && apt install -y jq

COPY --from=builder ${sourceDir}/ ${nginxDir}/${libVersion}/

# Update import map with the module URL
RUN moduleName=${libVersion}; \
    moduleUrl=${libVersion}/${libMainFile}; \
    echo $(cat ${nginxDir}/${importMapFilename} | \
    jq --arg moduleName "$moduleName" \ 
    --arg moduleUrl "$moduleUrl" '.imports[$moduleName] = $moduleUrl') > ${nginxDir}/app.importmap && \
    moduleName=${libVersion}/; \
    moduleUrl=${libVersion}/; \
    echo $(cat ${nginxDir}/${importMapFilename} | \
    jq --arg moduleName "$moduleName" \
    --arg moduleUrl "$moduleUrl" '.imports[$moduleName] = $moduleUrl') > ${nginxDir}/app.importmap